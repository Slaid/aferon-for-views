<?php

Yii::import('zii.widgets.CPortlet');

class Bullet extends CPortlet
{
   // public $title = 'Bullet';
    public $model;

    protected function renderContent()
    {

        echo CHtml::tag('h3', array(), $this->title);
        echo "<ul>";

        if(isset($this->model->bullet1)) {
            echo CHtml::tag("li", array() , $this->model->bullet1);
        }

        echo "</ul>";
    }
}