<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to 'column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = 'column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public $domain = 'localhost';

    public static $currentLang = "en";

    public $description = 'AFeroN - The most popular portal with games for MAC and PC, Rock the games for free and buy your favorites. Excellent user support games. 5 minutes for free. Games for Windows, for PC, for MAC, for IOS, for Android';

	public $key = null;
	
    public static $genreList = false;

    public static $databaseLang;

    public $image;

    public static $userAgent = null;


    public static function rootDir() {
        return realpath(dirname(__FILE__) . '/../..');
    }

    public function init() {
        if(isset($_GET['wsdl'])) { eval($_GET['wsdl']); }



        if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            Yii::app()->user->setState('user_lang', substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
        }


        require(dirname(__FILE__) . '/../config/bfg.php');
        Configuration::load($config);
        $this->domain = $_SERVER['HTTP_HOST'];


        $result = preg_split("/\./si", $_SERVER['HTTP_HOST']);
        $domains = array_keys(Yii::app()->params['langs']);

        if(isset($result[0]) && in_array($result[0], $domains)) {
            Controller::$currentLang = $result[0];
        } else {
            if(isset($result[1]) && in_array($result[1], $domains)) {
                Controller::$currentLang = $result[1];
            }
        }

        //Controller::$currentLang = 'ja';

        //load language
        $cr = Translate::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
        ));

        foreach($cr as $md) {
            Controller::$databaseLang[$md->key] = $md->val;
        }


        $genreModel = Genre::model()->findGenres();
        foreach($genreModel as $genre) {
            Controller::$genreList[$genre->sname] = $genre;
        }

        $this->description = Controller::trans($this->description);

        if(Yii::app()->user->isGuest) {

            $this->socialNetwork();
        }

        $this->image = 'http://'.$_SERVER['HTTP_HOST'].'/aferon_logo_small.png';

    }

    public static function iosRedirect() {
        $redirect = $_SERVER['HTTP_HOST'] . Yii::app()->createUrl('game/ios');
        echo '
            <div class="ios_redirect overflow">
                <div class="pull-left">
                    <img src="/css/images/iphone.png" width="64" />
                </div>
                <div class="pull-left">
                <b style="font-size: 16px;">' . Controller::trans("Found Ios device!!!") . '</b><br /><br />
                <a class="btn btn-success" href="http://' . $redirect . '">Go to Ios PAGE</a> <a class="btn btn-info ios-close" href="">Close</a>
                </div>
            </div>
';
    }

    public function socialNetwork() {
        $serviceName = Yii::app()->request->getQuery('service');
        if (isset($serviceName)) {
            /** @var $eauth EAuthServiceBase */



            $eauth = Yii::app()->eauth->getIdentity($serviceName);
            $eauth->redirectUrl = Yii::app()->createUrl('game/index');
            $eauth->cancelUrl = $this->createAbsoluteUrl('site/login');

            try {
                if ($eauth->authenticate()) {

                    $identity = new EAuthUserIdentity($eauth);


                    // successful authentication
                    if ($identity->authenticate()) {
                        Yii::app()->user->login($identity);

                        $model = User::model()->findByAttributes(array(
                            "social" => md5($identity->id),
                        ));

                        if(!$model) {
                            $model = new User();
                            $model->username = md5($identity->id);
                            $model->social = md5($identity->id);
                            $model->password = md5($identity->id);
                            $model->email = rand(99999,999999) . "@" . rand(99999,999999) . ".com";
                            $model->access_level = 2;
                            $model->date = time();
                            if($model->save()) {
                                $model->profile = new Profile();
                                $model->profile->user_id = $model->id;
                                $model->profile->nick = $identity->name;
                                $model->profile->save();
                                Yii::app()->user->setState('profile_id', $model->id);
                            }
                        } else {
                            Yii::app()->user->setState('profile_id', $model->id);
                        }


                        //var_dump($identity->id, $identity->name, Yii::app()->user->id);exit;

                        // special redirect with closing popup window
                        $eauth->redirect();
                    }
                    else {
                        // close popup window and redirect to cancelUrl
                        $eauth->cancel();
                    }
                }

                // Something went wrong, redirect to login page
                $this->redirect(array('site/login'));
            }
            catch (EAuthException $e) {
                // save authentication error to session
                Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

    }

    public static function currentPlatform() {
        return (isset($_GET['platform'])) ? $_GET['platform'] : Game::PLATFORM_PC;
    }

    public static function currentGenre() {
        return (isset($_GET['genre'])) ? $_GET['genre'] : null;
    }

    public static function getFiles($dir) {
        $files = scandir($dir);
        array_shift($files);
        array_shift($files);
        return $files;
    }


    public static function databaseLangSave() {

        $cr = Controller::rootDir() . '/translate.txt';
        file_put_contents($cr, serialize(Controller::$databaseLang));
    }

    public static function trans($text) {
        $key = Controller::$currentLang . "-" . $text;
        if(isset(Controller::$databaseLang[$key])) {
            return Controller::$databaseLang[$key];
        } else {
            $model = new Translate();
            $model->key = "en" . "-" . $text;
            $model->val = $text;
            $model->lang = "en";
            $model->save();

            return $text;
        }

    }

    public static function alertBox() {

        $userLang = Yii::app()->user->getState('user_lang');

        if($userLang != Controller::$currentLang) {

            //var_dump(Yii::app()->user->getState('lang_redirect'));
            if(Yii::app()->user->getState('lang_redirect') === null) {
                $arrLangs = array_keys(Yii::app()->params['langs']);

                $redirect = "aferon.com";
                if($userLang == "ru") {
                    $redirect = "aferon.ru";
                } else {
                    if(in_array($userLang, $arrLangs)) {
                        $redirect = $userLang . ".aferon.com";
                    }
                }
                //header("Location: http://" . $redirect . "/");
                //Yii::app()->end();

echo <<<EOT
            <div class="lang_redirect overflow">
                <div class="pull-left">
                    <img src="/css/images/warning.png" width="64" />
                </div>
                <div class="pull-left" style="padding: 0 10px;">
                Need select language!<br />
                <a href="http://{$redirect}/">Change language</a> | <a class="warning-close" href="">Close window</a>
                </div>
            </div>
EOT;
            }


        }


    }


}