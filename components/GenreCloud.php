<?php

Yii::import('zii.widgets.CPortlet');

class GenreCloud extends CPortlet
{
    public $title = 'Genres';

    protected function renderContent()
    {

        $genres = Genre::model()->findGenres();

        $items = array();

        foreach ($genres as $data) {
            if($data->ispri == "yes") {

                $items[] = array(
                    'label' => $data['name'],
                    'url' => array(
                        'game/games',
                        'platform' => Controller::currentPlatform(),
                        'genre' => $data['sname'],
                    ),
                    "icon" => $data['sname'],
                    "title" => "qwe",
                    "linkOptions" => array("title" => Controller::trans("View games with genre") . " " . $data['name']),
                );
            }

        }

        $this->widget('bootstrap.widgets.TbMenu', array(
            'type'=>'list',
            'items'=>$items,
        ));
    }
}