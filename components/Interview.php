<?php

Yii::import('zii.widgets.CPortlet');

class Interview extends CPortlet
{
    public $title = 'Recent comments Games';

    protected function renderContent()
    {
        echo "<ul>";
        $model = Gcomments::model()->with('game')->findAll(array(
            'order' => 'date DESC',
            'limit' => 5,
            'condition' => "game.lang = '" . Controller::$currentLang . "'",
        ));
        if(is_array($model) && count($model)) {
            foreach($model as $md) {
                echo "<li>";
                echo CHtml::tag('a', array(
                    "href" => $md->game->getUrl() . "#comments",
                    "title" => "View comments for game " . $md->game->gamename,
                ), $md->text);
                echo "</li>";
            }
        }
        echo "</ul>";
    }
}