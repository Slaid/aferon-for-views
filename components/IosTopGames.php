<?php

Yii::import('zii.widgets.CPortlet');

class IosTopGames extends CPortlet
{
    public $title = 'Ios Top Games';
    public $limit = 10;

    protected function renderContent()
    {

        $iosTop = Ios::model()->findAllByAttributes(array(
            'lang' => Controller::$currentLang,
        ), array(
            'order' => 'userRatingCount DESC',
            'limit' => $this->limit,
        ));

        echo '<ul>';
        if(is_array($iosTop) && count($iosTop) > 0) {
            foreach ($iosTop as $data) {
                echo '<li class="overflow left-top" style="margin-bottom: 5px; padding-bottom: 5px; border-bottom: 1px dashed #f1f1f1;">
                        <div class="pull-left" style="width: 57px; margin-right: 10px;"><a style="padding:0;" href="' . Yii::app()->createUrl('ios/view', array('sname' => $data->sname)) .'"><img src="' . $data->artworkUrl60 . '" /></a></div>
                        <div class="pull-left" style="width: 150px;">
                            <a href="' . Yii::app()->createUrl('ios/view', array('sname' => $data->sname)) .'" title="' . $data->trackName . '">' . $data->trackName . '</a>
                            <div class="price ' . (($data->price == "0") ? "free" : $data->price . " " . $data->currency) . '">
                                ' . (($data->price == "0") ? "Free" : $data->price . " " . $data->currency) . '
                            </div>
                        </div>
                     </li>';

            }
        }
        echo '</ul>';

        echo '<div style=""><a href="' . Yii::app()->createUrl('game/ios') . '">View all Ios Games!</a></div>';

    }
}