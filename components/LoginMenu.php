<?php

Yii::import('zii.widgets.CPortlet');

class LoginMenu extends CPortlet
{
	public function init()
	{
		$this->title=Controller::trans("User Panel");
		parent::init();
	}

	protected function renderContent()
	{
		$this->render('loginMenu');
	}
}