<?php

Yii::import('zii.widgets.CWidget');

class Review extends CWidget
{
    public $title = 'New Review';

    public function renderContent()
    {

        $features = Game::model()->findAllByAttributes(array(
            'lang' => Controller::$currentLang,
            'platform' => Game::PLATFORM_PC,
        ), array(
            'order' => 'rdate DESC',
            'limit' => 3,
        ));


        $items = array();
        $items[] = array('label' => Controller::trans($this->title));
        foreach($features as $data) {
            $items[] = array(
                'label' => $data->getGameName(),
                'url' => array('game/view', 'gameid' => $data->getSName($data->getGameName())),
                "linkOptions" => array("title" => Controller::trans("Review for PC game") . " " . $data->getShortDescription()),
            );
        }
        $this->widget('bootstrap.widgets.TbMenu', array(
            'type' => 'list',
            'items' => $items,
        ));

    }
}