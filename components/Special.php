<?php

Yii::import('zii.widgets.CPortlet');

class Special extends CPortlet
{
    public $title = 'Special';

    protected function renderContent()
    {


        $specials = Specials::model()->findSpecials();


        foreach ($specials as $data) {
            if($data && $data->game) {
                echo '<div class="special-item">';

                //if($_SERVER['HTTP_HOST'] != 'localhost') {
                    echo CHtml::tag('img', array(
                        'src' => Fish::model()->load($data->logo_url),
                        'title' => ($data->content=="catch") ? "Weekly" : "DailyDeal",

                    ));
                //}

                echo '<div class="special-content">';
                $image = json_decode($data->images);
                foreach($image as $img) {
                    echo CHtml::tag('a', array(
                            'href' => $data->game->getUrl(),
                            'class' => 'pull-left special-img',
                            'target' => '_blank',
                            'title' => Controller::trans('View special game') . ' ' . $data->gamename,
                        ),
                        CHtml::tag('img', array(
                            'src' => Fish::model()->images($data->game->foldername, 'feature'),
                            'alt' => Controller::trans('Special feature game screenshot 1')
                        ))
                    );
                }
                echo '<div class="special-description">';
                echo CHtml::tag('a', array(
                    'style' => 'font-size: 12px; font-weight: bold;',
                    'href' => $data->game->getUrl(),
                    'title' => Controller::trans('View special game') . ' ' . $data->gamename,
                ), $data->gamename);
                echo '<br /><b style="color:green;">' . $data->price . '</b> <b style="color:red; text-decoration: line-through">' . $data->game->price . '</b><br />';

                echo CHtml::tag("a", array(
                    'class' => 'btn btn-mini' . (($data->platform == Game::PLATFORM_MAC) ? ' btn-info' : ' btn-warning'),
                    'style' => 'margin-top: 5px;',
                    'rel' => 'nofollow',
                    'href' => $data->game->getDownloadUrl(),
                    'title' => Controller::trans('Download now this game') . ' ' . $data->gamename,
                ), Controller::trans("Now download"));
                echo '</div>';

                echo '<br/><b style="color: #5B44D3;">Offer END DATE</b><br/><div class="countdown">' . $data->offer_end_date . '</div>';

                echo '</div>';


                echo '</div>';
            }
        }
    }
}