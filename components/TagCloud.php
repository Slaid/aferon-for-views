<?php

Yii::import('zii.widgets.CPortlet');

class TagCloud extends CPortlet
{
    public $title = 'Genres';
    public $maxTags = 20;

    protected function renderContent()
    {

        $genres = Tag::model()->findTagWeights($this->maxTags);



        echo "<ul>";
        foreach ($genres as $sname => $data) {

            $link = CHtml::tag("a",
                array(
                    "href" => Yii::app()->createUrl('post/index', array(
                        'platform' => Game::getPlatformById(Controller::currentPlatform()),
                        'genre' => $sname,
                    )),
                    "_target" => "blank",
                    "title" => "View all with genre : " . $data['name'],
                ), $data['name']);


            echo CHtml::tag('li', array(
                    'class' => 'genre',
                ), $link) . "\n";
        }
        echo "</ul>";
    }
}