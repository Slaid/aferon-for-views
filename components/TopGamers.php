<?php

Yii::import('zii.widgets.CPortlet');

class TopGamers
{
    public $title = 'Top Gamers';

    public function init() {

    }

    public function run() {
        $model = User::model()->findAllByAttributes(array(

        ), array(
            "order" => "points DESC",
            'condition'=>'points>=1',
            'limit' => 5,
        ));

        $html = '';
        foreach($model as $data) {

            $html .= '
            <div class="user-list-block-sidebar">
                <div class="img">
                    ' . CHtml::tag('img',array(
                    "src" => Profile::getAvatar($data->id),
                )) . '
                </div>

                <div class="context">
                    ' . CHtml::tag("a", array(
                    "href" => Yii::app()->createUrl("user/view", array("id" => $data->id)),
                    "title" => Controller::trans("View profile"),
                ), (isset($data->profile->nick) ? $data->profile->nick : $data->username)) . ' <b style="color: green;">Rating ' . $data->points . '</b>
                    <div>
                        ' . Controller::trans("Date register") . ' : <br />' . date("j, M Y", $data->date) . '
                    </div>
                </div>
            </div>
            ';
        }
        echo $html;

    }

    protected function renderContent()
    {

    }
}