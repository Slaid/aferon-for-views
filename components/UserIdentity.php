<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	public $_id;

    public $email;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{

		$user=User::model()->find('LOWER(username)=?',array(strtolower($this->username)));
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$user->validatePassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id=$user->id;
			$this->username=$user->username;
            $this->setState('access_level', $user->acess_level);
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode==self::ERROR_NONE;
	}

    public function register() {
        $user = User::model()->find('LOWER(username)=?', array(strtolower($this->username)));

        if($user === null) {

            $user = new User();
            $user->email = strtolower($this->email);
            $user->username = $this->username;
            $user->password = $user->hashPassword($this->password);

            if($user->save()) {
                $this->errorCode=self::ERROR_NONE;
                return true;
            } else return false;

        }
        $this->errorCode=self::ERROR_USERNAME_INVALID;

    }

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}