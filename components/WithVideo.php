<?php

Yii::import('zii.widgets.CWidget');

class WithVideo extends CWidget
{
    public $title = 'Genres';

    public function renderContent()
    {

        $features = Game::model()->findAllByAttributes(array(
            'lang' => Controller::$currentLang,
            'platform' => Game::PLATFORM_PC,
        ), array('limit' => 4000));


        shuffle($features);
        $features = array_slice($features, 0 , 11);


        $items = array();
        $items[] = array('label' => Controller::trans($this->title));
        foreach($features as $data) {
            $items[] = array(
                'label' => $data->getGameName(),
                'url' => array('game/view', 'gameid' => $data->getSName($data->getGameName())),
                "linkOptions" => array("title" => Controller::trans("Download game") . " " . $data->getShortDescription()),
            );
        }
        $this->widget('bootstrap.widgets.TbMenu', array(
            'type' => 'list',
            'items' => $items,
        ));

    }
}