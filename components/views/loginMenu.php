<div class="overflow">
    <div class="pull-left">

        <ul>
            <li><?php echo CHtml::link(Controller::trans('Login'), array('site/login')); ?></li>
            <li><?php echo CHtml::link(Controller::trans('Register'), array('site/register')); ?></li>
        </ul>
        <div class="social_login">
            <b class="label-text"><?php echo Controller::trans('Fast Register'); ?></b>
            <div class="ic">
                <?php Yii::app()->eauth->renderWidget(); ?>
            </div>
        </div>
    </div>
</div>