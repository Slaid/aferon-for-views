<div class="overflow">
    <div class="pull-left" style="width: 110px; margin-right: 10px;">
        <img src="<?php echo Profile::getAvatar((Yii::app()->user->getState('profile_id')) ? Yii::app()->user->getState('profile_id') : Yii::app()->user->id); ?>?randval=<?php echo rand(99999,999999); ?>" />
    </div>
    <div class="pull-left">

        <ul style="width: 110px;">
            <?php /*<li><?php echo CHtml::link('Create New Post', array('post/create')); ?></li>
            <li><?php echo CHtml::link('Manage Posts', array('post/admin')); ?></li>
            <li><?php echo CHtml::link('Approve Comments', array('comment/index')) . ' (' . Comment::model()->pendingCommentCount . ')'; ?></li></li>
            <li><?php echo CHtml::link('Administrator', array('administrator/index')); ?> */ ?>
            <li><?php echo CHtml::link(Controller::trans('View Profile'), array('user/view')); ?></li>
            <li><?php echo CHtml::link(Controller::trans('Edit'), array('user/edit')); ?></li>
            <li><?php echo CHtml::link(Controller::trans('Logout'), array('site/logout')); ?></li>
        </ul>

    </div>
</div>