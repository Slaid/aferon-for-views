<?php
/*## AFFILIATE USER NAME #*/
$config['username']                         = 'lizzzee';
/*## CHANNEL AND  IDENTIFIER SETTINGS #*/
$config['channel_param']                    = 'channel';
$config['channel']                          = 'affiliates';
$config['identifier_param']                 = 'identifier';
$config['identifier']                       = 'af803568bb88';
/**## CACHE DIR #*/
$config['dir_cache']                        = '_xml_cache';
/**## DEFAULT LOCALE AND PLATFORM #*/
$config['locale']                           = 'en';
$config['platform']                         = 'pc';
/*## XML and ASSET SERVER #*/
$config['xml_server']                       = 'http://rss.bigfishgames.com/rss.php';
$config['asset_server']                     = 'http://games.bigfishgames.com';
/*## BFG SERVERS ##*/
$config['bfg_servers']['da']                = 'http://www.bigfishgames.dk';
$config['bfg_servers']['de']                = 'http://www.bigfishgames.de';
$config['bfg_servers']['en']                = 'http://www.bigfishgames.com';
$config['bfg_servers']['es']                = 'http://www.bigfishgames.es';
$config['bfg_servers']['fr']                = 'http://www.bigfishgames.fr';
$config['bfg_servers']['it']                = 'http://www.bigfishgames.it';
$config['bfg_servers']['ja']                = 'http://www.bigfishgames.jp';
$config['bfg_servers']['jp']                = 'http://www.bigfishgames.jp';
$config['bfg_servers']['nl']                = 'http://www.bigfishgames.nl';
$config['bfg_servers']['pt']                = 'http://www.bigfishgames.com.br';
$config['bfg_servers']['sv']                = 'http://www.bigfishgames.se';
/*## BFG STORE SERVERS ##*/
$config['bfg_store_servers']['da']          = 'https://store.bigfishgames.dk';
$config['bfg_store_servers']['de']          = 'https://store.bigfishgames.de';
$config['bfg_store_servers']['en']          = 'https://store.bigfishgames.com';
$config['bfg_store_servers']['es']          = 'https://store.bigfishgames.es';
$config['bfg_store_servers']['fr']          = 'https://store.bigfishgames.fr';
$config['bfg_store_servers']['it']          = 'https://store.bigfishgames.it';
$config['bfg_store_servers']['ja']          = 'https://store.bigfishgames.jp';
$config['bfg_store_servers']['jp']          = 'https://store.bigfishgames.jp';
$config['bfg_store_servers']['nl']          = 'https://store.bigfishgames.nl';
$config['bfg_store_servers']['pt']          = 'https://store.bigfishgames.com.br';
$config['bfg_store_servers']['sv']          = 'https://store.bigfishgames.se';
/*##  BFG SITE IDS ## */
$config['bfg_site_ids']['da']               = '17';
$config['bfg_site_ids']['de']               = '2';
$config['bfg_site_ids']['en']               = '1';
$config['bfg_site_ids']['es']               = '4';
$config['bfg_site_ids']['fr']               = '5';
$config['bfg_site_ids']['it']               = '18';
$config['bfg_site_ids']['ja']               = '6';
$config['bfg_site_ids']['jp']               = '6';
$config['bfg_site_ids']['nl']               = '15';
$config['bfg_site_ids']['pt']               = '19';
$config['bfg_site_ids']['sv']               = '16';
/*## DOWNLOAD FOLDER NAMES ##*/
$config['bfg_download_games_folders']['da'] ='download-spil';
$config['bfg_download_games_folders']['de'] ='download-spiele';
$config['bfg_download_games_folders']['en'] ='download-games';
$config['bfg_download_games_folders']['es'] ='juegos-de-descarga';
$config['bfg_download_games_folders']['fr'] ='jeux-a-telecharger';
$config['bfg_download_games_folders']['it'] ='scarica-giochi';
$config['bfg_download_games_folders']['ja'] ='download-games';
$config['bfg_download_games_folders']['jp'] ='download-games';
$config['bfg_download_games_folders']['nl'] ='download-spellen';
$config['bfg_download_games_folders']['pt'] ='jogos-para-baixar';
$config['bfg_download_games_folders']['sv'] ='ladda-ner-spel';
/*## ONLINE FOLDER NAMES ##*/
$config['bfg_online_games_folders']['da']   ='onlinespil';
$config['bfg_online_games_folders']['de']   ='online-spiele';
$config['bfg_online_games_folders']['en']   ='online-games';
$config['bfg_online_games_folders']['es']   ='juegos-en-linea';
$config['bfg_online_games_folders']['fr']   ='jeux-en-ligne';
$config['bfg_online_games_folders']['it']   ='giochi-online';
$config['bfg_online_games_folders']['ja']   ='online-games';
$config['bfg_online_games_folders']['jp']   ='online-games';
$config['bfg_online_games_folders']['nl']   ='online-spellen';
$config['bfg_online_games_folders']['pt']   ='jogos-online';
$config['bfg_online_games_folders']['sv']   ='online-games';
/**## REQUEST URL STRINGS #*/
$config['game_xml_request']                 = '{XML_SERVER}?username={USERNAME}&type=6&local={LOCALE}&gametype={GAMETYPE}';
$config['ogplaycount_xml_request']          = '{XML_SERVER}?ogplaycount&local={LOCALE}';
$config['genre_xml_request']                ='{XML_SERVER}?genre&local={LOCALE}';
/**## Game Info pages #*/
$config['game_info_pc']                     = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/{FOLDER}/index.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['game_info_mac']                    = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/mac/{FOLDER}/index.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['game_info_og']                     = '{BFG_SERVER}/{ONLINE_GAMES_FOLDER}/{GAMEID}/{FOLDER}/index.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
/**## IFRAMES #*/
$config['download_iframe_pc']               = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/{FOLDER}/download_pnp.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['download_iframe_mac']              = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/mac/{FOLDER}/download_pnp.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['buy_iframe_pc']                    = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/{FOLDER}/buy_pnp.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['buy_iframe_mac']                   = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/mac/{FOLDER}/buy_pnp.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['play_iframe_og']                   = '{BFG_SERVER}/{ONLINE_GAMES_FOLDER}/{GAMEID}/{FOLDER}/online.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
/**## DOWNLOAD URL'S #*/
$config['download_pc']                      = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/{FOLDER}/download.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['download_mac']                     = '{BFG_SERVER}/{DOWNLOAD_GAMES_FOLDER}/{GAMEID}/mac/{FOLDER}/download.html?{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
/**## ASSET SERVER PLAY URL #*/
$config['play_og']                          = '{ASSET_SERVER}/{FOLDERNAME}/online/index.html';
/**## PURCHASE URL #*/
$config['purchase_game']                    = '{BFG_STORE_SERVER}/cart.php?productID={PRODUCTID}&siteID={BFG_SITE_ID}&{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}';
$config['gift']                             = '{BFG_STORE_SERVER}/cart.php?productID={PRODUCTID}&siteID={BFG_SITE_ID}&tryGame={GAMEID}&{CHANNEL_PARAM}={CHANNEL}&{IDENTIFIER_PARAM}={IDENTIFIER}&cid=gamegift';

$config['unlimited']                        = '{BFG_SERVER}/unlimited/?afcode={IDENTIFIER}&{CHANNEL_PARAM}={CHANNEL}&identifier={IDENTIFIER}';
?>