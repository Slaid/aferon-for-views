<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
$dbconfig = require(dirname(__FILE__).'/../../dbconfig.php');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	'name' => "Official game portal",

	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import'=>array(
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
        'application.models.*',
        'application.components.*',
	),

    'theme'=>'bootstrap',

	'defaultController' => 'game',

	// application components
	'components' => array(

        'bootstrap' => array(
            'class'=>'bootstrap.components.Bootstrap',
        ),

		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin'=>false,
		),

		'db'=>array(
			'connectionString' => 'sqlite:protected/data/blog.db',
			'tablePrefix' => 'tbl_',
		),


        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'services' => array( // You can change the providers and their classes.


                 'google' => array(
                    'class' => 'GoogleOpenIDService',
                ),


                /*
                'yandex' => array(
                    'class' => 'YandexOpenIDService',
                ),
                */
                /* 'google_oauth' => array(
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'GoogleOAuthService',
                    'client_id' => '152470879064-fpq089ospajf475p33d27s4e120l6ets.apps.googleusercontent.com',
                    'client_secret' => 'zadVR4UeTLf3dii1icwdxIW5',
                    'title' => 'Google (OAuth)',
                ),
                */

                'twitter' => array(
                    'class' => 'TwitterOAuthService',
                    'key' => 'BFwJIFOwIfc1fTcyzFbPCg',
                    'secret' => 'TgiWaFEXDYel0fB7iBxz4ykMeeN1TQTs9AotwvqMaa8',
                ),

                'facebook' => array(
                    'class' => 'FacebookOAuthService',
                    'client_id' => '532596850146079',
                    'client_secret' => 'ee981d466726ba8502f02d66512d8e81',
                ),
                /*
                'vkontakte' => array(
                    'class' => 'VKontakteOAuthService',
                    'client_id' => '3713538',
                    'client_secret' => 'cra76fspxfnoeMEp3qjZ',
                ),
                */
                /*
                'mailru' => array(
                    'class' => 'MailruOAuthService',
                    'client_id' => '...',
                    'client_secret' => '...',
                ),
                */
            ),
        ),


		// uncomment the following to use a MySQL database

		'db' => $dbconfig,

		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager' => array(
			'urlFormat' => 'path',
            'showScriptName' => false,
			'rules' => array(

				'blog/<id:\d+>/<title:.*?>.html' => 'post/view',
				'posts/<platform:.*?>/<id:.*?>.html' => 'post/index',

                'php/<project_name:.*?>.html' => 'craft/index',

                'download-game/<gameid:.*?>.html' => 'game/view',
                'view-list/<genre:.*?>/games/for/<platform:.*?>.html' => 'game/games',

                'view-list/<platform:.*?>/games.html' => 'game/games',

                'play-online-game/<gameid:.*?>.html' => 'game/online',

                'gift-the-game/<gameid:.*?>.html' => 'game/giftCompressor3000',

                'game/gift' => 'game/giftGame',

                'game/ios.html' => 'ios/index',

                'game-club.html' => 'club/index',

                'ios/view/<sname:.*?>.html' => 'ios/view',

                'game/view/<gameid:.*?>' => 'game/viewID',

                'games/<platform:.*?>' => 'game/games',

                'page/view/<view:.*?>.html' => 'site/page',

                'files/<gameid:.*?>.exe' => 'game/download',

                'user/page/<id:.*?>.html' => 'user/view',

                'game/index.html' => 'game/index',

                'sitemap.xml' => 'site/getSitemap',
                'robots.txt' => 'site/robots',
                'admin' => 'game/admin',
                'export.json' => 'game/json',

				'<controller:\w+>/<action:\w+>.html' => '<controller>/<action>',


			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),



	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);