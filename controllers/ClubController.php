<?php

class ClubController extends Controller
{
	public $layout='column1';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

    public $n = 0;


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to access 'index' and 'view' actions.
				'actions' => array('index', 'view', 'find'),
				'users' => array('*'),
			),
			array('allow', // allow authenticated users to access all actions
				'users' => array('@'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{

		$model = $this->loadModel();

		$this->render('view',array(
			'model' => $model,
		));
	}

    public function createPage() {
        $name = $_GET['sname'];

        $model = new Club();
        $model->lang = controller::$currentLang;
        $model->name = Club::snameToName($name);
        $model->sname = $name;
        $model->promotion = 0;
        $model->rank = 0;
        $model->date = time();
        $model->save();
        return $model;
    }



    public function actionCreate() {

    }

    public function actionIndex() {

        $model = new Club();
        $model->lang = Controller::$currentLang;

        if(isset($_POST['Club']['name'])) {
            $model->name = $_POST['Club']['name'];
        }
        $dataProvider = $model->search();

        if(count($dataProvider->data) == 0) {

            $dataProvider->data[] = $this->parser($_POST['Club']['name']);
        }

        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));

    }

    public function parser($name = null) {
        if($name) {
            var_dump($name);
            die();
        }
    }


	public function loadModel()
	{

		if($this->_model===null)
		{

			if(isset($_GET['sname']))
			{

                $model = new Club();
                $model = $model->findByAttributes(array(
                    "sname" => $_GET['sname'],
                    "lang" => Controller::$currentLang,
                ));

                $this->_model=$model;

			}

			if($this->_model===null)
                $this->_model = $this->createPage();
		}

		return $this->_model;
	}
}
