<?php

class GameController extends Controller
{
	public $layout='column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

    public $n = 0;


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		//return array(
		//	'accessControl', // perform access control for CRUD operations
		//);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to access 'index' and 'view' actions.
				'actions' => array('index', 'export', 'update', 'unlimited', 'strategy', 'view', 'viewID', 'hrumer', 'nofolow', 'download', 'online', 'rating', 'games', 'community', 'gift', 'giftGame', 'giftCompressor3000', 'giftPage', 'find', 'android'),
				'users' => array('*'),
			),
			array('allow', // allow authenticated users to access all actions
                'actions' => array('admin'),
				'users' => array('@'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

    public function actionJson() {
        $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en';
        $platform = (isset($_GET['platform'])) ? $_GET['platform'] : 'pc';
        $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
        $page = $page - 1;
        $model = new Game();

        $attr = array(
            'lang' => $lang,
            'platform' => $platform,
        );

        if((isset($_GET['genre'])) && trim($_GET['genre']) != "") {
            $attr['genreid'] = $_GET['genre'];
        }

        $games = $model->findAllByAttributes($attr, array('limit' => 5, 'offset' => $page * 5, 'order' => 'releasedate DESC'));

        $genres = new Genre();
        $genres = $genres->findAllByAttributes(array(
            'lang' => $lang,
        ));

        $items = array();

        foreach ($genres as $data) {
            if($data->ispri == "yes") {
                $items[$data->genreid] = $data->name;
            }
        }

        echo json_encode(array("tpl" => $this->renderPartial('json', array(
                'model' => $games,
                'page' => $page,
                'lang' => $lang,
                'platform' => $platform,
            ), true, false),
            "genres" => $items,
            "count" => count($games),
        ));
    }

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{

		$model = $this->loadModel();
        //$relativeModel = $model->getRelative();

		$this->render('view',array(
			'model' => $model,
            //'relative' => $relativeModel,
		));
	}

    public function actionViewID()
    {

        if(isset($_GET['gameid']))
        {

            $model = new Game();
            $model = $model->findByAttributes(array(
                "gameid" => $_GET['gameid'],
                "lang" => Controller::$currentLang,
            ));

            $this->_model=$model;

        }
        if($this->_model===null)
            throw new CHttpException(404, Controller::trans('The requested page does not exist.'));
			
		header("HTTP/1.1 301 Moved Permanently"); 
		header("Location: " . Yii::app()->createUrl('game/view', array('gameid' => $model->getSName()))); 
		
		Yii::app()->end();

        //$relativeModel = $model->getRelative();

        $this->render('view',array(
            'model' => $model,
            //'relative' => $relativeModel,
        ));
    }

    public function actionRating() {

        $model = Game::model()->findByPk($_POST['gameid']);

        if($model) {
            $selectRating = (isset($_POST['value'])) ? $_POST['value'] : "";

            if($selectRating > 1 && $selectRating <= 5) {
                $newRating = (int)$model->rating + (int)$selectRating;
                $newCountRating = (int)$model->count_rating + 1;
                $model->rating = $newRating;
                $model->count_rating = $newCountRating;
                $model->save();
            }
        }
    }

    public function actionUnlimited() {
        $this->layout = 'column1';
        $this->render('unlimited', array());
    }

    public function actionDownload() {

        $file = $this->rootDir() . '/download_files.txt';
        if(!is_file($file)) {
            file_put_contents($file, serialize(array(
                "download" => 0,
            )));
        }

        $statistic = unserialize(file_get_contents($file));
        $statistic['download']++;
        file_put_contents($file, serialize($statistic));

        $model = Game::model()->findByAttributes(array(
            "gameid" => $_GET['gameid'],
            "lang" => Controller::$currentLang,
        ));

        $fish = new Fish();

        header("HTTP/1.1 302 Moved Permanently");
        if($model->platform == Game::PLATFORM_PC)
            header("Location: " . $fish->getParams('download_pc', $model->gameid, $model->foldername));

        if($model->platform == Game::PLATFORM_MAC)
            header("Location: " . $fish->getParams('download_mac', $model->gameid, $model->foldername));


    }

    public function actionOnline() {
        $model = $this->loadModel();

        $this->render('online', array(
            'model' => $model,
        ));
    }


    public function actionNofolow() {
        header("HTTP/1.1 302 Moved Permanently");
        header("Location: " . base64_decode($_GET['url']));
        Yii::app()->end();
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model = new Game();


        $features = $model->findAllByAttributes(array(
            'lang' => Controller::$currentLang,
            'platform' => Controller::currentPlatform(),
            'hasvideo' => 'yes',
            'hasdwfeature' => 'yes',
        ), array('limit' => 4));

        $models = Game::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
            "platform" => $this->currentPlatform(),
        ), array("limit" => 7, "order" => "gamerank DESC"));

        $news = Game::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
            "platform" => Game::PLATFORM_PC,
            "is_guide" => 0,
        ), array("limit" => 6, "order" => "releasedate DESC"));

		$this->render('index', array(
            'features' => $features,
            'models' => $models,
            'news' => $news,
            'find' => Game::model(),
		));

    }

    public function actionStrategy() {
        $model = Game::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
            "is_guide" => "pc",
        ), array(
            'limit' => '12',
            'condition' => "gamename LIKE '%" . addslashes(@$_POST['Game']['gamename']) . "%'"
        ));

        $this->render('guide', array(
            'model' => $model,
            'find' => Game::model(),
        ));
    }

    public function actionFind() {
        $model = Game::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
            "platform" => "pc",
        ), array(
            'limit' => '12',
            'condition' => "gamename LIKE '%" . addslashes(@$_POST['Game']['gamename']) . "%'"
        ));

        $this->render('find', array(
            'model' => $model,
            'find' => Game::model(),
        ));
    }

    public function actionSubscribe() {

        $model = new Game();

        $model->platform = Controller::currentPlatform();
        $model->lang = Controller::$currentLang;

        $model->postPerPage = 9;


        $dataProvider = $model->search();

        $model = new Subscribe();
        if(isset($_POST['Subscribe'])) {
            $model->attributes = $_POST['Subscribe'];
            if($model->save()) {

                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode("Aferon Subscribe").'?=';
                $headers="From: $name <" . Yii::app()->params['adminEmail'] . ">\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-Type: text/plain; charset=UTF-8";

                $body='Aferon.com
                Thank you subscribe to...';

                mail($model->email,$subject,$body,$headers);

                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> ' . Controller::trans("Thank you signed"));
            }
        }


        $this->render('subscribe', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionHrumer() {
        $model = Game::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
        ), array(
            "order" => "gamerank DESC",
            "limit" => 4000,
        ));
        shuffle($model);
        $result = array();
        $domain = (isset($_GET['domain'])) ? $_GET['domain'] : $this->domain;
        for($i = 0; $i < 100; $i++) {
            $result[] = "" . @$_GET['text'] . " [url=http://" . $domain . Yii::app()->createUrl('game/view', array('gameid' => $model[$i]->getSName())) . "]" . $model[$i]->getGameName() . "[/url]" .
                    " [url=http://" . $domain . Yii::app()->createUrl('game/view', array('gameid' => $model[$i]->getSName())) . "]" . $model[$i]->getGameName() . " " . Controller::trans("review") . "[/url]<br />";
            $resultTT[] = "http://" . $this->domain . Yii::app()->createUrl('game/view', array('gameid' => $model[$i]->getSName())) . "<br />";
        }

        echo "{" . implode(" | " , $result) . "}<br /><br />";
        echo "{" . implode(" | " , $resultTT) . "}<br /><br />";
        die();

    }



    public function actionGift() {
        $model = $this->loadModel();

        $email_shuffle = (isset($_POST['email_list'])) ? $_POST['email_list'] : false;
        preg_match_all("/[a-zA-Z\-\_\.]+@[a-zA-Z]+\.[a-zA-Z]{2,4}/si", $email_shuffle, $result);

        ob_start();
        $this->renderPartial('html_gift_game', array(
            'data' => $model,
        ));
        $html = ob_get_contents();
        ob_end_clean();



        //$html = "Privert";

        $emails = $result[0];
        var_dump($email_shuffle);

        foreach($emails as $email) {
            echo $email;
            Functions::smtpmail("slaid-1@mail.ru", "Present for you", "Привет как дела?", false, 'mail');
        }
    }

    protected function createGift($bg, $font, $text, $top, $left, $tTop, $tLeft, $textColor, $result) {

        $rgb=0xFFFFFF;
        $quality=95;
        if (!file_exists($bg)) return false;

        $size = getimagesize($bg);

        if ($size === false) return false;

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));

        $icfunc = "imagecreatefrom" . $format;
        if (!function_exists($icfunc)) return false;


        $isrc = $icfunc($bg);
        $ifont = $icfunc($font);

        $idest = imagecreatetruecolor($size[0], $size[1]);

        imagefill($idest, 0, 0, $rgb);
        imagecopyresampled($idest, $isrc, 0, 0, 0, 0,
            $size[0], $size[1], $size[0], $size[1]);

        imagecopyresampled($idest, $ifont, (int)$left-10, (int)$top-10, 0, 0,
            175, 150, 175, 150);

        $textcolor = imagecolorallocate($idest, $textColor[0], $textColor[1], $textColor[2]);

        $texts = preg_split('/\\n/si', $text);

        $topTextStart = $tTop + 5;
        $leftTextStart = $tLeft;
        $fontSize = 14;

        $font_file = $this->rootDir() . '/css/font/arial.ttf';
        foreach($texts as $id => $text) {
            //imagestring($idest, $fontSize, $leftTextStart, $topTextStart + ($id * 20), $text, $textcolor);
            imagefttext($idest, $fontSize, 0, $leftTextStart - 10, $topTextStart + ($id * 20), $textcolor, $font_file, $text);
        }

        imagejpeg($idest, $result, $quality);

        imagedestroy($isrc);
        imagedestroy($idest);

        return true;
    }

    public function actionGiftCompressor3000() {
        $model = $this->loadModel();

        if(isset($_POST['img'])) {
            $path = $this->rootDir();
            $this->createGift(
                $path . $_POST['img'],
                $path . Fish::images($model->foldername, 'feature'),
                $_POST['text'],
                str_replace("px", "", $_POST['img2top']),
                str_replace("px", "", $_POST['img2left']),
                str_replace("px", "", $_POST['text2top']),
                str_replace("px", "", $_POST['text2left']),
                explode(",", str_replace(array("rgb(", " ", ")"), array("","",""), $_POST['textColor'])),
                $path . '/images/result_gift/' . md5($path . $_POST['img'] . $path . Fish::images($model->foldername, 'feature')) . '.jpg'
            );

            echo 'http://' . $this->domain . '/images/result_gift/' . md5($path . $_POST['img'] . $path . Fish::images($model->foldername, 'feature')) . '.jpg',

            die();
        }

        $dir_bg = $this->rootDir() . "/images/gift";
        $file_name_list = $this->getFiles($dir_bg);

        $this->render('gift_compressor_3000',array(
            'data' => $model,
        ));
    }

    public function actionGiftGame() {

        $selectPlatfotm = (isset($_GET['platform'])) ? $_GET['platform'] : "pc";
        $compare_model = Game::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
            "platform" => $selectPlatfotm,
        ), array(
            "limit" => 15,
            "order" => "gamesize DESC",
        ));

        $this->render('gift_game', array(
            'model' => $compare_model,
        ));

    }


    public function actionGames()
    {

        $model = new Game();

        $model->platform = Controller::currentPlatform();
        $model->lang = Controller::$currentLang;

        if(isset($_GET['genre']))
        $model->genreid = Controller::$genreList[Controller::currentGenre()]->genreid;

        $model->postPerPage = 9;

        if(isset($_GET['tag'])) {
            $model->tag = Controller::$genreList[$_GET['tag']]->genreid;
        }


        $dataProvider = $model->search();


        $features = $model->findAllByAttributes(array(
            'lang' => Controller::$currentLang,
            'platform' => Controller::currentPlatform(),
        ), array('limit' => 5));


        $this->render('games', array(
            'dataProvider' => $dataProvider,
            'features' => $features,
        ));

    }


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */

    public function actionCommunity() {
        $model = $this->loadModel();
        $this->render('community', array(
            'model' => $model,
        ));
    }


    public function actionAndroid() {
        echo "//development for android";
    }

	public function loadModel()
	{

		if($this->_model===null)
		{

			if(isset($_GET['gameid']))
			{

                $model = new Game();
                $model = $model->findByAttributes(array(
                    "lang" => Controller::$currentLang,
                ), array(
                    "condition" => sprintf("(`sname` = '%s') or (`rsname` = '%s')", $_GET['gameid'], $_GET['gameid']),
                ));

                $this->_model=$model;

			}
			if($this->_model===null)
				throw new CHttpException(404, Controller::trans('The requested page does not exist.'));
		}

		return $this->_model;
	}

    public function actionAdmin() {
        $model = new Game();

        $id = (isset($_GET['id'])) ? $_GET['id'] : null;
        $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en';
        $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
        $limit = 10;
        $offset = $limit * ($page - 1);

        if(!$id) {

            $model = $model->findAllByAttributes(array(
                'lang' => $lang,
            ), array(
                'offset' => $offset,
                'limit' => $limit,
                //'order' => 'date DESC',
            ));

            $this->renderPartial('admin', array(
                'pages' => round(Game::model()->count(array("condition" => "`lang` = '" . $lang . "'")) / $limit) + 2,
                'step' => 1,
                'model' => $model,
            ));

        } else {

            $model = $model->findByPk($id);

            $syncModel = new Sync();
            $syncModel = $syncModel->findByAttributes(array(
                'gameid' => $id
            ));

            if(!$syncModel) $syncModel = new Sync();

            //Всех мам приветствует моя команда

            if(isset($_POST['Sync']['short'])) {
                $syncModel->gamename = $_POST['Sync']['gamename'];
                $syncModel->gameid = $id;
                $syncModel->short = $_POST['Sync']['short'];
                $syncModel->med = $_POST['Sync']['med'];
                $syncModel->long = $_POST['Sync']['long'];
                $syncModel->seo_title = $_POST['Sync']['seo_title'];
                $syncModel->seo_key = $_POST['Sync']['seo_key'];
                $syncModel->date = time();
                if($syncModel->save()) {
                    $md = new Game();
                    $md = $md->findByAttributes(array(
                        "gameid" => $id,
                    ));
                    $md->rdate = date("Y-m-d H:i:s");
                    //echo (isset($_POST['Game']['rsname'])) ? $_POST['Game']['rsname'] : $_POST['Game']['sname'];
                    $md->rsname = (isset($_POST['Game']['rsname'])) ? $_POST['Game']['rsname'] : null;
                    $md->save();
                }

            }

            $this->renderPartial('admin', array(
                'step' => 2,
                'model' => $model,
                'syncModel' => $syncModel,
            ));
        }



    }

    public function actionUpdate() {

        set_time_limit(0);

        $model = new Game();
        $model = $model->findAllByAttributes(array(
           "review_upload" => 0,
        ), array(
            "limit" => 30,
        ));

        foreach($model as $game) {

            $game_id = $game->gameid;

            $url = Fish::getParams('game_info_pc', (string)$game_id, '1');

            $page = file_get_contents($url);
            preg_match_all("/<span class=\"summary\">([^<]+)/si", $page, $resultTitle);
            $reviews['title'] = $resultTitle[1];

            preg_match_all("/<span class=\"description\">([^<]+)/si", $page, $resultDesc);
            $reviews['desc'] = $resultDesc[1];

            preg_match_all("/<span class=\"dtreviewed\">([^<]+)/si", $page, $createdAt);
            $reviews['created_at'] = $createdAt[1];

            $rr = array();

            for($i = 0; $i < count($reviews['title']); $i++) {
                $rr[$i]['title'] = $reviews['title'][$i];
                $rr[$i]['desc'] = $reviews['desc'][$i];
                $rr[$i]['created_at'] = $reviews['created_at'][$i];
            }

            foreach($rr as $review) {
                $md = new Reviews();
                $md->game_id = $game_id;
                $md->title = $review['title'];
                $md->desc = $review['desc'];
                $md->created_at = $review['created_at'];
                $md->save();
            }

            $gameModel = new Game();
            $gameModel = $gameModel->findByPk($game_id);
            $gameModel->review_upload = 1;
            $gameModel->save();

        }
    }
}
