<?php

class IosController extends Controller
{
	public $layout='column_ios';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

    public $n = 0;


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to access 'index' and 'view' actions.
				'actions' => array('index', 'view', 'find', 'hrumer'),
				'users' => array('*'),
			),
			array('allow', // allow authenticated users to access all actions
				'users' => array('@'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{

		$model = $this->loadModel();

		$this->render('view',array(
			'model' => $model,
		));
	}

    public function actionIndex() {

        $model = new Ios();

        Yii::app()->user->setState('ios_redirect', true);

        $condition = '';
        if(isset($_GET['price'])) {
            if($_GET['price'] == "free")
                $condition = 'price = 0';
            if($_GET['price'] == "paid")
                $condition = 'price > 0';
        }

        $model = $model->findAllByAttributes(array(
            'lang' => Controller::$currentLang,
        ), array(
            'limit' => 20,
            'order' => 'releaseDate DESC',
            'condition' => $condition,
        ));

        $this->render('index', array(
            'model' => $model,
        ));

    }

    public function actionFind() {
        $model = array();
        if(isset($_POST['Ios']['name'])) {
            $findName = $_POST['Ios']['name'];
            $model = Ios::model()->findAllByAttributes(array(
                "lang" => Controller::$currentLang,
            ), array (
                'order' => 'releaseDate DESC',
                'limit' => '20',
                'condition' => "trackName LIKE '%" . addslashes($findName) . "%'"
            ));

            if(is_array($model) && count($model) == 0) {
                $model = $this->findNewGames($findName);
            }
        }
        $this->render('find', array(
            'model' => $model,
        ));
    }

    protected function findNewGames($findName) {
        $model = array();

        $langISO = array(
            "en" => "us",
            "ru" => "ru",
            "nl" => "nl",
            "pt" => "pt",
            "sv" => "sv",
            "ja" => "jp",
            "it" => "it",
            "fr" => "fr",
            "es" => "es",
            "de" => "de",
            "da" => "us",
        );

        $lang = Controller::$currentLang;
        $lang = $langISO[$lang];
        $findName = urlencode($findName);

        $query = sprintf('https://itunes.apple.com/search?parameterkeyvalue&term=%s&country=%s&media=software&limit=50&at=11l6T4', $findName, $lang);

        $data = json_decode(file_get_contents($query));

        foreach($data->results as $data) {
            $data = (array)$data;
            if($data['primaryGenreName'] == "Games") {

                $modelNew = new Ios();

                foreach($data as $key => $item) {
                    if(is_array($item)) {
                        $data[$key] = implode(',', $item);
                    }
                }
                $modelNew->attributes = $data;
                $modelNew->lang = Controller::$currentLang;

                $sname = preg_replace("/[^A-Za-z0-9 ]+/si", "", $data['trackName']) . "-" . $data['trackId'] . "-" . Controller::$currentLang;

                $sname = preg_replace("/[ ]+/si", "-", $sname);
                $modelNew->sname = $sname;
                $modelNew->save();
                $model[]= $modelNew;

            }
        }
        return $model;
    }
	public function loadModel()
	{

		if($this->_model===null)
		{

			if(isset($_GET['sname']))
			{

                $model = new Ios();
                $model = $model->findByAttributes(array(
                    "sname" => $_GET['sname'],
                    "lang" => Controller::$currentLang,
                ));

                $this->_model=$model;

			}

			if($this->_model===null)
				throw new CHttpException(404, Controller::trans('The requested page does not exist.'));
		}

		return $this->_model;
	}

    public function actionHrumer() {
        $model = Ios::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
        ), array(
            "limit" => 4000,
        ));

        shuffle($model);
        $result = array();

        $domain = (isset($_GET['domain'])) ? $_GET['domain'] : $this->domain;
        for($i = 0; $i < count($model); $i++) {
            ob_start();
            $this->renderPartial('hrumer', array(
                'model' => $model[$i],
                'domain' => $domain,
            ));
            $html = ob_get_contents();
            ob_end_clean();

            $result[] = $html;
        }

        echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>";
        echo "{" . implode(" | " , $result) . "}<br /><br />";
        echo "</body></html>";
        die();
    }
}
