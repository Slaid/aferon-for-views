<?php

class SiteController extends Controller
{
	public $layout='column2';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
        $model = Game::model()->findAllByAttributes(array(
            "lang" => Controller::$currentLang,
        ), array(
            'order' => 'releasedate DESC',
            'limit' => 7,
        ));

	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest) {
	    		echo $error['message'];
            }
	    	else {
	        	$this->render('error', array("error" => $error, "model" => $model));
            }
	    }
	}

    public  function actionRobots() {
        $domain = (Controller::$currentLang == "en") ? "" : Controller::$currentLang . ".";
        echo <<<EOD
User-Agent: *
Disallow: /protected/

Sitemap: http://{$domain}aferon.com/sitemap/en_sitemap.xml
EOD;

    }

    public function actionGetSitemap() {
        echo file_get_contents(controller::rootDir() . "/sitemap/" . Controller::$currentLang . "_sitemap.xml");
    }

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{

		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
                Functions::smtpmail(Yii::app()->params['adminEmail'], $model->subject, $model->body . "<br />" . $model->email);
				Yii::app()->user->setFlash('contact', Controller::trans('Thank you for contacting us. We will respond to you as soon as possible.'));
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

    public function actionChat() {
        $this->render('chat');
    }

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model = new LoginForm('login');

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid

			if($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
            }
		}
		// display the login form
		$this->render('login',array('model' => $model));
	}

    public function actionRegister() {


        $model = new LoginForm('register');


        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->registration()) {
                Yii::app()->user->setFlash('success', sprintf('<strong>Profit!</strong> You have successfully registered. Please %s',
                    CHtml::tag("a", array(
                            "href" => Yii::app()->createUrl('site/login')
                        ), "Login"
                    )
                ));
                $model = new LoginForm('register');
            }
        }

        $this->render('register',array('model' => $model));
    }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionSitemap() {
        $block = array(
            "platforms" => Game::getPlatforms(),
            "genres" => Genre::model()->findGenres(),
        );
        if(isset($_GET['genre']) && isset($_GET['platform'])) {
            $block['games'] = Game::model()->findAllByAttributes(array(
                "lang" => Controller::$currentLang,
                "genreid" => $_GET['genre'],
                "platform" => $_GET['platform'],
            ));
        }
        $this->render('sitemap', array("block" => $block));
    }
}
