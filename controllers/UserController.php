<?php

class UserController extends Controller
{
	public $layout='column2';
    public $user_id = false;

    private $_model = null;

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to access 'index' and 'view' actions.
                'actions'=>array('index', 'view', 'send', 'getMessages', 'points', 'setUserLang', 'setIosFind'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated users to access all actions
                'actions' => array('edit', 'uploadAvatar', 'addComment', 'deleteComment', 'createChanel', 'changeChanel', 'updateActiveUser', 'setUserLang'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function getUserId() {
        $user_id = Yii::app()->user->getState('profile_id');
        if(!$user_id)
            $user_id = Yii::app()->user->id;
        return $user_id;
    }

    public function actionIndex() {

        $model = User::model()->findAll();
        $this->render('index', array('model' => $model));
    }

	public function actionView() {

        $model = $this->loadModel();
        $this->render('view',array('data'=>$model));
    }

    public function actionSetUserLang() {
        Yii::app()->user->setState('lang_redirect', 1);
        echo json_encode(array("lang_redirect" => "true"));
        Yii::app()->end();
    }

    public function actionSetIosFind() {
        Yii::app()->user->setState('ios_redirect', true);
        echo json_encode(array("ios_redirect" => "true"));
        Yii::app()->end();
    }

    public function actionAddComment() {
        $errors = null;

        if(isset($_REQUEST['gameid']) && $_POST['text']) {

            $comment_model = new Gcomments();
            $comment_model->gameid = $_REQUEST['gameid'];
            $comment_model->user_id = $this->getUserId();
            $comment_model->text = strip_tags($_POST['text']);
            $comment_model->date = time();
            $comment_model->save();
            $errors = $comment_model->getErrors();

        }

        $model = Game::model()->findByAttributes(array(
            "gameid" => $_REQUEST['gameid'],
        ));

        $this->renderPartial('_game_comment_list', array(
            'data' => $model,
            'errors' => $errors,
        ));
    }


    public function actionDeleteComment() {

        if(isset($_REQUEST['gameid']) && isset($_REQUEST['id'])) {

            Gcomments::model()->deleteAllByAttributes(array(
                "gameid" => $_REQUEST['gameid'],
                "id" => $_REQUEST['id'],
                "user_id" => $this->getUserId(),
            ));
        }

        $model = Game::model()->findByAttributes(array(
            "gameid" => $_REQUEST['gameid'],
        ));

        $this->renderPartial('_game_comment_list', array(
            'data' => $model,
        ));

    }

    public function actionEdit() {

        $model = $this->loadModel();
        if($model->id != $this->getUserId())
            throw new CHttpException(404, Controller::trans('The requested page does not exist.'));

        if($_POST) {
            if($_POST['avatar'] != "false") {
                $serverUserDir = Controller::rootDir() . Yii::app()->params['images_user'] . '/' . $this->getUserId();
                if(copy($serverUserDir . '/temp_avatar.jpg', $serverUserDir . '/avatar.jpg')) {
                    $model->profile->avatar = Yii::app()->params['images_user'] . '/' . $this->getUserId() . '/' . 'avatar.jpg';
                }
            }
            $model->profile->attributes = $_POST['Profile'];
            $model->profile->save();
        }
        $this->render('edit',array('data'=>$model));
    }

    public function actionGetMessages() {
        $dbfile = dirname(__FILE__) . "/../data/chat.db";
        $chat_array = unserialize(file_get_contents($dbfile));
        $this->widget('bootstrap.widgets.TbMenu', array(
            'type' => 'list',
            'items' => $chat_array,
        ));
    }

    public function actionSend() {

        $user_id = $this->getUserId();
        if(!$user_id) {
            echo json_encode(array(
                'result' => true,
                'message' => CHtml::tag("a", array(
                    'href' => Yii::app()->createUrl('site/login'),
                ), "Please login..."),
            ));
            die();
        }


        $msg = (isset($_POST['msg'])) ? $_POST['msg'] : false;
        if($msg) {
            $chat_array = array();
            $chanel = (isset($_GET['chanel'])) ? $_GET['chanel'] : false;
            $dbfile = dirname(__FILE__) . "/../../chat_data/" . $chanel . ".txt";
            if(!is_file($dbfile)) {

                $chat_array[] = array(
                    'username' => Yii::app()->user->name,
                    'userid' => Yii::app()->user->id,
                    'text' => CHtml::encode($msg),
                    'id' => $id = 0,
                );

            } else {

                $chat_array = json_decode(file_get_contents($dbfile));
                if(is_array($chat_array) && count($chat_array) > 19) {
                    array_shift($chat_array);
                }
                $chat_array[] = array(
                    'username' => Yii::app()->user->name,
                    'userid' => Yii::app()->user->id,
                    'text' => CHtml::encode($msg),
                    'id' => $id = $chat_array[count($chat_array) - 1]->id+1,
                );

            }
            file_put_contents($dbfile, json_encode($chat_array));
            echo json_encode(array(
                'result' => true,
                'data' => json_encode($chat_array[count($chat_array)-1]),
                'id' => $id,
            ));
            die();
        }
    }

    public function actionUpdateActiveUser() {
        if(!Yii::app()->user->isGuest) {
            $user_id = $this->getUserId();
            $user_name = Yii::app()->user->name;
            $_chanelName = ($_GET['chanel']) ? $_GET['chanel'] : false;
            if(!$_chanelName) return false;

            $dbfile = dirname(__FILE__) . "/../../chat_data/chanel_list.txt";
            $chanel_list = json_decode(file_get_contents($dbfile));

            $chanel_list->$_chanelName->user_list->$user_name->update_time = time();

            file_put_contents($dbfile, json_encode($chanel_list));

        }

    }

    public function actionChangeChanel() {
        $_chanelOld = (isset($_REQUEST['chanel_name_old'])) ? $_REQUEST['chanel_name_old'] : false;
        $_chanelNew = (isset($_REQUEST['chanel_name_new'])) ? $_REQUEST['chanel_name_new'] : false;

        if(!$_chanelNew || Yii::app()->user->isGuest) return false;

        $user_name = Yii::app()->user->name;

        $userTimeLiveInChat = 30;

        $dbfile = dirname(__FILE__) . "/../../chat_data/chanel_list.txt";
        $chanel_list = json_decode(file_get_contents($dbfile));


        foreach($chanel_list as $name => $chanel_info) {
            $chanel_info->user_list = (array)$chanel_info->user_list;

            if($name == $_chanelNew) {

                $chanel_info->user_list[$user_name] = array(
                    'userid' => Yii::app()->user->id,
                    'update_time' => time(),
                );

            } else {
                unset($chanel_info->user_list[$user_name]);
            }


            if(count($chanel_info->user_list) > 0) {
                $chanel_info->user_list = (array)$chanel_info->user_list;
                foreach($chanel_info->user_list as $us_name => $us_data) {
                    $us_data = (array)$us_data;

                    if(@$us_data['update_time'] + $userTimeLiveInChat < time()) {

                        unset($chanel_info->user_list[$us_name]);
                    }
                }
            }


        }
        file_put_contents($dbfile, json_encode($chanel_list));
    }

    public function actionCreateChanel() {

        $_chanelName = ($_POST['chanel_name']) ? $_POST['chanel_name'] : false;
        preg_match_all('/[a-zA-Z0-9\_\-]+/si', $_chanelName, $result);
        $_chanelName = $result[0][0];

        if(Yii::app()->user->isGuest) {
            echo json_encode(array(
                'result' => false,
                'message' => Controller::trans("Please need login"),
            ));
            die();
        }

        if($_chanelName) {
            $message = "Chanel created!";
            $result = true;

            $username = Yii::app()->user->name;

            $dbfile = dirname(__FILE__) . "/../../chat_data/chanel_list.txt";
            if(!is_file($dbfile)) {

                $id = 0;
                $chanel_list[$_chanelName] = array(
                    'user_list' => false,
                );

            } else {

                $chanel_list = json_decode(file_get_contents($dbfile));

                $chanel_list = (array)$chanel_list;

                if(in_array($_chanelName, array_keys($chanel_list))) {
                    $message = Controller::trans("Name is already");
                    $result = false;

                } else {
                    $chanel_list[$_chanelName] = array(
                        'user_list' => array(
                            $username => array(
                                'userid' => Yii::app()->user->id,
                                'update_time' => time(),
                            ),
                        ),

                    );
                }

                foreach($chanel_list as $name => $chanel_info) {
                    $chanel_info = (array)$chanel_info;

                    if($chanel_info['user_list'] == false) {
                        @unlink(dirname(__FILE__) . "/../../chat_data/". $name . ".txt");
                        unset($chanel_list[$name]);
                    }
                }
            }

            if($result === true) {
                $chat_array[] = array(
                    'username' => Yii::app()->user->name,
                    'userid' => Yii::app()->user->id,
                    'text' => Controller::trans("is create this chat."),
                    'id' => 0,
                );
                file_put_contents(dirname(__FILE__) . "/../../chat_data/". $_chanelName . ".txt", json_encode($chat_array));
            }

            file_put_contents($dbfile, json_encode($chanel_list));
            echo json_encode(array(
                'result' => $result,
                'message' => $message,
            ));

        } else {
            echo json_encode(array('result' => false, 'message' => Controller::trans('Please write chanel name.')));
        }
    }

    public function actionUploadAvatar() {

        $serverUserDir = Controller::rootDir() . Yii::app()->params['images_user'] . '/' . $this->getUserId();
        if(!file_exists($serverUserDir)) {
            mkdir($serverUserDir);
        }
        $uploadfile = $serverUserDir . '/' . 'temp_avatar.jpg';

        $musor = preg_split("/[\.]+/si", $_FILES['avatar']['name']);
        $extensionFile = $musor[count($musor) - 1];

        if($extensionFile == "jpg" || $extensionFile == "png" || $extensionFile = "bmp") {

            if (move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile)) {
                echo json_encode(array(
                    'success' => true,
                    'patch' => Yii::app()->params['images_user'] . '/' . $this->getUserId() . '/temp_avatar.jpg',
                    'msg' => Controller::trans("Upload is Ok!"),
                    'filename' => 'temp_avatar.jpg',
                ));
            } else {
                echo json_encode(array(
                    'success' => false,
                    'msg' => 'File attack',
                ));
            }
        } else {
            echo json_encode(array(
                'success' => false,
                'msg' => 'You Invalid',
            ));
        }

    }

    public function actionPoints() {
        if(isset($_GET['gameid'])) {
            $user_model = new User();
            $user_model = $user_model->loadModel();

        }
    }

    public function loadModel()
    {

        if($this->_model===null)
        {
            $profile_id = (isset($_GET['id'])) ? $_GET['id'] : $this->getUserId();
            $model = User::model()->findByPk($profile_id);

            if($model !== null && $model->profile === null) {

                $model->profile = new Profile();
                $model->profile->user_id = $profile_id;
                $model->profile->save();
            }

            $this->_model = $model;


            if($this->_model===null)
                throw new CHttpException(404, Controller::trans('The requested page does not exist.'));
        }

        return $this->_model;
    }


}
