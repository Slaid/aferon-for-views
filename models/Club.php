<?php

class Club extends CActiveRecord
{
    /**
     * The followings are the available columns in table 'tbl_tag':
     * @var integer $id
     * @var string $name
     * @var integer $frequency
     */
    public $name = null;

    public $postPerPage = 9;

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{club}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('sname', 'required'),
            //array('sname', 'unique'),
            //array('trackId, kind, artistId, features, supportedDevices, isGameCenterEnabled, screenshotUrls, ipadScreenshotUrls, artworkUrl60, artworkUrl512, artistViewUrl, artistName, price, version, description, currency, genres, genreIds, releaseDate, sellerName, bundleId, trackName,  primaryGenreName, primaryGenreId, releaseNotes, formattedPrice, wrapperType, trackCensoredName, lang, fileSizeBytes, sellerUrl, contentAdvisoryRating, averageUserRatingForCurrentVersion, userRatingCountForCurrentVersion, artworkUrl100, trackViewUrl, trackContentRating, averageUserRating, userRatingCount', 'safe'),
            //array('frequency', 'numerical', 'integerOnly'=>true),
            //array('name', 'length', 'max'=>128),
            //array('trackId+lang', 'application.extensions.uniqueMultiColumnValidator'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'promo' => array(self::HAS_MANY, 'ClubPromotion', array('club_id' => 'id', 'lang' => 'lang')),
        );
    }

    public function getPromo($type = 'fun-art') {
        $result = array();
        foreach ($this->promo as $data) {
            if($data->type == $type) {
                $result[] = $data;
            }
        }
        return $result;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            "id" => "id",
            "lang" => "Language",
            "name" => "Club name",
            "description" => "Description",
            "meta_description" => "Meta description",
            "url_developer" => "Developer Url",
            "sname" => "Patch Name",
        );
    }

    public static function snameToName($sname) {
        $sname = str_replace("-", " ", $sname);
        if (!function_exists('mb_ucfirst') && extension_loaded('mbstring'))
        {
            /**
             * mb_ucfirst - преобразует первый символ в верхний регистр
             * @param string $str - строка
             * @param string $encoding - кодировка, по-умолчанию UTF-8
             * @return string
             */
            function mb_ucfirst($str, $encoding='UTF-8')
            {
                $str = mb_ereg_replace('^[\ ]+', '', $str);
                $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).
                    mb_substr($str, 1, mb_strlen($str), $encoding);
                return $str;
            }
        }
        return mb_ucfirst($sname);
    }

    public static function nameToSName($name) {
        //preg_match_all("//");
    }


    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('lang', $this->lang);

        if($this->name) {
            $criteria->addSearchCondition('name', $this->name);
        }

        return new CActiveDataProvider('Club', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->postPerPage,
            ),
            'sort' => array(
                'defaultOrder' => 'date, rank DESC',
            ),
        ));
    }

}