<?php

class ClubPromotion extends CActiveRecord
{
    /**
     * The followings are the available columns in table 'tbl_tag':
     * @var integer $id
     * @var string $name
     * @var integer $frequency
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{club_promotion}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('sname', 'required'),
            //array('sname', 'unique'),
            //array('trackId, kind, artistId, features, supportedDevices, isGameCenterEnabled, screenshotUrls, ipadScreenshotUrls, artworkUrl60, artworkUrl512, artistViewUrl, artistName, price, version, description, currency, genres, genreIds, releaseDate, sellerName, bundleId, trackName,  primaryGenreName, primaryGenreId, releaseNotes, formattedPrice, wrapperType, trackCensoredName, lang, fileSizeBytes, sellerUrl, contentAdvisoryRating, averageUserRatingForCurrentVersion, userRatingCountForCurrentVersion, artworkUrl100, trackViewUrl, trackContentRating, averageUserRating, userRatingCount', 'safe'),
            //array('frequency', 'numerical', 'integerOnly'=>true),
            //array('name', 'length', 'max'=>128),
            //array('trackId+lang', 'application.extensions.uniqueMultiColumnValidator'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'club' => array(self::BELONGS_TO, 'Club', array('club_id' => 'id', 'lang' => 'lang')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            "id" => "id",
            "lang" => "Language",
            "name" => "Club name",
            "description" => "Description",
            "meta_description" => "Meta description",
            "promotion" => "Promotion ID",
            "url_developer" => "Developer Url",
        );
    }


    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('lang', $this->lang);

        if($this->name) {
            $criteria->addSearchCondition('name', $this->name);
        }

        return new CActiveDataProvider('Club', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->postPerPage,
            ),
            'sort' => array(
                'defaultOrder' => 'date, rank DESC',
            ),
        ));
    }

}