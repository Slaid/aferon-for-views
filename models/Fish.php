<?php

class Fish
{
    public $imagesList = array();

    public static function getParams($string, $gameid, $folder) {


        $url = Parsing::parseTemplate(
            Configuration::get($string), array(
                'xml_server' => Configuration::get('xml_server'),
                'username' => Configuration::get('username'),
                'locale' => Controller::$currentLang,
                'folder' => $folder,
                'gameid' => $gameid,
                'channel_param' => Configuration::get('channel_param'),
                'identifier' => Configuration::get('identifier'),
                'channel' => Configuration::get('channel'),
                'identifier_param' => Configuration::get('identifier_param'),
                'productid' => "1",
                'bfg_site_id' => "1",
                'bfg_server' => Configuration::get('bfg_servers', Controller::$currentLang),
                'online_games_folder' => Configuration::get('bfg_online_games_folders', Controller::$currentLang),
                'bfg_store_server' => Configuration::get('bfg_store_servers', Controller::$currentLang),
                'download_games_folder' => Configuration::get('bfg_download_games_folders', Controller::$currentLang),
            )
        );


        return $url;
    }

    public static function affUrl($url) {
        $url = $url . sprintf('?channel=%s&identifier=%s', Configuration::get('channel'), Configuration::get('identifier'));
        $url = Yii::app()->createUrl('game/nofolow', array("url" => base64_encode($url)));
        return $url;
    }

    public static function model() {
        return new fish();
    }

    public static function load($exUrl) {
        $exUrl = str_replace("ja.","jp.",$exUrl);
        $md5 = md5($exUrl);
        $imagesPath = Yii::app()->basePath . '/../images/md5/' . $md5 . '.jpg';
        if(!is_file($imagesPath)) {
            file_put_contents($imagesPath, file_get_contents($exUrl));
        }
        return "/images/md5/" . $md5 . ".jpg";
    }

    public static function images($foldername, $imgName = false) {
        if($imgName !== false) {
            $images = self::images($foldername);
            return (isset($images[$imgName]) ? $images[$imgName] : "");
        }

        $imagesPath = Yii::app()->basePath . '/../data/' . $foldername . '/';
        list($lang, $gamename) = explode("_", $foldername);
        return array(
            "screen1" => (is_file($imagesPath . "screen1.jpg")) ?  "/data/".$foldername."/screen1.jpg" : "http://cdn-games.bigfishsites.com/".$foldername."/screen1.jpg",
            "screen2" => (is_file($imagesPath . "screen2.jpg")) ?  "/data/".$foldername."/screen2.jpg" : "http://cdn-games.bigfishsites.com/".$foldername."/screen2.jpg",
            "screen3" => (is_file($imagesPath . "screen3.jpg")) ?  "/data/".$foldername."/screen3.jpg" : "http://cdn-games.bigfishsites.com/".$foldername."/screen3.jpg",
            "feature" => (is_file($imagesPath ."/". $gamename . "_feature.jpg")) ?  "/data/".$foldername."/".$gamename."_feature.jpg" : "http://cdn-games.bigfishsites.com/".$foldername."/".$gamename."_feature.jpg",
       );

    }

    public static function feature_img($gamename, $foldername) {
        $imagesPath = Yii::app()->basePath . '/../data/' . $foldername . '/';
        return (is_file($imagesPath ."/". $gamename . "_feature.jpg")) ?  "/data/".$foldername."/".$gamename."_feature.jpg" : "http://cdn-games.bigfishsites.com/".$foldername."/".$gamename."_feature.jpg";
    }

    public static function resize($src, $dest, $width, $height, $type = false)
    {
        $rgb=0xFFFFFF;
        $quality=95;
        if (!file_exists($src)) return false;

        $size = getimagesize($src);

        if ($size === false) return false;

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
        $icfunc = "imagecreatefrom" . $format;
        if (!function_exists($icfunc)) return false;

        $x_ratio = $width / $size[0];
        $y_ratio = $height / $size[1];

        $ratio       = min($x_ratio, $y_ratio);

        $use_x_ratio = ($x_ratio == $ratio);

        $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
        $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
        $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
        $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);


        $isrc = $icfunc($src);
        if($type) {
            $new_left = 0;
            $new_top = 0;
            $idest = imagecreatetruecolor($new_width, $new_height);
        } else {
            $idest = imagecreatetruecolor($width, $height);
        }

        imagefill($idest, 0, 0, $rgb);
        imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
            $new_width, $new_height, $size[0], $size[1]);

        imagejpeg($idest, $dest, $quality);

        imagedestroy($isrc);
        imagedestroy($idest);

        return true;

    }


    public static $syndb = null;
    public static $cc = null;

    public static function loaddb() {
        if(Fish::$syndb == null) {
            $lines =  explode("\n", file_get_contents(Controller::rootDir() . '/eng_syn.txt'));

            foreach($lines as $line) {
                list($key, $vals) = explode("|", $line);
                $arr = explode(",", $vals);
                $bb = array();
                foreach($arr as $ar) {
                    if(trim($ar) != "") {
                        $bb[] = $ar;
                    }
                }
                Fish::$syndb[$key] = $bb;
            }
        }
    }

    public static function rep($text = array()) {
        $text = $text[0];

        if(isset(Fish::$syndb[$text])) {
            if(Fish::$cc != null && Fish::$cc > 1) {
                for($i = 0; $i < Fish::$cc; $i++) {
                    $textArr[] = Fish::$syndb[$text][rand(0, count(Fish::$syndb[$text]) -1)];
                }
                $text = "{" . implode("|", $textArr) . "}";
            } else {
                $text = Fish::$syndb[$text][rand(0, count(Fish::$syndb[$text]) -1)];

            }

        }
        return $text;
    }

    public static function syn($text) {
        $res = preg_replace_callback("/[a-zA-Z0-9]+/si", 'Fish::rep', $text);
        return $res;
    }


    public static function unic($text, $count = false) {

        if($count != false) {
            Fish::$cc = $count;
        }

        Fish::loaddb();
        return Fish::syn($text);
    }
}


?>