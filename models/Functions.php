<?php
class Functions {

    public static function getSmtp($server = "aferon") {
        $smtpServer = array(
            "aferon" => array(
                "host" => "smtp.aferon.com",
                "debug" => 0,
                "auth" => true,
                "port" => 25,
                "charset" => "utf8",
                "username" => "admin@aferon.com",
                "name"  => "AFeRon Games :P",
                "password" => "BiG162534",
                "addreply" => "admin@aferon.com",
                "replyto" => "<AFeRon> admin@aferon.com"
            ),
            "mail" => array(
                "host" => "smtp.mail.ru",
                "debug" => 0,
                "auth" => true,
                "port" => 2525,
                "charset" => "utf8",
                "username" => "refresh-91@bk.ru",
                "name"  => "AFeRon Games :P",
                "password" => "!qwe123",
                "addreply" => "refresh-91@bk.ru",
                "replyto" => "<AFeRon> refresh-91@bk.ru"
            )
        );
        return $smtpServer[$server];
    }

    public static function smtpmail($to, $subject, $content, $attach=false, $server = "aferon")
    {

        $__smtp = self::getSmtp($server);

        $path = dirname(__FILE__).'/../../xml/engine/';
        require_once($path . 'class/class.phpmailer.php');

        $mail = new PHPMailer(true);

        $mail->IsSMTP();
        try {
            $mail->Host       = $__smtp['host'];
            $mail->SMTPDebug  = $__smtp['debug'];
            $mail->SMTPAuth   = $__smtp['auth'];
            $mail->CharSet    = $__smtp['charset'];
            $mail->Port       = $__smtp['port'];
            $mail->Username   = $__smtp['username'];
            $mail->Password   = $__smtp['password'];
            $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
            $mail->AddAddress($to);
            $mail->SetFrom($__smtp['addreply'], $__smtp['name']);
            $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
            $mail->Subject = htmlspecialchars($subject);
            $mail->MsgHTML($content);



            if($attach)  $mail->AddAttachment($attach);

            $mail->Send();

            return true;

        } catch (phpmailerException $e) {

            return $e->errorMessage();

        } catch (Exception $e) {

            return  $e->getMessage();

        }
    }
}
?>