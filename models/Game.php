<?php

/**
 * Class Post
 */
class Game extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

    const PLATFORM_PC = "pc";
    const PLATFORM_MAC = "mac";
    const PLATFORM_OG = "og";



    public $platform = self::PLATFORM_PC;
    public $postPerPage = '9';

    public $tag = false;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    /**
     * @param int $platformId
     * @return mixed
     */
    public static function getPlatforms($platform = null) {

        if($platform) {
            $platforms = self::getPlatforms();
            return (isset($platforms[$platform])) ? $platforms[$platform] : null;
        }

        return array(
            self::PLATFORM_PC => Controller::trans("PC Games"),
            self::PLATFORM_MAC => Controller::trans("MAC Games"),
            self::PLATFORM_OG => Controller::trans("Online Games"),
        );

    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{games}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gamename, meddesc, platform, lang', 'required'),
			array('platform', 'in', 'range' => array_keys(Game::getPlatforms())),
			array('gamename', 'length', 'max' => 255),
            array('create_time', 'date', 'format' => 'yyyy-M-d H:m:s'),
            array('gameid+platform+lang', 'application.extensions.uniqueMultiColumnValidator'),
            array('sname, gameid, oggameid, macgameid, pcgameid, rdate, rsname, family, familyid, productid, genreid, allgenreid, shortdesc, longdesc, bullet1, bullet2, bullet3, bullet4, bullet5, foldername, price, hasdownload, hasvideo, hasflash, hasdwfeature, dwwidth, dwheight, gamerank, releasedate, gamesize, systemreq, onlineiframeheight, onlineiframewidth, author_id, create_time', 'safe'),

            array('gamename, platform, lang', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			//'comments' => array(self::HAS_MANY, 'Comment', 'id', 'condition' => 'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			//'commentCount' => array(self::STAT, 'Comment', 'id', 'condition' => 'status='.Comment::STATUS_APPROVED),
            'genre' => array(self::BELONGS_TO, 'Genre', array('genreid' => 'genreid', 'lang' => 'lang')),
            'sync' => array(self::HAS_ONE, 'Sync', array('gameid' => 'gameid')),
            'comments' => array(self::HAS_MANY, 'Gcomments', array('gameid' => 'gameid'), 'limit' => 5, 'order'=>'date DESC'),
            'reviews' => array(self::HAS_MANY, 'Reviews', array('game_id' => 'gameid')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'gameid' => 'Game ID',
            'sname' => 'Url Name',
			'gamename' => 'Game Name',
			'lang' => 'Language',
			'platform' => 'Platform',
			'oggameid' => 'Online Game ID',
			'macgameid' => 'Mac Game ID',
			'pcgameid' => 'PC Game ID',
            'family' => 'Family',
            'familyid' => 'Family ID',
            'productid' => 'Product ID',
            'genreid' => 'Genre ID',
            'allgenreid' => 'All Genre ID',
            'shortdesc' => 'Short Description',
            'meddesc' => 'Medium Description',
            'longdesc' => 'Long Description',
            'bullet1' => 'Bullet 1',
            'bullet2' => 'Bullet 2',
            'bullet3' => 'Bullet 3',
            'bullet4' => 'Bullet 4',
            'bullet5' => 'Bullet 5',
            'foldername' => 'Folder Name',
            'price' => 'Price',
            'hasdownload' => 'Has download',
            'hasvideo' => 'Has video',
            'hasflash' => 'Has Flash',
            'hasdwfeature' => 'Has DW Feature',
            'dwwidth' => 'DW width',
            'dwheight' => 'DW height',
            'gamerank' => 'Game Rank',
            'releasedate' => 'Release Date',
            'gamesize' => 'Game Size',
            'systemreq' => 'System Request',
            'onlineiframeheight' => 'Online Iframe Height',
            'onlineiframewidth' => 'Online Iframe Width',
            'author_id' => 'Author ID',
            'create_time' => 'Create Time',
            'img_is_load' => 'Images is Load?',
            'rating' => 'Rating',
            'count_rating' => 'Count Rating',
            'rdate' => 'Rewrite date',
            'rsname' => "Rewrite sname",
		);
	}

    public function getShortDescription(){
        //return $this->shortdesc;
        return (isset($this->sync->short)) ? $this->sync->short : $this->shortdesc;
    }

    public function getMedDescription(){
        //return $this->meddesc;
        return (isset($this->sync->med)) ? $this->sync->med : $this->meddesc;
    }

    public function getLongDescription(){
        //return $this->longdesc;
        return (isset($this->sync->long)) ? $this->sync->long : $this->longdesc;
    }
	
	public function getSeoTitle($default = ''){
        //return $this->longdesc;
        return (isset($this->sync->seo_title)) ? $this->sync->seo_title : $default;
    }
	
	public function getSeoKey($default = ''){
        //return $this->longdesc;
        return (isset($this->sync->seo_key)) ? $this->sync->seo_key : $default;
    }

    public function getGameName(){
        //return $this->gamename;
        return (isset($this->sync->gamename)) ? $this->sync->gamename : $this->gamename;
    }

    public function gamesize()
    {
        $file_size = $this->gamesize;
        if ( 1073741824 <= $file_size )
        {
            $file_size = ( round( $file_size / 1073741824 * 100 ) / 100 )." Gb";
            return $file_size;
        }
        if ( 1048576 <= $file_size )
        {
            $file_size = ( round( $file_size / 1048576 * 100 ) / 100 )." Mb";
            return $file_size;
        }
        if ( 1024 <= $file_size )
        {
            $file_size = ( round( $file_size / 1024 * 100 ) / 100 )." Kb";
            return $file_size;
        }
        $file_size .= " byte";
        return $file_size;
    }

    public function getGenre($param = "name") {

        if(isset($this->genre)) {
            return $this->genre->$param;
        } else {
            $model = $this->genreCreate($this->genreid);
            return "Is created";
        }
    }

    protected function genreCreate($genreid = null) {
        $model = new Genre();
        $model->attributes = Genre::model()->findByAttributes(array(
            'lang' => 'en',
            'genreid' => $genreid,
        ))->getAttributes();
        $model->lang = Controller::$currentLang;
        $model->save();
    }



	/**
	 * @return string the URL that shows the detail of the post
	 */

    public function getSName() {
        if(isset($this->rsname) && $this->rsname != "") {
            return $this->rsname;
        }

        if(isset($this->sname) && $this->sname != "") {
            return $this->sname;
        }

    }

	public function getUrl()
	{

		return Yii::app()->createUrl('game/view', array(
            'gameid' => $this->getSName(),
		));

	}

    public function getDownloadUrl($platform = Game::PLATFORM_PC) {
        return Yii::app()->createUrl('game/download', array(
            'gameid' => $this->gameid,
        ));
    }

    public function getRelative() {

        $model = new $this;
        $model = $model->findAllByAttributes(array(
            'genreid' => $this->genreid,
            'lang' => $this->lang,
            'platform' => $this->platform,
        ), array("limit" => "5", "order" => "gamerank DESC"));
        return $model;
    }

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;


        $criteria->compare('lang', $this->lang);
		$criteria->compare('platform', $this->platform);
        $criteria->compare('genreid', $this->genreid);
        $criteria->compare('hasdwfeature', $this->hasdwfeature);

        if($this->tag) {
            $criteria->addSearchCondition('allgenreid', $this->tag);
        }

		return new CActiveDataProvider('Game', array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->postPerPage,
            ),
			'sort' => array(
				'defaultOrder' => 'platform, releasedate DESC',
			),
		));
	}


}