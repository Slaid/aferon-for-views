<?php

class Ios extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_tag':
	 * @var integer $id
	 * @var string $name
	 * @var integer $frequency
	 */
    public $name = '';

    public $data = array();

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ios}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sname', 'required'),
            array('sname', 'unique'),
            array('trackId, kind, artistId, features, supportedDevices, isGameCenterEnabled, screenshotUrls, ipadScreenshotUrls, artworkUrl60, artworkUrl512, artistViewUrl, artistName, price, version, description, currency, genres, genreIds, releaseDate, sellerName, bundleId, trackName,  primaryGenreName, primaryGenreId, releaseNotes, formattedPrice, wrapperType, trackCensoredName, lang, fileSizeBytes, sellerUrl, contentAdvisoryRating, averageUserRatingForCurrentVersion, userRatingCountForCurrentVersion, artworkUrl100, trackViewUrl, trackContentRating, averageUserRating, userRatingCount', 'safe'),
			//array('frequency', 'numerical', 'integerOnly'=>true),
			//array('name', 'length', 'max'=>128),
            //array('trackId+lang', 'application.extensions.uniqueMultiColumnValidator'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'sync' => array(self::HAS_ONE, 'IosTranslate', array('ios_id' => 'id')),
		);
	}

    public function getDescription() {
        if(empty($this->sync)) {
            $model = new IosTranslate();
            $model->ios_id = $this->id;
            $model->lang = Controller::$currentLang;
            $model->description = Fish::unic($this->description);
            $model->save();
        }

        return (isset($this->sync->description)) ? $this->sync->description : $this->description;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{

		return array(
            'artistId' => "ID",
            'kind' => null,
            'features' => null,
            'supportedDevices' => null,
            'isGameCenterEnabled' => null,
            'screenshotUrls' => null,
            'ipadScreenshotUrls' => null,
            'artworkUrl60' => null,
            'artworkUrl512' => null,
            'artistViewUrl' => null,
            'artistName' => null,
            'price' => null,
            'version' => null,
            'description' => null,
            'currency' => null,
            'genres' => null,
            'genreIds' => null,
            'releaseDate' => null,
            'sellerName' => null,
            'bundleId' => null,
            'trackId' => null,
            'trackName' => null,
            'primaryGenreName' => null,
            'primaryGenreId' => null,
            'releaseNotes' => null,
            'formattedPrice' => null,
            'wrapperType' => null,
            'trackCensoredName' => null,
            'lang' => null,
            'fileSizeBytes' => null,
            'sellerUrl' => null,
            'contentAdvisoryRating' => null,
            'averageUserRatingForCurrentVersion' => null,
            'userRatingCountForCurrentVersion' => null,
            'artworkUrl100' => null,
            'trackViewUrl' => null,
            'trackContentRating' => null,
            'averageUserRating' => null,
            'userRatingCount' => null,

		);

	}




}