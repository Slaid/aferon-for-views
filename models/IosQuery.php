<?php

class IosQuery extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ios_query}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('query', 'required'),
            array('query', 'unique'),
            array('id, allgameid, allgameid, img, description', 'safe'),
			//array('frequency', 'numerical', 'integerOnly'=>true),
			//array('name', 'length', 'max'=>128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'game' => array(self::HAS_MANY, 'Game', array('genreid' => 'genreid', 'lang' => 'lang')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{

		return array(
             /*
            'artistId' => "ID",
            'kind' => null,
            'features' => null,
            'supportedDevices' => null,
            'isGameCenterEnabled' => null,
            'screenshotUrls' => null,
            'ipadScreenshotUrls' => null,
            'artworkUrl60' => null,
            'artworkUrl512' => null,
            'artistViewUrl' => null,
            'artistName' => null,
            'price' => null,
            'version' => null,
            'description' => null,
            'currency' => null,
            'genres' => null,
            'genreIds' => null,
            'releaseDate' => null,
            'sellerName' => null,
            'bundleId' => null,
            'trackId' => null,
            'trackName' => null,
            'primaryGenreName' => null,
            'primaryGenreId' => null,
            'releaseNotes' => null,
            'formattedPrice' => null,
            'wrapperType' => null,
            'trackCensoredName' => null,
            'languageCodesISO2A' => null,
            'fileSizeBytes' => null,
            'sellerUrl' => null,
            'contentAdvisoryRating' => null,
            'averageUserRatingForCurrentVersion' => null,
            'userRatingCountForCurrentVersion' => null,
            'artworkUrl100' => null,
            'trackViewUrl' => null,
            'trackContentRating' => null,
            'averageUserRating' => null,
            'userRatingCount' => null,
             */
		);

	}




}