<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
    public $email;
    public $confirm_password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
            array('username, password, email, confirm_password', 'required', 'on' => 'register'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),

            array('email', 'email'),
            array('email','unique', 'className' => 'User'),

			// password needs to be authenticated
			array('password', 'authenticate', 'on' => 'login'),
            //register
            //array('username, password, email, confirm_password', 'required', 'on' => 'register'),
            array('password', 'compare', 'compareAttribute' => 'confirm_password', 'on' => 'register'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
            'email' => 'Email',
            'confirm_password' => 'Password Confirm',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		$this->_identity=new UserIdentity($this->username,$this->password);
		if(!$this->_identity->authenticate())
			$this->addError('password','Incorrect username or password.');
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}

    public function registration() {
        if($this->_identity===null)
        {
            $this->_identity=new UserIdentity($this->username,$this->password);
            $this->_identity->email = $this->email;
            $this->_identity->register();
        }


        if($this->_identity->errorCode===UserIdentity::ERROR_NONE) {
            return true;
        } else {
            $this->addError('username', 'Username is exists');
        }
    }
}
