<?php

class Profile extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{profile}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
            array('nick, points, avatar, sigma', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::HAS_ONE, 'User', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User ID',
			'Nick' => 'Nick',
			'points' => 'Game Rating',
			'avatar' => 'Avatar',
			'sigma' => 'Sigma',
		);
	}


    public static function getAvatar($user_id) {
        $path = Yii::app()->params['images_user'];
        return (!is_file(Controller::rootDir() . Yii::app()->params['images_user'] . '/'. $user_id . "/avatar.jpg")) ? '/images/default_avatar.jpg' : Yii::app()->params['images_user'] . '/'. $user_id . '/avatar.jpg';
    }
}