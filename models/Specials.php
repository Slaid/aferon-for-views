<?php

class Specials extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_tag':
	 * @var integer $id
	 * @var string $name
	 * @var integer $frequency
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{specials}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gameid, gamename, images, content, lang, platform', 'required'),
            array('logo_url, genreid, tagline, genrename, allgenreid, offer_start_date, offer_end_date, link, price, platform, lang', 'safe'),
			//array('frequency', 'numerical', 'integerOnly'=>true),
			//array('name', 'length', 'max'=>128),
            array('gameid+platform+lang+content', 'application.extensions.uniqueMultiColumnValidator'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'game' => array(self::BELONGS_TO, 'Game', 'gameid'),
            'translate' => array(self::HAS_ONE, 'Translate', array('gameid' => 'gameid')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'logo_url' => 'Logo Url',
            'tagline' => 'Tag Line',
            'gameid' => 'Game ID',
            'images' => 'Images',
            'gamename' => 'Game Name',
            'genreid' => 'Genre ID',
            'genrename' => 'Genre Name',
            'allgenreid' => 'All Genre ID',
            'offer_start_date' => 'Start Date',
            'offer_end_date' => 'End Date',
            'link' => 'Link',
            'price' => 'Price',
            'lang' => 'Lang',
            'platform' => 'Platform',

		);
	}


	/**
	 * Returns tag names and their corresponding weights.
	 * Only the tags with the top weights will be returned.
	 * @param integer the maximum number of tags that should be returned
	 * @return array weights indexed by tag names.
	 */
	public function findSpecials()
	{
        $models = array();

        $models['catch'] = $this->findByAttributes(array(
            'lang' => Controller::$currentLang,
            'platform' => Controller::currentPlatform(),
        ), array(
            "order" => "id DESC",
            "limit" => 1,
            "condition" => "content='catch'",
        ));

        $models['dailydeal'] = $this->findByAttributes(array(
            'lang' => Controller::$currentLang,
            'platform' => Controller::currentPlatform(),
        ), array(
            "order" => "id DESC",
            "limit" => 1,
            "condition" => "content='dailydeal'",
        ));

		return $models;
	}

}