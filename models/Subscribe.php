<?php

class Subscribe extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_tag':
	 * @var integer $id
	 * @var string $name
	 * @var integer $frequency
	 */
    const STATUS_ACTIVE = 0;

    const STATUS_NOT_ACTIVE = 1;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subscribe}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email', 'required'),
            array('email', 'unique'),
            array('email', 'email'),
            //array('status', 'in' => array_keys(Subscribe::getStatus())),
            //array('trackId, kind, artistId, features, supportedDevices, isGameCenterEnabled, screenshotUrls, ipadScreenshotUrls, artworkUrl60, artworkUrl512, artistViewUrl, artistName, price, version, description, currency, genres, genreIds, releaseDate, sellerName, bundleId, trackName,  primaryGenreName, primaryGenreId, releaseNotes, formattedPrice, wrapperType, trackCensoredName, lang, fileSizeBytes, sellerUrl, contentAdvisoryRating, averageUserRatingForCurrentVersion, userRatingCountForCurrentVersion, artworkUrl100, trackViewUrl, trackContentRating, averageUserRating, userRatingCount', 'safe'),
			//array('frequency', 'numerical', 'integerOnly'=>true),
			//array('name', 'length', 'max'=>128),
            //array('trackId+lang', 'application.extensions.uniqueMultiColumnValidator'),
		);
	}

    public static function getStatus() {
        return array(
            Subscribe::STATUS_ACTIVE => "Active",
            Subscribe::STATUS_NOT_ACTIVE => "Not active",
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		//return array(
        //    'sync' => array(self::HAS_ONE, 'IosTranslate', array('ios_id' => 'id')),
		//);
        return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{

		return array(
            "id" => "ID",
            "name" => "Your name",
            "email" => "Email address",
		);

	}




}