<?php

class Sync extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_tag':
	 * @var integer $id
	 * @var string $name
	 * @var integer $frequency
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{translate}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('key, lang', 'required'),
            //array('val', 'safe'),
            //array('key', 'unique'),
            //array('key+lang', 'application.extensions.uniqueMultiColumnValidator'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'game' => array(self::HAS_ONE, 'Game', array('gameid' => 'gameid')),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'id' => 'Id',
		);
	}


}