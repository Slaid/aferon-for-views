<div class="club-info">
    <?php foreach($model as $data) : ?>

        <div class="promo-material">
            <div class="<?php echo $data->type; ?>">
                <div class="img">
                    <img class="zoom" src="<?php echo $data->url; ?>"/>
                </div>
                <div class="name" title="<?php echo $data->description; ?>"><?php echo $data->name; ?></div>
            </div>
        </div>

    <?php endforeach; ?>
</div>
