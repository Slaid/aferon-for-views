<div class="club-item">
    <div class="pull-left">
        <img src="/images/default_avatar.jpg" style="height: 100px;">
    </div>
    <div class="pull-left margin-left">
        <a href="<?php echo Yii::app()->createUrl("club/view", array("sname" => $data->sname)); ?>"><h1><?php echo $data->name; ?></h1></a>
        <p><?php echo $data->description; ?></p>
        <div class="smeta">
            <a href="">Comments (2)</a>
            <a href="">Materials (12)</a>
            <a href="">Subscribers (2011)</a>
        </div>
    </div>

</div>
