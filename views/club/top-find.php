<div class="find-club overflow">

    <div class="label-find-club">
        <div style="position: relative;">
            <div class="auth">
                <?php
                if(Yii::app()->user->isGuest) {
                    Yii::app()->eauth->renderWidget();
                }
                ?>
            </div>
            <h1><?php echo Controller::trans("Find your gaming community"); ?></h1>
            <?php  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'searchForm',
                'type'=>'search',
                'action' => Yii::app()->createUrl('club/index'),
                //'htmlOptions'=>array('class'=>'well'),
            ));  ?>

            <div style="margin: 10px;">
                <b style="color: green"><?php echo Controller::trans("Example"); ?>: </b> Angry Birds
            </div>
            <?php echo $form->textFieldRow(Club::model(), 'name', array('class'=>'input-medium', 'style' => 'width: 420px')); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'htmlOptions' => array('class' => 'btn-info'), 'label'=>'Find')); ?>

            <?php $this->endWidget(); ?>
        </div>
    </div>
    <img style="position: absolute; top:0; height: 196px; right: 0;" src="/css/images/elf.jpg" alt="<?php echo Controller::trans("girl elf"); ?>" />
</div>