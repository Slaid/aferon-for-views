<?php
$this->pageTitle = $model->sname . " " . Controller::trans("fun art club.");
$this->description = $model->meta_description;

?>

<div class="club">

    <?php $this->renderPartial('top-find'); ?>

    <div class="club-body">

        <div class="tabs">
            <ul>

                <li class="active"><a href="#tab1" class="tab-active btn btn-warning"><i class="icon-photo"></i><?php echo Controller::trans("Photo"); ?></a></li>
                <li><a href="#tab2" class="tab-active btn btn-success"><i class="icon-video"></i><?php echo Controller::trans("Video"); ?></a></li>
                <li><a href="#tab3" class="tab-active btn btn-info"><i class="icon-music"></i><?php echo Controller::trans("Music"); ?></a></li>
                <li><a href="#tab4" class="tab-active btn"><i class="icon-news"></i><?php echo Controller::trans("Promotion"); ?></a></li>

                <li style="float: right;"><a class="btn" href="<?php echo Yii::app()->createUrl('ios/find', array('name' => $model->name)); ?>"><i class="icon-filefind"></i><?php echo Controller::trans("Download this game!"); ?></a></li>
            </ul>
        </div>

        <div class="look-tabs">

            <div id="tab1" class="tab photo-tab active">
                <h2><?php echo Controller::trans("Fun Art wallpapers"); ?> <?php echo $model->name; ?></h2>

                <?php $this->renderPartial('_promotion', array('model' => $model->getPromo('fun-art'))); ?>
            </div>

            <div id="tab2" class="tab video-tab">
                <h2><?php echo Controller::trans("Video clip"); ?> <?php echo $model->name; ?></h2>

                <?php $this->renderPartial('_promotion', array('model' => $model->getPromo('video'))); ?>
            </div>

            <div id="tab3" class="tab music-tab">
                <h2><?php echo Controller::trans("Music track"); ?> <?php echo $model->name; ?></h2>

                <?php $this->renderPartial('_promotion', array('model' => $model->getPromo('music'))); ?>
            </div>
            <div id="tab4" class="tab news-tab">
                <h2><?php echo Controller::trans("Information about game"); ?> <?php echo $model->name; ?></h2>

                <?php $this->renderPartial('_promotion', array('model' => $model->getPromo('info'))); ?>
            </div>

        </div>

        <?php $this->renderPartial('../ios/share_buttons'); ?>
    </div>
</div>

<script>
    $(function() {

        var typeData = 'video';
        $.ajax({
            url: '/xml/script/parser_content.php',
            dataType: 'JSON',
            type: 'POST',
            data: {type: typeData, name: '<?php echo $model->name; ?>' },
            success: function (data) {
                alert(data);
            }
        });

    });
</script>