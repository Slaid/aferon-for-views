<?php
if($data->platform == Game::PLATFORM_PC)
{
    echo CHtml::tag("a",
        array(
            "href" => $data->getDownloadUrl(),
            "_target" => "blank",
            'rel' => 'nofollow',
            "class" => "btn btn-warning btn-small",
            "title" => Controller::trans("Download pc game") . " " . $data->gamename
        ), Controller::trans("Download PC"));
}

if($data->platform == Game::PLATFORM_MAC)
{
    echo CHtml::tag("a",
        array(
            "href" => $data->getDownloadUrl(),
            "_target" => "blank",
            'rel' => 'nofollow',
            "class" => "btn btn-info btn-small",
            "title" => Controller::trans("Download mac game") . " " . $data->gamename
        ), Controller::trans("Download MAC"));
}

if($data->platform == Game::PLATFORM_OG)
{
    echo CHtml::tag("a",
        array(
            "href" => Yii::app()->createUrl('game/online', array('gameid' => $data->getSName())),
            "_target" => "blank",
            "class" => "playOnline btn btn-danger btn-small",
            "title" => Controller::trans("Play online") . " " .  $data->gamename
        ), Controller::trans("Play Now"));
}

?>