<div class="">
    <div class="compare-game-block">
        <div class="images bottom-padding">
            <?php echo CHtml::tag("a", array(
                'href' => $data->getUrl(),
                'title' => "View game " . $data->gamename,
            ), CHtml::tag("img", array(
                "src" => Fish::images($data->foldername, "feature"),
            ))); ?>
        </div>

        <div class="right">
            <div class="label-text">
                <?php echo $data->gamename; ?>
            </div>

            <div class="genre bottom-padding">
                <?php /*
                <div class="icon-<?php echo $data->genre->sname; ?>"></div><?php echo $data->genre->name; ?><br />
                */ ?>
            </div>
            <?php echo CHtml::tag("a",
                array(
                    "href" => $data->getUrl(),
                    "_target" => "blank",
                    "class" => "btn btn-warning btn-mini float-left",
                    "title" => Controller::trans("View game") . " " . $data->gamename,
                ), Controller::trans("View"));
            ?>

        </div>

    </div>
</div>
