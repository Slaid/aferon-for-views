<?php

$this->description = $data->getMedDescription();
$this->image = "http://" . $_SERVER['HTTP_HOST'] . Fish::images($data->foldername, 'screen3');

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(
        Game::getPlatforms($data->platform) => Yii::app()->createUrl('game/games', array('platform' => $data->platform)),
        $data->getGenre("name") => Yii::app()->createUrl('game/games', array('platform' => $data->platform, 'genre' => $data->getGenre("sname"))),
        Controller::trans('Download Game'),
    ),
));


?>
    <div class="singl-post">
        <div class="title" >
            <h1><?php echo CHtml::encode($data->getGameName()); ?></h1>


        </div>


        <div class="content">
            <div class="game-info">
                <div style="overflow: hidden;">
                    <?php

                    $array[] = array(
                        "image" => Fish::images($data->foldername, 'screen1'),
                        "alt" => Controller::trans("Game screenshot") . " 1 " . $data->getGameName(),
                        "label" => Controller::trans("Game screenshot") . " 1 " . $data->getGameName(),
                        // "caption" => $data->shortdesc,
                    );
                    $array[] = array(
                        "image" => Fish::images($data->foldername, 'screen2'),
                        "alt" => Controller::trans("Game screenshot") . " 2 " . $data->getGameName(),
                        "label" => Controller::trans("Game screenshot") . " 2 " . $data->getGameName(),
                        // "caption" => $data->shortdesc,
                    );
                    $array[] = array(
                        "image" => Fish::images($data->foldername, 'screen3'),
                        "alt" => Controller::trans("Game screenshot") . " 3 " . $data->getGameName(),
                        "label" => Controller::trans("Game screenshot") . " 3 " . $data->getGameName(),
                        // "caption" => $data->shortdesc,
                    );


                    ?>

                    <?php $this->widget('bootstrap.widgets.TbCarousel', array(
                        'htmlOptions' => array('class' => "galley"),
                        'items' => $array,
                    ));

                    ?>

                    <div class="property">

                        <div style="padding: 5px 0">
                            <?php if($_SERVER['HTTP_HOST'] != 'localhost') : ?>
                            <div class="google-adsense google-adsense-widget" >
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
								<!-- aferon -->
								<ins class="adsbygoogle"
									 style="display:inline-block;width:250px;height:250px"
									 data-ad-client="ca-pub-9710740179130354"
									 data-ad-slot="2843747982"></ins>
								<script>
								(adsbygoogle = window.adsbygoogle || []).push({});
								</script>
                            </div>
                            <?php endif; ?>
                        </div>


                    </div>


                </div>

                <?php $this->renderPartial('share_buttons'); ?>

                <div class="overflow system">

                    <div class="span-7">
                        <?php
                        if(isset($data->bullet1)) {
                            echo CHtml::tag("b", array(), Controller::trans("Bullet"));
                            echo "<ul>";

                            echo CHtml::tag("li", array() , $data->bullet1);

                            if(isset($data->bullet2)) {
                                echo CHtml::tag("li", array() , $data->bullet2);
                            }
                            if(isset($data->bullet3)) {
                                echo CHtml::tag("li", array() , $data->bullet3);
                            }
                            if(isset($data->bullet4)) {
                                echo CHtml::tag("li", array() , $data->bullet4);
                            }
                            if(isset($data->bullet5)) {
                                echo CHtml::tag("li", array() , $data->bullet5);
                            }
                            echo "</ul>";


                        }
                        ?>


                    </div>
                    <div class="span-7">
                        <?php
                        $request = json_decode($data->systemreq);

                        if($request) {

                            echo CHtml::tag("b", array(), Controller::trans("Min System request"));
                            $array_key_replace_request = array(
                                "sysreqos" => "OS",
                                "sysreqmhz" => "CPU",
                                "sysreqmem" => "Memory",
                                "sysreqdx" => "DiretX",
                                "sysreqhd" => "Hard driver (MB)",
                            );

                            echo "<ul>";
                            foreach($request as $platform => $params) {
                                foreach($params as $key => $req) {
                                    echo "<li>" . str_replace(array_keys($array_key_replace_request), array_values($array_key_replace_request), $key) . " : " . $req . "</li>";
                                }
                            }
                            echo "</ul>";

                        }
                        ?>
                    </div>

                </div>
                <div class="clear"></div>
            </div>

            <div class="long-desc">
                <h2 style="font-size: 14px;" class="label-text">Review for game <?php echo $data->getGameName(); ?></h2>
                 <p><?php echo $data->getLongDescription(); ?></p>
            </div>


            <div class="button-group">
                <?php $this->renderPartial('_buttons', array(
                    'data' => $data,
                )); ?>

                <?php


                    if(isset($data->oggameid)) {
                        echo CHtml::tag("a", array(
                            "href" => Yii::app()->createUrl('game/online', array('gameid' => $data->getSName())),
                            "_target" => "blank",
                            "class" => "playOnline btn btn-danger btn-small",
                            "title" => Controller::trans("Play online game") . " : " . $data->getGameName(),
                            ), Controller::trans("Play Online"));

                    }

                    if(isset($data->macgameid)) {
                        echo CHtml::tag("a", array(
                            "href" => Yii::app()->createUrl('game/download', array('gameid' => $data->macgameid)),
                            "_target" => "blank",
                            "class" => "btn btn-info btn-small",
                            "title" => Controller::trans("Download mac game") . " : " . $data->getGameName(),
                            ),
                            Controller::trans("Download MAC"));

                    }

                    if(isset($data->pcgameid)) {
                        echo CHtml::tag("a", array(
                            "href" => Yii::app()->createUrl('game/download', array('gameid' => $data->pcgameid)),
                            "_target" => "blank",
                            "class" => "btn btn-warning btn-small",
                            "title" => Controller::trans("Download pc game") . " : " . $data->getGameName(),
                            ) , Controller::trans("Download PC"));

                    }

                ?>


                <?php $this->renderPartial('_watch_video', array(
                    'data' => $data,
                )); ?>
                <div class="btn btn-inverse" link="<?php echo Fish::getParams('purchase_game', $data->gameid, $data->foldername); ?>&tryGame=<?php echo $data->gameid; ?>&cid=gamegift" onclick="document.location.href=$(this).attr('link');"><?php echo Controller::trans("Gift Game"); ?></div>

 <br/>


                <br />
            </div>


            <br />
            <br />


            <div class="label-text"><?php echo Controller::trans('Review at a glimpse'); ?></div>
            <div class="well span-15">
                <div class="wall-point">
                    <div class="span-7">
                        <div class="label-text"><?php echo Controller::trans('Genre'); ?> : <?php echo $data->getGenre("name"); ?></div>
                        <p><?php echo $data->getGenre("description"); ?></p>

                        <div class="label-text"><?php echo Controller::trans('Date release'); ?></div>
                        <p><?php echo date('F j, Y', strtotime($data->releasedate)); ?></p>


                    </div>
                    <div class="span-6">


                        <?php if($data->platform != 'og') : ?>
                        <div class="label-text" style="margin-left: 70px;"><?php echo Controller::trans('Five free minutes'); ?></div>
                        <div style="background: url('/css/images/subscribe.png'); width: 229px; height: 44px;">
                            <div style="color: #E4E4E4;  font-size: 10px; margin-left: 185px; padding-top: 2px; text-shadow: 1px 1px 1px #686767; width: 39px;"><?php echo $data->price; ?></div>
                        </div>
						
                        <?php endif; ?>



                        <div class="label-text"><?php echo Controller::trans('Game Rank'); ?></div>
                        <p><?php echo $data->gamerank; ?> <?php echo Controller::trans('points'); ?></p>

                        <div class="label-text"><?php echo Controller::trans('Please rate the game'); ?></div>

                        <b itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                            <span itemprop="ratingValue"><?php echo $data->rating / $data->count_rating; ?></span> /
                            <span itemprop="reviewCount"><?php echo $data->count_rating; ?></span>
                        </b> &nbsp;
                        <b><?php echo Controller::trans('Rate It'); ?>:</b> <div data-productid="<?php echo $data->gameid; ?>" class="rateit" data-rateit-value="<?php echo $data->rating / $data->count_rating; ?>"></div>


                    </div>
                </div>

                <div class="tagline clear">
                    <h2 class="label-text"><?php echo Controller::trans('Tag line'); ?></h2>
                    <?php
                        $genreIds = explode(",", $data->allgenreid);

                        foreach(Controller::$genreList as $genre) {
                           if(in_array($genre->id, $genreIds)) {
                               echo CHtml::tag("a", array(
                                      'href' => Yii::app()->createUrl("game/games", array('tag' => $genre->sname, 'platform' => Controller::currentPlatform())),
                                      'class' => 'tag-genre',
                                      'title' => Controller::trans("View all games with tag") . " " . $genre->name,
                                   ), $genre->name);
                           }
                        }
                    ?>
                </div>
            </div>



            <br />
            <br />

        </div>


        <br /> <br />


    </div>
<div class="clear"></div>