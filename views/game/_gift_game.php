<!-- Button to trigger modal -->
<a href="#gift-model" role="button" class="btn" data-toggle="modal" style="font-size: 30px; color: #ABD622;"><i class="icon-gift"></i><?php echo Controller::trans('Gift Game'); ?></a>

<!-- Modal -->
<div style="width: 800px; margin-left: -400px;" id="gift-model" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">X</button>
        <h3 id="myModalLabel"><?php echo Controller::trans('Gift game you friend'); ?></h3>
    </div>
    <div class="modal-body" style="background: #DAECFF;">
        <div class="pull-left">

            <div id="html-send" style="border: 1px dashed #ccc;">


                <?php $this->renderPartial('html_gift_game', array(
                    'data' => $data,
                )); ?>

            </div>
        </div>
        <div class="pull-left span-6">
            <div class="text-label"><?php echo Controller::trans('Message Header'); ?></div>
            <input type="text" id="html_subject" placeholder="Present for You">
            <div class="text-label"><?php echo Controller::trans('What is your name?'); ?></div>
            <input type="text" id="html_name" placeholder="John">
            <div class="text-label"><?php echo Controller::trans('Friend emails'); ?></div>
            <textarea id="email-list" style="width:200px; max-width: 200px; height:100px; max-height:100px;"></textarea>
            <div id="send-gift" class="btn btn-info"><?php echo Controller::trans('Send Message'); ?></div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo Controller::trans('Close'); ?></button>
        <!--<button class="btn btn-primary">Save changes</button>-->
    </div>
</div>

<script>
    $(function() {
        $('#send-gift').click(function() {
            var subject = $('#html_subject').val();
            if(subject != "" && subject.length > 10) {
                var email_list = $('#email-list').val();
                if(email_list != "" && email_list.length > 7) {
                    var name = $('#html_name').val();
                    if(name != "" && name.length > 3) {
                        $.ajax({
                            url: '/game/gift?gameid=<?php echo $data->gameid; ?>', //your server side script
                            data: { subject: subject, email_list: email_list, name: name}, //our data
                            type: 'POST',
                            success: function (data) {
                               alert("sussecc");
                            }
                        });
                    } else {
                        alert('<?php echo Controller::trans("The name is less than 3 characters"); ?>');
                    }
                } else {
                    alert('<?php echo Controller::trans("Not valid email address"); ?>');
                }
            } else {
                alert('<?php echo Controller::trans("The message header is less than 10 characters"); ?>');
            }
        });
    })
</script>