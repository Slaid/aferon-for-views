<div class="special-item-right" style="border-bottom: 1px solid #ccc;">

    <div class="special-content">
        <div class="lefter">
        <?php

        $segments = explode("_", $data->foldername);
        echo CHtml::tag("a", array(
            "title" => "View more " . $data->getGameName(),
            "href" => $data->getUrl(),
            "class" => 'pull-left',
            "style" => "width: 80px; height: 80px;",
        ), CHtml::tag('img', array(
            'src' => Fish::feature_img($segments[1], $data->foldername),
            'alt' => Controller::trans("Feature screenshot game"). " " . $data->gamename,
        )));

        ?>

        <b class="icon-adventure_large">  </b>
            <span style="font-size: 12px;"><?php echo $data->gamesize(); ?></span>
        </div>


        <div class="special-description">
            <a class="title" href="<?php echo $data->getUrl(); ?>" title="<?php echo Controller::trans("View information about game"); ?> <?php echo $data->getGameName(); ?>"><?php echo $data->getGameName(); ?></a>
                <span class="home_view"><i class="icon-<?php echo $data->getGenre('sname'); ?>"></i>
                     <a href="<?php echo Yii::app()->createUrl('game/games', array('platform' => Controller::currentPlatform(), 'genre' => $data->getGenre('sname'))); ?>" title="<?php echo Controller::trans("View games with genre");?> <?php echo $data->getGenre('name'); ?>"><?php echo $data->getGenre('name'); ?></a>
                </span>

            <a class="btn btn-warning btn-mini" rel="nofollow" href="<?php echo $data->getDownloadUrl(); ?>">Download</a>

        </div>
    </div>
</div>