
<div class="game-block-min pull-left span-4">
    <div class="title">
        <?php echo CHtml::link(CHtml::encode($data->getGameName()), $data->getUrl(), array("title" => Controller::trans("View game") . " " . $data->getGameName())); ?>
    </div>

    <?php

    $segments = explode("_", $data->foldername);
    echo CHtml::tag("a", array(
        "title" => Controller::trans("Play game") . " " . $data->getGameName(),
        "href" => $data->getUrl(),
    ), CHtml::tag('img', array(
        'src' => Fish::feature_img($segments[1], $data->foldername),
        'style' => 'border-radius: 16px; border: 1px solid #ccc; width: 188px;',
    )));
    ?>



    <?php echo CHtml::tag("a",
        array(
            "href" => $data->getUrl(),
            "_target" => "blank",
            "class" => "btn btn-small btn-info",
            "title" => Controller::trans("View more about game") . " : " . $data->getGameName(),
            "id" => "playOnline",
        ), Controller::trans("View")); ?>


    <div data-productid="<?php echo $data->gameid; ?>" class="rateit" data-rateit-value="<?php echo $data->rating / $data->count_rating; ?>"></div>

</div>


<?php $this->n++;
if($this->n % 5 == 0) {
    echo "<div style='clear:both'></div>";
} ?>