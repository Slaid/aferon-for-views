
    <div class="game-block pull-left span-5">
        <div class="title">
            <?php  echo CHtml::link(CHtml::encode($data->getGameName()), $data->getUrl(), array("title" => Controller::trans("View game") . " " . $data->getGameName())); ?>
        </div>

        <?php

        $segments = explode("_", $data->foldername);
        echo CHtml::tag("a", array(
            "title" => Controller::trans("View information about game") . " " . $data->getGameName(),
            "href" => $data->getUrl(),
        ), CHtml::tag('img', array(
            'src' => Fish::feature_img($segments[1], $data->foldername),
            'style' => 'border-radius: 16px; border: 1px solid #ccc; width: 188px;',
            'alt' => Controller::trans("Feature screenshot game"). " " . $data->gamename
        )));
        ?>


        <div class="content">
            <?php
                echo $data->getShortDescription();
            ?>
        </div>

        <div class="nav">


            <b><?php echo Controller::trans("Rate It"); ?>:</b> <div data-productid="<?php echo $data->gameid; ?>" class="rateit" data-rateit-value="<?php echo $data->rating / $data->count_rating; ?>"></div>

        </div>

        <?php
        if($data->platform != Game::PLATFORM_OG) {
            echo CHtml::tag("a",
                array(
                    "href" => $data->getUrl(),
                    "_target" => "blank",
                    "class" => "btn btn-small",
                    "title" => Controller::trans("View more about game") . " : " . $data->getGameName(),
                    "id" => "playOnline",
                ), Controller::trans("View more"));
            }
        ?>

        <?php $this->renderPartial('_buttons', array(
            'data' => $data,
        )); ?>

    </div>
