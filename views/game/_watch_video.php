<!-- Button to trigger modal -->
<a href="#myModal" role="button" title="<?php echo Controller::trans("Watch the video games"); ?> <?php echo $data->getGamename(); ?>" class="btn" data-toggle="modal"><i class="icon-play"></i><?php echo Controller::trans("Watch Video"); ?></a>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">X</button>
        <h3 id="myModalLabel"><?php echo Controller::trans("Watch Video Clip"); ?></h3>
    </div>
    <div class="modal-body">
        <div class="pull-left">
            <object
                type="application/x-shockwave-flash"
                id="kt_player_internal"
                name="kt_player_internal"
                data="/css/player.swfx"
                width="316"
                height="254">
                <param name="allowfullscreen" value="true">
                <param name="allowscriptaccess" value="always">
                <param name="bgcolor" value="000000">
                <param name="flashvars" value="video_url=<?php echo urlencode(sprintf("http://cdn-games.bigfishsites.com/%s/%s.flv", $data->foldername, $data->foldername)); ?>%2F&amp;video_id=3805&amp;license_code=87ccba8ba9ba16a2577df6a86ed97705&amp;postfix=.mp4&amp;timeline_screens_url=%3Fmode%3Dasync%26action%3Dtimelines%26count%3D104%26video_id%3D3805%26format%3D.mp4%26rnd%3D1380219136&amp;timeline_screens_interval=10&amp;preview_url=<?php echo urlencode(Fish::images($data->foldername, "screen1")); ?>&amp;skin=1&amp;bt=3&amp;hide_controlbar=0&amp;embed=0&amp;internal_id=kt_player_internal">
            </object>

        </div>
        <div class="pull-right span-4">
            <p>
                <?php echo $data->getMedDescription(); ?>
            </p>
            <?php $this->renderPartial('_buttons', array(
                'data' => $data,
            )); ?>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo Controller::trans("Close"); ?></button>
        <!--<button class="btn btn-primary">Save changes</button>-->
    </div>
</div>