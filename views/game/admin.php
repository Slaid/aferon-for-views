<?php if($step == 1) : ?>

<ul class="gamelist">
<?php foreach($model as $data) : ?>

    <li><a href="?id=<?php echo $data->gameid; ?>"><?php echo $data->gamename; ?></a></li>

<?php endforeach; ?>

    <?php for($i=1; $i < $pages; $i++) : ?>

        <?php echo CHtml::tag("a", array(
            "href" => Yii::app()->createUrl('game/admin', array("page" => $i)),
        ), $i); ?>

    <?php endfor; ?>
</ul>


<?php endif; ?>



<?php if($step == 2) : ?>

    <style>
        input, textarea {
            width: 300px;
            margin: 0 20px;
        }
    </style>

    <div style="float:left;margin: 0 20px;">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'horizontalForm',
            'type'=>'horizontal',
        )); ?>

        <?php echo $form->textFieldRow($model, 'gamename'); ?>
        <?php echo $form->textAreaRow($model, 'shortdesc', array('class' => 'span8', 'rows' => 5)); ?>
        <?php echo $form->textAreaRow($model, 'meddesc', array('class' => 'span8', 'rows' => 7)); ?>
        <?php echo $form->textAreaRow($model, 'longdesc', array('class' => 'span8', 'rows' => 15)); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save')); ?>
        <?php $this->endWidget(); ?>
    </div>

    <div style="float:left;margin: 0 20px;">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'horizontalForm',
            'type'=>'horizontal',
        ));  ?>

        <div style="overflow: hidden">
            <div style="float: left">
                <?php echo $form->textFieldRow($syncModel, 'gamename'); ?>
                <?php echo $form->textAreaRow($syncModel, 'short', array('class' => 'span8', 'rows' => 5)); ?>
                <?php echo $form->textAreaRow($syncModel, 'med', array('class' => 'span8', 'rows' => 7)); ?>
                <?php echo $form->textAreaRow($syncModel, 'long', array('class' => 'span8', 'rows' => 15)); ?>
            </div>
            <div style="float: left">
                <?php echo $form->textAreaRow($syncModel, 'seo_title', array('class' => 'span8', 'rows' => 5)); ?>
                <?php echo $form->textAreaRow($syncModel, 'seo_key', array('class' => 'span8', 'rows' => 5)); ?>
                <?php echo ($model->rsname) ? $form->textFieldRow($model, 'rsname') : $form->textFieldRow($model, 'sname'); ?>
            </div>
        </div>

        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save')); ?>
        <?php $this->endWidget();  ?>
    </div>

<?php endif; ?>