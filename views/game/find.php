<?php
    $findGames = (isset($_POST['Game']['gamename'])) ? " " . CHtml::encode($_POST['Game']['gamename']) : "";
    $this->pageTitle = Controller::trans("Game search results") . " " . $findGames;
    $this->description = Controller::trans("Download") . " " . Game::getPlatforms(Controller::currentPlatform()) . " " . Controller::trans("view review games.") . " " . (isset($_GET['genre']) ? Controller::trans("Genre") . " - " . $this->genreList[$_GET['genre']]->name : "") . ". " . Controller::trans("Page") . " - " . intval(@$_GET['Game_page']);
?>
<br /><br />
<div class="span-18">

    <h1><?php echo Controller::trans("Download"); ?> <?php echo Game::getPlatforms(Controller::currentPlatform()); ?><?php echo (isset($_GET['genre']) ?  " " . Controller::trans("with genre") . " " . $this->genreList[$_GET['genre']]->name . "." : ".")?></h1>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'searchForm',
        'type'=>'search',
        'action' => Yii::app()->createUrl('game/find'),
        //'htmlOptions'=>array('class'=>'well'),
    )); ?>

    <?php echo $form->textFieldRow($find, 'gamename', array('class'=>'input-medium', 'style' => 'width: 300px;', 'prepend'=>'<i class="icon-search"></i>', 'value' => @$_POST['Game']['gamename'])); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Go')); ?>

    <?php $this->endWidget(); ?>


<br><br>
<div class="label-text"><?php echo Controller::trans("List"); ?> <?php echo Game::getPlatforms(Controller::currentPlatform()); ?></div>

<?php if(is_array($model) && count($model) > 0) : ?>
    <?php foreach($model as $id => $data) : ?>
        <div class="news-block">
            <div class="right pull-left">
                <div>
                    <a href="<?php echo $data->getUrl(); ?>">
                        <img src="<?php echo Fish::model()->images($data->foldername, 'feature'); ?>" alt="<?php echo Controller::trans("Feature screenshot game");?> <?php echo $data->gamename; ?>" />
                    </a>
                </div>
                <a href="<?php echo Yii::app()->createUrl('game/download', array('gameid' => $data->gameid)); ?>" title="<?php echo Controller::trans("Now download Pc Game"); ?> <?php echo $data->gamename; ?>">
                    <div class="btn btn-mini btn-warning font-download">download</div>
                </a>
            </div>
            <div class="description pull-left" style="width: 600px;">
                <div class="title">
                    <a href="<?php echo $data->getUrl(); ?>" title="<?php echo Controller::trans("View game"); ?> <?php echo $data->gamename; ?>">
                        <b class="label-text"><?php echo $data->gamename; ?></b>
                    </a>
                </div>
                <div class="desc">
                    <?php echo $data->getMedDescription(); ?>
                </div>
                <div class="ff">
                    <?php echo date("j, M Y", strtotime($data->releasedate)); ?> &nbsp; / &nbsp; <i class="icon-<?php echo $data->getGenre('sname'); ?>"></i>
                    <a href="<?php echo Yii::app()->createUrl('game/games', array('platform' => Controller::currentPlatform(), 'genre' => $data->getGenre('sname'))); ?>" title="<?php echo Controller::trans("View games with genre");?> <?php echo $data->getGenre('name'); ?>"><?php echo $data->getGenre('name'); ?></a> &nbsp;
                    / &nbsp; <b><?php echo Controller::trans("Size"); ?> : </b><?php echo $data->gamesize(); ?>
                </div>
            </div>

        </div>
		
		
    <?php endforeach; ?>
<?php endif; ?>


</div>


