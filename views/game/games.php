<?php

    $this->pageTitle = "AFeroN Games"   . ' - ' . ((Controller::currentPlatform() == "og") ? Controller::trans("Play") : Controller::trans("Download")) . ' ' . Game::getPlatforms(Controller::currentPlatform()) . (isset($_GET['tag']) ?  " " . Controller::trans("by tag") . " " . Controller::$genreList[$_GET['tag']]->name . "." : "") . (isset($_GET['genre']) ?  " " . Controller::trans("with genre") . " " . Controller::$genreList[$_GET['genre']]->name . "." : "");
    $this->description = Controller::trans("Download") . " " . Game::getPlatforms(Controller::currentPlatform()) . " " . (isset($_GET['genre']) ?  " " . Controller::trans("with genre") . " " . Controller::$genreList[$_GET['genre']]->name . "." : ".") . " - " . Yii::app()->name . " " . ((Controller::currentPlatform() == "og") ? Controller::trans("Play") : Controller::trans("Download")) . " " . Game::getPlatforms(Controller::currentPlatform()) . ", " . Controller::trans("view review games.") . " " . (isset($_GET['genre']) ? Controller::trans("Genre") . " - " . Controller::$genreList[$_GET['genre']]->name . ". " : "") . Controller::trans("Page") . " - " . intval(@$_GET['Game_page']);
?>
<br /><br />
<div class="span-18">

<h1><?php echo Controller::trans("Download"); ?>
    <?php echo Game::getPlatforms(Controller::currentPlatform()); ?>
    <?php echo (isset($_GET['genre']) ?  " " . Controller::trans("with genre") . " " . Controller::$genreList[$_GET['genre']]->name . "." : "")?>
    <?php echo (isset($_GET['tag']) ?  " " . Controller::trans("with tag") . " " . Controller::$genreList[$_GET['tag']]->name . "." : "")?>
</h1>


<br>
    <div class="genre-desc">
        <?php if(isset($_GET['genre'])) {
            echo Controller::$genreList[$_GET['genre']]->description;
        } ?>

        <?php if(isset($_GET['tag'])) {
            echo Controller::$genreList[$_GET['tag']]->description;
        } ?>

        <?php if(isset($_GET['platform']) && $_GET['platform'] == "pc") {
            echo Controller::trans("Download your favorite games for your computer from our collection.");
        } ?>

        <?php if(isset($_GET['platform']) && $_GET['platform'] == "og") {
            echo Controller::trans("Play free online on the website of our games.");
        } ?>

        <?php if(isset($_GET['platform']) && $_GET['platform'] == "mac") {
            echo Controller::trans("Choose the best game for your MAC.");
        } ?>
    </div>


<br>
<div class="label-text"><?php echo Controller::trans("List"); ?> <?php echo Game::getPlatforms(Controller::currentPlatform()); ?></div>


<?php

    $this->widget('zii.widgets.CListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
    'ajaxUpdate' => false,
	'template' => "<div style='overflow: hidden;'>{items}\n</div>{pager}",
));  ?>

</div>


