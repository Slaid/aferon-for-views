<?php

$this->description = $data->getMedDescription();

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(
        Game::getPlatforms($data->platform) => Yii::app()->createUrl('game/index', array('platform' => $data->platform)),
        $data->genre->name => Yii::app()->createUrl('game/index', array('platform' => $data->platform, 'genre' => $data->genreid)),
        Controller::trans('Information'),
    ),
));

?>
    <div class="singl-post">
        <div class="title" >
            <h1><?php echo CHtml::encode($data->getGameName()); ?></h1>
        </div>


        <div class="content">
            <div class="game-info">
                <?php

                $imgUrls = Fish::images($data->foldername);
                foreach ($imgUrls as $id => $img) {
                    $array[] = array(
                        "image" => $img,
                        "alt" => "Game screenshot" . $data->getGameName(),
                        "label" => "Game screenshot " . $data->getGameName(),
                       // "caption" => $data->shortdesc,
                    );
                }
                ?>

                <?php $this->widget('bootstrap.widgets.TbCarousel', array(
                    'htmlOptions' => array('class' => "galley"),
                    'items' => $array,
                ));

                ?>

                <div class="property">

                    <div style="padding: 5px 0">
                        <div class="google-adsense google-adsense-widget" >
                            <script type="text/javascript">
                                <!--
                                google_ad_client = "pub-9710740179130354";
                                google_alternate_color = "FFFFFF";
                                google_ad_width = 250;
                                google_ad_height = 250;
                                google_ad_format = "250x250";
                                google_ad_type = "text_image";
                                google_ad_channel ="AdSense Default";
                                google_color_border = "B0C9EB";
                                google_color_link = "164675";
                                google_color_bg = "FFFFFF";
                                google_color_text = "333333";
                                google_color_url = "2666F5";
                                google_ui_features = "rc:0";
                                //-->
                            </script>

                            <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                            </script>
                        </div>
                    </div>

                    <div class="share">
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                            <a class="addthis_button_preferred_1"></a>
                            <a class="addthis_button_preferred_2"></a>
                            <a class="addthis_button_preferred_3"></a>
                            <a class="addthis_button_preferred_4"></a>
                            <a class="addthis_button_compact"></a>
                            <a class="addthis_counter addthis_bubble_style"></a>
                        </div>
                        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5171c4f31f5f433a"></script>
                        <!-- AddThis Button END -->
                    </div>

                </div>

                <div class="">
                    <div class="span-7">
                        <?php
                        if(isset($data->bullet1)) {
                            echo CHtml::tag("b", array(), "Bullet");
                            echo "<ul>";

                            echo CHtml::tag("li", array() , $data->bullet1);

                            if(isset($data->bullet2)) {
                                echo CHtml::tag("li", array() , $data->bullet2);
                            }
                            if(isset($data->bullet3)) {
                                echo CHtml::tag("li", array() , $data->bullet3);
                            }
                            if(isset($data->bullet4)) {
                                echo CHtml::tag("li", array() , $data->bullet4);
                            }
                            if(isset($data->bullet5)) {
                                echo CHtml::tag("li", array() , $data->bullet5);
                            }
                            echo "</ul>";


                        }
                        ?>


                    </div>
                    <div class="span-7">
                        <?php
                        $request = json_decode($data->systemreq);

                        if($request) {

                            echo CHtml::tag("b", array(), "Min System request");
                            $array_key_replace_request = array(
                                "sysreqos" => "OS",
                                "sysreqmhz" => "CPU",
                                "sysreqmem" => "Memory",
                                "sysreqdx" => "DiretX",
                                "sysreqhd" => "Hard driver (MB)",
                            );

                            echo "<ul>";
                            foreach($request as $platform => $params) {
                                foreach($params as $key => $req) {
                                    echo "<li>" . str_replace(array_keys($array_key_replace_request), array_values($array_key_replace_request), $key) . " : " . $req . "</li>";
                                }
                            }
                            echo "</ul>";

                        }
                        ?>
                    </div>

                </div>
            </div>

            <div style="min-height: 100px;">
                 <?php echo $data->getLongDescription(); ?>
            </div>
            <br /><br />

            <div class="button-group">
                <?php $this->renderPartial('_buttons', array(
                    'data' => $data,
                )); ?>

                <?php
                    if(isset($data->oggameid)) {
                        echo CHtml::tag("a", array(
                            "href" => Yii::app()->createUrl('game/online', array('gameid' => $data->oggameid)),
                            "_target" => "blank",
                            "class" => "playOnline btn btn-danger btn-small",
                            "title" => "Play online game : " . $data->getGameName(),
                            ), "Play Online");

                    }

                    if(isset($data->macgameid)) {
                        echo CHtml::tag("a", array(
                            "href" => Yii::app()->createUrl('game/download', array('gameid' => $data->macgameid)),
                            "_target" => "blank",
                            "class" => "btn btn-info btn-small",
                            "title" => "Play online game : " . $data->getGameName(),
                            ),
                            "Download MAC");
                    }

                    if(isset($data->pcgameid)) {
                        echo CHtml::tag("a", array(
                            "href" => Yii::app()->createUrl('game/download', array('gameid' => $data->pcgameid)),
                            "_target" => "blank",
                            "class" => "btn btn-warning btn-small",
                            "title" => "Play online game : " . $data->getGameName(),
                            ) , "Download PC");
                    }

                    if(!YiiBase::app()->user->isGuest) {
                        echo CHtml::tag("a",
                            array(
                                "href" => Yii::app()->createUrl('game/share', array('gameid' => $data->gameid)),
                                "class" => "btn btn-inverse btn-small",
                                "title" => "Give a friend a game".  $data->gamename,
                            ), "Give a friend a game");
                    }

                ?>

                <br />
                <br />

                <?php $this->renderPartial('_watch_video', array(
                    'data' => $data,
                )); ?>

                <br />
                <br />
            </div>


            <br />
            <br />


            <div class="label-text">Review at a glimpse</div>
            <div class="review well span-15">

                <div class="span-7">
                    <div class="label-text">Genre : <?php echo $data->genre->name; ?></div>
                    <p><?php echo $data->genre->description; ?></p>

                    <div class="label-text">Date release</div>
                    <p><?php echo date('F j, Y', strtotime($data->releasedate)); ?></p>


                </div>
                <div class="span-6">
                    <div class="label-text">Game Rank</div>
                    <p><?php echo $data->gamerank; ?> points</p>

                    <div class="label-text">Please rate the game</div>

                    <b>Rate It:</b> <div data-productid="<?php echo $data->gameid; ?>" class="rateit" data-rateit-value="<?php echo $data->rating / $data->count_rating; ?>"></div>


                </div>
            </div>



            <br />
            <br />

        </div>


        <br /> <br />


    </div>
<div class="clear"></div>