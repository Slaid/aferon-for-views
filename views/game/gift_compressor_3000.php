<?php

$this->description = Controller::trans("Do not know what to give to a friend? girlfriend? friend? Give him one of our great games! Such as") . " ". $data->gamename;

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(
        Controller::trans("Gift Game") => Yii::app()->createUrl('game/giftGame'),
        Controller::trans("Compressor") => Yii::app()->createUrl('game/giftCompressor3000', array('gameid' => $data->getSName())),
        $data->gamename => $data->getUrl(),
        Controller::trans("Game pack"),
    ),
));

?>
    <div class="singl-post">


        <?php /*
        <img src="/css/images/gift_compressor.png" title="Online gift compressor" />
        <div class="title" >
            <a href="<?php echo Yii::app()->createUrl('game/view', array('gameid' => $data->gameid)) ?>" class="btn btn-inverse">Back</a><h1><?php echo CHtml::encode($data->getGameName()); ?>  game pack in a gift box</h1>
        </div>
        */ ?>

        <h1><?php echo Controller::trans("Create a Gift Card"); ?></h1>
        <?php echo Controller::trans("Generator greeting cards, choose a suitable background, write a greeting and send to your friend!"); ?>


        <div class="content">

            <div class="game-info">
                <div style="overflow: hidden;">

                    <div class="rr">
                        <a class="reset btn btn-min" href="#reset"><?php echo Controller::trans("Reset"); ?></a>
                        Text : <textarea style="margin: 0;" type="text" id="imgTextVal">Happy holiday</textarea>
                        <div class="color-group">
                            <a href="rgb(83, 197, 67)" style="background: rgb(83, 197, 67)"></a>
                            <a href="rgb(250, 182, 6)" style="background: rgb(250, 182, 6)"></a>

                            <a href="rgb(6, 152, 250)" style="background: rgb(6, 152, 250)"></a>
                            <a href="rgb(250, 6, 94)" style="background: rgb(250, 6, 94)"></a><br />

                            <a href="rgb(250, 250, 6)" style="background: rgb(250, 250, 6)"></a>
                            <a href="rgb(182, 6, 250)" style="background: rgb(182, 6, 250)"></a>

                            <a href="rgb(255, 255, 255)" style="background: rgb(255, 255, 255)"></a>
                            <a href="rgb(0, 0, 0)" style="background: rgb(0, 0, 0)"></a>
                        </div>



                        <a href="#gift-model" role="button" class="btn btn-warning submit" data-toggle="modal" link="<?php echo Yii::app()->createUrl('game/giftCompressor3000', array('gameid' => $data->gameid)); ?>" class="btn btn-warning submit" style="margin: 10px;">Create a Gift Card</a>

                        <!-- Modal -->
                        <div style="width: 800px; margin-left: -400px; top 5%;" id="gift-model" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">X</button>
                                <h3 id="myModalLabel"><?php echo Controller::trans("Your greeting card created"); ?></h3>
                            </div>
                            <div class="modal-body" style="background: #DAECFF;">
                                <div class="pull-left">

                                    <div id="html-send" style="border: 1px dashed #ccc;float: left;">



                                    </div>
                                    <div class="urlGift"></div>
                                </div>
                                <div class="pull-left span-3">




                                    <strong>Share</strong>
                                    <?php $this->renderPartial('share_buttons'); ?>

                                    <a target="_blank" class="label-text" href="<?php echo Fish::getParams('purchase_game', 1, 1); ?>&cid=gamegift"><?php echo Controller::trans("Gift Game"); ?></a>
                                </div>
                            </div>
                            <div class="modal-footer">


                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                <!--<button class="btn btn-primary">Save changes</button>-->
                            </div>
                        </div>


                    </div>
                    <div id="compress_image">

                        <div class="image1" id="drug" style="top:10px; left:10px;">
                            <img src="<?php echo Fish::images($data->foldername, 'feature'); ?>" />
                        </div>
                        <div class="image2">
                            <img src="">
                        </div>
                        <div id="imgText" style="top:170px; left:185px;">
                            Happy holiday
                        </div>
                    </div>

                    <div id="compress_content">

                        <ul>
                        <?php

                            $dir_bg = $this->rootDir() . "/images/gift";
                            $file_name_list = $this->getFiles($dir_bg);
                            foreach($file_name_list as $file) : ?>

                                <li>

                                    <img src="<?php echo "/images/gift/" . $file; ?>" />

                                </li>

                                <?php //echo Fish::model()->resize($dir_bg . "/" . $file,  $dir_bg . "/" . $file, 640, 480, true); /* compress game */ ?>

                            <?php endforeach; ?>
                        </ul>

                    </div>

                    <script>
                        $(function() {

                            function fixEvent(e) {
                                // получить объект событие для IE
                                e = e || window.event

                                // добавить pageX/pageY для IE
                                if ( e.pageX == null && e.clientX != null ) {
                                    var html = document.documentElement
                                    var body = document.body
                                    e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
                                    e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
                                }

                                // добавить which для IE
                                if (!e.which && e.button) {
                                    e.which = e.button & 1 ? 1 : ( e.button & 2 ? 3 : ( e.button & 4 ? 2 : 0 ) )
                                }

                                return e
                            }

                            $('.reset').click(function() {
                                $('#drug').css('left',0);
                                $('#drug').css('top',0);
                                $('#imgText').css('left',0);
                                $('#imgText').css('top',0);
                                return false;
                            });

                            var textColor = 'rgb(0, 0, 0)';

                            $('.color-group a').click(function() {
                                var color = $(this).attr('href');
                                textColor = color;
                                $('#imgText').css('color', color);
                                return false;
                            });


                            var dragMaster = (function() {

                                var dragObject
                                var mouseOffset

                                var areaX;
                                var areaY;

                                var picWidth;

                                // получить сдвиг target относительно курсора мыши
                                function getMouseOffset(target, e) {
                                    var docPos	= getPosition(target)
                                    return {x:e.pageX - docPos.x, y:e.pageY - docPos.y}
                                }

                                function mouseUp(){
                                    dragObject = null

                                    // очистить обработчики, т.к перенос закончен
                                    document.onmousemove = null
                                    document.onmouseup = null
                                    document.ondragstart = null
                                    document.body.onselectstart = null
                                }

                                function mouseMove(e){
                                    e = fixEvent(e);

                                    var topSet = e.pageY - mouseOffset.y - areaY;
                                    var leftSet = e.pageX - mouseOffset.x - areaX;

                                    if(topSet > 0 && leftSet > 0) {
                                        with(dragObject.style) {
                                            position = 'absolute'
                                            top = topSet + 'px'
                                            left = leftSet + 'px'
                                        }
                                    }
                                    return false
                                }

                                function mouseDown(e) {
                                    e = fixEvent(e)
                                    if (e.which!=1) return

                                    dragObject  = this

                                    // получить сдвиг элемента относительно курсора мыши
                                    mouseOffset = getMouseOffset(this, e)

                                    // эти обработчики отслеживают процесс и окончание переноса
                                    document.onmousemove = mouseMove
                                    document.onmouseup = mouseUp

                                    // отменить перенос и выделение текста при клике на тексте
                                    document.ondragstart = function() { return false }
                                    document.body.onselectstart = function() { return false }

                                    return false
                                }

                                return {
                                    makeDraggable: function(element){
                                        element.onmousedown = mouseDown;

                                        areaY = $('#compress_image').offset().top;
                                        areaX = $('#compress_image').offset().left;

                                    }
                                }

                            }())

                            function getPosition(e){
                                var left = 0
                                var top  = 0

                                while (e.offsetParent){
                                    left += e.offsetLeft
                                    top  += e.offsetTop
                                    e	 = e.offsetParent
                                }

                                left += e.offsetLeft
                                top  += e.offsetTop

                                return {x:left, y:top}
                            }

                            var drug = $('.image1 img');
                            dragMaster.makeDraggable(document.getElementById('drug'));
                            dragMaster.makeDraggable(document.getElementById('imgText'));


                            var setBG = function($url) {
                                $('.image2 img').attr('src', $url);
                            }

                            setBG('<?php echo "/images/gift/" . $file; ?>');

                            $('#compress_content ul li img').click(function() {
                                setBG($(this).attr('src'));
                            });

                            $('#imgTextVal').keyup(function() {
                                var html = $(this).val();
                                html = html.replace(/\n/g,"<br>");
                                $('#imgText').html(html);
                            });


                            $('.submit').live('click', function() {
                                $.ajax({
                                    url: $(this).attr('href'), //your server side script
                                    data: {
                                        img: $('.image2 img').attr('src'),
                                        img2top: $('#drug').css('top'),
                                        img2left: $('#drug').css('left'),
                                        text2top: $('#imgText').css('top'),
                                        text2left: $('#imgText').css('left'),
                                        text: $('#imgTextVal').val(),
                                        textColor: textColor
                                    }, //our data
                                    type: 'POST',
                                    success: function (data) {
                                        $('.urlGift').text(data);
                                        $('#html-send').html('<img style="max-height: 390px; max-width: 625px;" src="'+data+'">');
                                    }
                                });
                                return false;
                            });

                        });
                    </script>

                    <div class="clear"></div>

                </div>

        </div>


        <br /> <br />


    </div>
        </div>
<div class="clear"></div>