<?php

$this->pageTitle=Yii::app()->name . ' ' . Controller::trans('How to congratulate a person? Give him our game!');

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array('Community'=>array('user/compareGames'), Controller::trans('Gift game.')),
));

$this->description=Controller::trans("AFeroN - What to give? Doubt that you give someone? Do not take chances give him an interesting game!");

?>

<div class="span-18 margin-bottom">
    <h1 style="margin-left: 20px;"><?php echo Controller::trans("How to congratulate a familiar person? What to give?"); ?></h1>

    <div style="font-size: 14px; text-shadow: 1px 1px 1px #f1f1f1;">
        <div class="pull-left" style="width: 600px;">
            <?php echo Controller::trans("How to congratulate a familiar person?"); ?><br/><br />
            <?php echo Controller::trans("Congratulate a familiar person, give him one of the games in our collection. He is very overjoyed"); ?> ...<br/><br/>

            <?php echo Controller::trans("Select the game you like and click on donate, follow the instructions"); ?> ...<br/><br/>



            <b><?php echo Controller::trans("Try it! It's free!"); ?> <font color="green"><?php echo Controller::trans('Select the game and click on "packed with"'); ?></font></b><br/>
        </div>
        <div class="pull-right">
            <img src="/css/images/cat_gift.gif" />
        </div>


        <div class="clear"></div>
    </div>
    <br /><br />

    <h2 class="label-text"><?php echo Controller::trans("Collection of the best games"); ?> ...</h2>

    <?php foreach($model as $id => $data) : ?>
        <div class="cp_block">
            <a href="<?php echo $data->getUrl(); ?>" title="<?php Controller::trans("View game") ?> <?php echo $data->gamename; ?>">
                <div class="back">
                    <img src="<?php echo Fish::model()->images($data->foldername, 'feature'); ?>"/>
                </div>
                <div class="font">
                    <div class="head">
                        <?php echo $data->gamename; ?>
                    </div>
                    <div class="body">
                        <?php echo $data->getShortDescription(); ?>
                    </div>
                </div>
            </a>
            <div class="label-text"><a href="<?php echo Yii::app()->createUrl("game/giftCompressor3000", array("gameid" => $data->getSName())); ?>" class="btn btn-danger"><?php echo Controller::trans("Packed With"); ?></a></div>
        </div>

    <?php endforeach; ?>
</div>