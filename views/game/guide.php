<?php
    $findGames = (isset($_POST['Game']['gamename'])) ? " " . CHtml::encode($_POST['Game']['gamename']) : "";
    $this->pageTitle = Controller::trans("Strategy guide for games");
    $this->description = Controller::trans("Passing games, download the appropriate application for viewing the passing game.");
?>
<br /><br />
<div class="span-18">

    <h1><?php echo Controller::trans("Strategy guide for games"); ?> AFeroN edition</h1>


<br><br>
<div class="label-text"><?php echo Controller::trans("List"); ?> <?php echo Game::getPlatforms(Controller::currentPlatform()); ?></div>

<?php if(is_array($model) && count($model) > 0) : ?>
    <?php foreach($model as $data) : ?>
        <div class="news-block">
            <div class="right pull-left">
                <div>
                    <a href="<?php echo $data->getUrl(); ?>">
                        <img src="<?php echo Fish::model()->images($data->foldername, 'feature'); ?>" alt="<?php echo Controller::trans("Feature screenshot game");?> <?php echo $data->gamename; ?>" />
                    </a>
                </div>

            </div>
            <div class="pull-left" style="margin-left: 10px; width: 75px;">
                <a href="<?php echo Yii::app()->createUrl('game/download', array('gameid' => $data->gameid)); ?>" title="<?php echo Controller::trans("Now download Pc Game"); ?> <?php echo $data->gamename; ?>">
                    <div class="btn btn-mini btn-warning font-download">Download Strategy Guide</div>
                </a>
            </div>
            <div class="description pull-left" style="width: 520px;">
                <div class="title">
                    <a href="<?php echo $data->getUrl(); ?>" title="<?php echo Controller::trans("View game"); ?> <?php echo $data->gamename; ?>">
                        <b class="label-text"><?php echo $data->gamename; ?> AferoN edition</b>
                    </a>
                </div>
                <div class="desc" style="margin-bottom: 10px;">
                    <?php echo $data->getMedDescription(); ?>
                </div>
                <div class="ff">
                    <?php echo date("j, M Y", strtotime($data->releasedate)); ?> &nbsp; / &nbsp; <i class="icon-<?php echo $data->getGenre('sname'); ?>"></i>
                    <a href="<?php echo Yii::app()->createUrl('game/games', array('platform' => Controller::currentPlatform(), 'genre' => $data->getGenre('sname'))); ?>" title="<?php echo Controller::trans("View games with genre");?> <?php echo $data->getGenre('name'); ?>"><?php echo $data->getGenre('name'); ?></a> &nbsp;
                    / &nbsp; <b><?php echo Controller::trans("Size"); ?> : </b><?php echo $data->gamesize(); ?>
                </div>
            </div>

        </div>
    <?php endforeach; ?>
<?php endif; ?>


</div>


