<?php
    $this->pageTitle = Yii::app()->name . ' - ' . Game::getPlatforms(Controller::currentPlatform());

?>

<div class="right-sidebar">
    <div class="top-seven" value="<?php echo @date("N"); ?>">

        <div class="portlet-decoration">
            <div class="portlet-title"><?php echo Controller::trans("Top 7 Games"); ?> ( <?php echo @date("l"); ?> )</div>
        </div>
        <div class="bb">
            <?php foreach($models as $data) { ?>
                <?php $this->renderPartial('_home_view', array(
                    'data' => $data,
                )); ?>
            <?php } ?>
            <script>
                $('.bb').find('.special-item-right').eq(<?php echo @date("N"); ?>-1).addClass("currentWeek");
            </script>
        </div>
    </div>


    <div class="top-seven"">
        <div class="portlet-decoration">
            <div class="portlet-title"><?php echo Controller::trans("Find us on Facebook"); ?></div>
        </div>
        <div class="bb">
            <div class="fb-like-box" data-href="https://www.facebook.com/download.fun.games" data-width="250" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
        </div>
    </div>

    <?php  $this->widget('Interview', array(
        'title' => Controller::trans("Comments on Games"),
    )); ?>

    <?php /*<div id="compare">
    </div> */ ?>


</div>

<div class="main">

    <b style="text-shadow: 1px 1px 1px #FFFFFF; color: #ADADAD; margin: 0 0 5px 20px;"><?php echo Controller::trans("Example:"); ?> </b> <span style="color: #0E85CD">Lost in Reefs 2</span> <br />
    <div class="qgame">

        <?php  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'searchForm',
            'type'=>'search',
            'action' => Yii::app()->createUrl('game/find'),
            //'htmlOptions'=>array('class'=>'well'),
        )); ?>

        <?php echo $form->textFieldRow(Game::model(), 'gamename', array('class'=>'input-medium', 'style' => 'width: 380px;')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'htmlOptions' => array('class' => 'btn-info'), 'label'=>'Find')); ?>

        <?php $this->endWidget();  ?>

    </div>

<div class="label-text"><?php echo Controller::trans("Featured Games"); ?></div>
    <div class="feature">
        <ul>
        <?php if(is_array($features) && count($features) > 0) {
            foreach ($features as $id => $data) { ?>

            <?php

            echo CHtml::tag("li",array(), CHtml::tag("a", array(
                'href' => $data->getUrl(),
                'title' => Controller::trans('View images'),
                'video' => '
                    <object
                        type="application/x-shockwave-flash"
                        id="kt_player_internal"
                        name="kt_player_internal"
                        data="/css/player.swfx"
                        width="316"
                        height="254">
                        <param name="allowfullscreen" value="true">
                        <param name="allowscriptaccess" value="always">
                        <param name="bgcolor" value="000000">
                        <param name="flashvars" value="video_url=' . urlencode(sprintf("http://cdn-games.bigfishsites.com/%s/%s.flv", $data->foldername, $data->foldername)) . '%2F&amp;video_id=3805&amp;license_code=87ccba8ba9ba16a2577df6a86ed97705&amp;postfix=.mp4&amp;timeline_screens_url=%3Fmode%3Dasync%26action%3Dtimelines%26count%3D104%26video_id%3D3805%26format%3D.mp4%26rnd%3D1380219136&amp;timeline_screens_interval=10&amp;preview_url=' . urlencode(Fish::images($data->foldername, "screen1")) . '&amp;skin=1&amp;bt=3&amp;hide_controlbar=0&amp;embed=0&amp;internal_id=kt_player_internal">
                    </object>
                ',
            ), CHtml::tag('img', array(
                'src' => Fish::images($data->foldername, 'screen1'),
                'alt' => $data->getGameName(),
            )) ));

            ?>


        <?php } } ?>
        </ul>

        <div class="preload">
            <div class="ctn">
                <div class="embed_feature">

                    <object
                        type="application/x-shockwave-flash"
                        id="kt_player_internal"
                        name="kt_player_internal"
                        data="/css/player.swfx"
                        width="316"
                        height="254">
                        <param name="allowfullscreen" value="true">
                        <param name="allowscriptaccess" value="always">
                        <param name="bgcolor" value="000000">
                        <param name="flashvars" value="video_url=<?php echo urlencode(sprintf("http://cdn-games.bigfishsites.com/%s/%s.flv", $data->foldername, $data->foldername)); ?>%2F&amp;video_id=3805&amp;license_code=87ccba8ba9ba16a2577df6a86ed97705&amp;postfix=.mp4&amp;timeline_screens_url=%3Fmode%3Dasync%26action%3Dtimelines%26count%3D104%26video_id%3D3805%26format%3D.mp4%26rnd%3D1380219136&amp;timeline_screens_interval=10&amp;preview_url=<?php echo urlencode(Fish::images($data->foldername, "screen1")); ?>&amp;skin=1&amp;bt=3&amp;hide_controlbar=0&amp;embed=0&amp;internal_id=kt_player_internal">
                    </object>

                </div>
            </div>
            <div class="label-text"><div class="icon-play pull-left"></div><b><?php echo Controller::trans("Play Now"); ?> &nbsp;</b><a href="<?php echo $data->getUrl(); ?>"><?php echo $data->getGamename(); ?></a></div>
        </div>
    </div>

<br /><br />

    <div class="news">
        <h4 class="label-text"><?php echo Controller::trans("New Games"); ?></h4>
        <ul>
            <li>
                <?php if(is_array($news) && count($news) > 0) {
                foreach ($news as $id => $data) { ?>
                    <div class="news-block">
                        <div class="right pull-left">
                            <div>
                                <a href="<?php echo $data->getUrl(); ?>">
                                    <img src="<?php echo Fish::model()->images($data->foldername, 'feature');  ?>" alt="<?php echo Controller::trans("Feature screenshot game");?> <?php echo $data->gamename; ?>" />
                                </a>
                            </div>
                            <a href="<?php echo $data->getDownloadUrl(); ?>" rel="nofollow" title="<?php echo Controller::trans("Now download Pc Game"); ?> <?php echo $data->gamename; ?>">
                                <div class="btn btn-mini btn-warning font-download"><?php echo Controller::trans("download"); ?></div>
                            </a>
                        </div>
                        <div class="description pull-left">
                            <div class="title">
                                <a href="<?php echo $data->getUrl(); ?>" title="<?php echo Controller::trans("View game"); ?> <?php echo $data->gamename; ?>">
                                    <b class="label-text"><?php echo $data->gamename; ?></b>
                                </a>
                            </div>
                            <div class="desc">
                                <?php echo $data->getMedDescription(); ?>
                            </div>
                            <div class="ff">
                                <?php echo date("j, M Y", strtotime($data->releasedate)); ?> &nbsp; / &nbsp; <i class="icon-<?php echo $data->getGenre('sname'); ?>"></i>
                                <a href="<?php echo Yii::app()->createUrl('game/games', array('platform' => Controller::currentPlatform(), 'genre' => $data->getGenre('sname'))); ?>" title="<?php echo Controller::trans("View games with genre");?> <?php echo $data->getGenre('name'); ?>"><?php echo $data->getGenre('name'); ?></a> &nbsp;
                                / &nbsp; <b><?php echo Controller::trans("Size"); ?> : </b><?php echo $data->gamesize(); ?>
                            </div>
                        </div>

                    </div>
					
                <?php  } } ?>
            </li>
        </ul>

    </div>



    <div class="all label-text"><a title="<?php echo Controller::trans("View all new pc games"); ?>" href="<?php echo Yii::app()->createUrl('game/games', array('platform' => Game::PLATFORM_PC));?>"><?php echo Controller::trans("View all games"); ?>.</a></div>
</div>

