<?php if(count($model) > 0): ?>
<?php foreach($model as $data): ?>
    <div class="item">
        <div class="top">
            <a target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('game/view', array(
                'gameid' => $data->getSName(),
            )); ?>">
                <div class="img"><img src="http://<?php echo $this->domain; ?><?php echo Fish::model()->images($data->foldername, 'feature'); ?>" <?php echo $data->gamename; ?>" /></div>
                <div class="info">
                    <div class="title"><?php echo $data->gamename; ?></div>
                    <div class="desc"><?php echo $data->meddesc; ?></div>
                </div>

            </a>
        </div>
        <div class="meta-info">
            <div class="size"><?php echo $data->gamesize(); ?></div>
            <div class="genre"><a target="_blank" href="http://<?php echo $this->domain; ?><?php echo Yii::app()->createUrl('game/games', array('platform' => $platform, 'genre' => $data->getGenre('sname'))); ?>" title="<?php echo $data->getGenre('name'); ?>"><?php echo $data->getGenre('name'); ?></a></div>
            <div class="date"><?php echo date("j, M Y", strtotime($data->releasedate)); ?></div>

        </div>
        <div class="group_buttons">
            <b class="top-numbers"></b>
            <?php
            if($data->platform == Game::PLATFORM_PC)
            {
                echo CHtml::tag("a",
                    array(
                        "href" => "http://" . $this->domain . $data->getDownloadUrl(),
                        "target" => "_blank",
                        'rel' => 'nofollow',
                        "class" => "btn btn-warning btn-small",
                    ), "Download PC");
            }

            if($data->platform == Game::PLATFORM_MAC)
            {
                echo CHtml::tag("a",
                    array(
                        "href" => "http://" . $this->domain . $data->getDownloadUrl(),
                        "target" => "_blank",
                        'rel' => 'nofollow',
                        "class" => "btn btn-mac btn-small",
                    ), "Download MAC");
            }
            echo CHtml::tag("a",
                array(
                    "href" => Yii::app()->createAbsoluteUrl('game/view', array(
                            'gameid' => $data->getSName(),
                        )),
                    "target" => "_blank",
                    'rel' => 'nofollow',
                    "class" => "btn btn-info btn-small",
                ), "View info");
            ?>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>