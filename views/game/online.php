<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />

    <style>
        body {
            padding:10px;
            text-align:center;
        }

        #alacontent {
            padding: 0px;
            margin-top: -85px;
            height: 568px;
            overflow: hidden;
        }
    </style>
</head>
<body>
<div id="alacontent" style="overflow: hidden">

    <?php


        if($model->platform == Game::PLATFORM_OG) {
            echo CHtml::tag('iframe', array(
                'src' => Fish::model()->getParams('play_iframe_og', $model->gameid, $model->foldername),
                'height' => $model->onlineiframeheight,
                'width' => $model->onlineiframewidth,
                'scrolling' => 'no',
                'style' => 'border: 0',
            ));
        }
    ?>
</div>

<div class="">

</div>

</body>
</html>
<?php die(); ?>