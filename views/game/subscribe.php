<?php

$this->description = Controller::trans("Subscribe to the daily update of the games. At any time you can unsubscribe from our mailing list.");
$this->pageTitle = Controller::trans("Free subscription to new games every day!");


?>

<div class="subscribe">
<div class="well">
    <h1>Subscribe to daily updates of games</h1>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

<?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
)); ?>


<?php echo $form->textFieldRow($model, 'name'); ?>
<?php echo $form->textFieldRow($model, 'email', array('hint' => Controller::trans("Enter your email address"))); ?>

<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=> Controller::trans('Subscribe!'))); ?>

<?php $this->endWidget(); ?>
</div>
</div>
<div class="clear"></div>

<br /><br />
<div class="games">
    <h2 class="label-text">New games every day updates!</h2>


    <?php

    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
        'ajaxUpdate' => true,
        'template' => "<div style='overflow: hidden;'>{items}\n</div>{pager}",
    ));  ?>
</div>