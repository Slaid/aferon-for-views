<?php
    if($model->platform == "pc")
        $for = Controller::trans("Download game") . " " . $model->getGamename() . " " . Controller::trans("for PC");
    if($model->platform == "mac")
        $for = Controller::trans("Download game") . " " . $model->getGamename() . " " . Controller::trans("for MAC");
    if($model->platform == "og")
        $for = Controller::trans("Play free online game in the") . " " . $model->getGamename();
    $this->pageTitle= $model->getSeoTitle($for);
	$this->key = $model->getSeoKey();
?>

<?php $this->renderPartial('_full_view', array(
	'data'=>$model,
)); ?>

<div class="comments" id="#comments" style="margin: 20px 40px;">

    <?php $reviews = $model->reviews; ?>
    <?php if(is_array($reviews) && count($reviews)): ?>
        <style>
            .reviews .item {
                background: #f1f1f1;
                padding: 5px 10px;
                border-top: 1px dashed #fff;
                margin-bottom: 10px;
            }
            .reviews .head {
                overflow: hidden;
                margin-bottom: 10px;
            }
            .reviews .text {
                float: left;
                font-size: 18px;
                font-weight: bold;
            }
            .reviews .created_at {
                float: right;
            }
        </style>
        <div class="reviews">
    <?php foreach($reviews as $review): ?>
        <div class="item">
            <div class="head">
                <div class="text"><?php echo $review->title; ?></div>
                <div class="created_at"><?php echo $review->created_at; ?></div>
            </div>
            <p><?php echo $review->desc; ?></p>
        </div>
    <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <div id="comment_list">
        <?php  $this->renderPartial('../user/_game_comment_list', array(
            'data'=>$model,
        ));  ?>
    </div>

    <div class="label-text"><?php echo Controller::trans("Please leave this game"); ?></div>
    <div class="comments-area">
        <textarea class="comment_text" style="width:600px; max-width: 620px; max-height: 100px;" placeholder="Your comment"></textarea><br />
        <div title="<?php echo Controller::trans("Post a Comment"); ?>" class="btn send-comment"><?php echo Controller::trans("Send"); ?></div>
    </div>
    <script>
        $(function() {
            $(".send-comment").click(function() {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('user/addComment', array('gameid' => $model->gameid)); ?>',
                    data: { text: $('.comment_text').val() }, //our data
                    type: 'POST',
                    success: function (data) {
                        $('#comment_list').html(data);
                        $('.comment_text').val('');
                    }
                });
            });
            $(".delete").live('click', function() {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('user/deleteComment', array('gameid' => $model->gameid)); ?>',
                    data: { text: $('.comment_text').val(), id: $(this).attr('value') }, //our data
                    type: 'POST',
                    success: function (data) {
                        $('#comment_list').html(data);
                    }
                });
            });
        });
    </script>
</div>

