<?php
$this->pageTitle = Controller::trans("IOS Game Finder, Find your ios Games, Download games for IPhone, Ipad, Ipod");
$this->description = Controller::trans("IOS Game Finder by aferon, Quickly find your favorite game now is easy. Use the free search IOS games for your Iphone, Ipad, Ipod.");

?>

<div style="margin: 30px 30px;">

    <div class="well">
        <h1><?php echo Controller::trans("IOS Game Finder"); ?></h1>

        <?php  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'searchForm',
            'type'=>'search',
            'action' => Yii::app()->createUrl('ios/find'),
            //'htmlOptions'=>array('class'=>'well'),
        ));  ?>

        <div style="margin: 10px;">
        <b style="color: green"><?php echo Controller::trans("Example"); ?>: </b> Angry Birds
        </div>
        <?php echo $form->textFieldRow(Ios::model(), 'name', array('class'=>'input-medium', 'style' => 'width: 585px')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'htmlOptions' => array('class' => 'btn-info'), 'label'=>'Find')); ?>

        <?php $this->endWidget(); ?>

    </div>

    <div class="ios-items-block">
        <?php if(is_array($model) && count($model) > 0) : ?>
            <?php foreach($model as $data) : ?>
                <a href="<?php echo Yii::app()->createUrl('ios/view', array('sname' => $data->sname)); ?>" class="ios-item overflow">
                    <div class="pull-left left">
                        <div>
                            <img src="<?php echo $data->artworkUrl60; ?>" />
                        </div>
                        <div class="price <?php echo ($data->price == "0") ? "free" : $data->price . " " . $data->currency; ?>">
                            <?php echo ($data->price == "0") ? "Free" : $data->price . " " . $data->currency; ?>
                        </div>
                    </div>
                    <div class="pull-left ios-desc overflow">
                        <b><?php  echo $data->trackName; ?></b><br />
                        <span><?php  echo $data->description; ?></span>
                        <div>
                            <div class="btn btn-mini btn-info"><?php echo Controller::trans("View game"); ?></div>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

