<?php
$this->pageTitle = Controller::trans("New games for your iPhone, iPad, iPod every day!");
$this->description = Controller::trans("Top 10 best iphone games, Download top games for your iPhone, iPad, iPod.");

?>

<div style="margin: 30px 30px;">

    <h1><?php echo Controller::trans("New games for your iPhone, iPad, iPod every day!"); ?></h1>
    <div class="well">
        <p style="font-size: 16px" class="label-text"><?php echo Controller::trans("Find ios games"); ?></p>
        <?php  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'searchForm',
            'type'=>'search',
            'action' => Yii::app()->createUrl('ios/find'),
            //'htmlOptions'=>array('class'=>'well'),
        ));  ?>

        <div style="margin: 10px;">
        <b style="color: green"><?php echo Controller::trans("Example"); ?>: </b> Angry Birds
        </div>
        <?php echo $form->textFieldRow(Ios::model(), 'name', array('class'=>'input-medium', 'style' => 'width: 585px')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'htmlOptions' => array('class' => 'btn-info'), 'label'=>'Find')); ?>

        <?php $this->endWidget(); ?>

    </div>

    <div class="overflow">
        <h2 class="label-text pull-left"><?php echo Controller::trans("New games"); ?></h2>
        <div class="pull-right">
            <b><?php echo Controller::trans("Price"); ?></b>
            <select name="price" onchange="document.location.href='<?php echo Yii::app()->createUrl("ios/index"); ?>'+((this.options[this.selectedIndex].value != 'all') ? '?price='+this.options[this.selectedIndex].value : '');">
                <option value="all">
                    <?php echo Controller::trans("All"); ?>
                </option>
                <option value="free"<?php echo (isset($_GET['price']) && $_GET['price'] == "free") ? ' selected="selected"': ""; ?>>
                    <?php echo Controller::trans("Free"); ?>
                </option>
                <option value="paid"<?php echo (isset($_GET['price']) && $_GET['price'] == "paid") ? ' selected="selected"': ""; ?>>
                    <?php echo Controller::trans("Paid"); ?>
                </option>
            </select>
        </div>
    </div>

    <div class="ios-items-block">
        <?php if(is_array($model) && count($model) > 0) : ?>
            <?php foreach($model as $data) : ?>
                <a href="<?php echo Yii::app()->createUrl('ios/view', array('sname' => $data->sname)); ?>" class="ios-item overflow">
                    <div class="pull-left left">
                        <div>
                            <img src="<?php echo $data->artworkUrl60; ?>" />
                        </div>
                        <div class="price <?php echo ($data->price == "0") ? "free" : $data->price . " " . $data->currency; ?>">
                            <?php echo ($data->price == "0") ? "Free" : $data->price . " " . $data->currency; ?>
                        </div>
                    </div>
                    <div class="pull-left ios-desc overflow">
                        <b><?php  echo $data->trackName; ?></b><br />
                        <span><?php  echo $data->description; ?></span>
                        <div>
                            <div class="btn btn-mini btn-info"><?php echo Controller::trans("View game"); ?></div>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

