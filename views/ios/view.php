<?php
$this->pageTitle = CHtml::encode($model->trackName) . " for your iPhone, iPad, iPod";
$this->description = "Download games " . $model->trackName . " by " . $model->sellerName . ". Price " . (($model->price == "0") ? "Free" : $model->price . "$") . ". Bug fix " . str_replace("\n", "", $model->releaseNotes);

?>

<div style="margin: 30px 30px;">

    <h1><?php echo $model->trackName; ?></h1>
    <div class="view-ios" style="padding: 10px; background: #FFF; border: 1px solid #ccc;">
        <div class="overflow" style="margin-bottom: 40px;">
            <div class="pull-left left">
                <div>
                    <img src="<?php echo $model->artworkUrl60; ?>" />
                </div>
                <div class="price <?php echo ($model->price == "0") ? "free" : $model->price . " " . $model->currency; ?>">
                    <?php echo ($model->price == "0") ? "Free" : $model->price . " " . $model->currency; ?>
                </div>
                <div class="wanted">
                    <?php echo $model->contentAdvisoryRating; ?>
                </div>
            </div>
            <div class="pull-left overflow" style="margin-left: 10px; width: 600px;">
                <b><?php  echo $model->trackName; ?></b><br />
                <span><?php echo str_replace("\n","<br />",$model->getDescription()); ?></span>


                <?php
                    if(isset($model->youtube) && $model->youtube != '') { ?>
                        <h1><?php  echo $model->trackName; ?> <?php echo Controller::trans("video review"); ?></h1>
                <iframe style="margin: 20px" width="420" height="315" src="<?php echo $model->youtube; ?>" frameborder="0" allowfullscreen></iframe>

                <?php } ?>
            </div>
        </div>

        <div class="overflow supported">
            <div class="pull-left" style="width: 128px;">
                <h6><?php echo Controller::trans("Supported Devices"); ?></h6>
                <?php echo str_replace(",","<br/>", $model->supportedDevices); ?>
            </div>
            <div class="pull-left">

                <?php $this->renderPartial('share_buttons'); ?>

                <?php $images = explode(",", $model->screenshotUrls); array_pop($images); ?>
                <?php if(is_array($images) && count($images) > 0) : ?>
                <h6 style="margin-left: 40px;">Screenshoots block</h6>



                <div class="sliderGallery">
                    <ul>
                        <?php foreach($images as $img) : ?>
                            <li><img class="zoom" style="height: 180px;" src="<?php echo $img; ?>" /></li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="slider">
                        <div class="handle"></div>
                    </div>
                </div>
                <?php else: ?>
                    <div class="">
                        <h6>Screenshoot</h6>
                        <img src="<?php echo $model->artworkUrl100; ?>" style="height: 180px;" class="zoom" />
                    </div>
                <?php endif; ?>

                <script type="text/javascript" charset="utf-8">


                    $(function() {
                        var slideFoo = function(wh) {
                            var container = $('.sliderGallery');
                            var ul = $(container).find('ul');
                            var itemsWidth = wh - container.outerWidth();

                            $('.slider', container).slider({
                                min: 0,
                                max: itemsWidth,
                                handle: '.handle',
                                stop: function (event, ui) {
                                    ul.animate({'left' : ui.value * -1}, 500);
                                },
                                slide: function (event, ui) {
                                    ul.css('left', ui.value * -1);
                                }
                            });
                        };

                        var getWidthUl = function() {
                            if(intervalOff == false) {

                                var wh = 0;
                                var ul = $('.sliderGallery').find('ul li');

                                $.each(ul, function() {
                                    wh = wh + $(this).width();
                                });


                                console.log(wh);

                                if(wh > 0) {
                                    intervalOff = true;
                                    slideFoo(wh);
                                }
                            }
                        }



                        var intervalOff = false;
                        setInterval(getWidthUl, 1000);

                    });
                </script>

                <div style="padding: 10px; font-weight: bold;">
                    Release date : <?php echo $model->releaseDate; ?>
                </div>


            </div>
<?php /*
            <a style="display: block; margin-top: 20px;" class="btn btn-info pull-right" href="<?php echo Yii::app()->createUrl("club/view", array("name" => $model->trackName));?>"><i class="icon-smile"></i> <?php echo $model->trackName; ?> <?php echo Controller::trans("FUN CLUB"); ?></a>
*/ ?>
        </div>
        <div class="download_block" style="margin-bottom: 40px;margin-left: 200px;">
            <img src="/css/images/app-store.png" style="display: inline-block; vertical-align: middle;" /> <div class="btn btn-success btn-large" link="<?php  echo $model->trackViewUrl; ?>" onclick="document.location.href=$(this).attr('link');">Download Game</div>


        </div>
    </div>
</div>

