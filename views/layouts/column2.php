<?php $this->beginContent('/layouts/main'); ?>
<div class="container">

	<div class="wrap">
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>


    <div id="sidebar">
        <?php  if(!Yii::app()->user->isGuest) {
            $this->widget('UserMenu');
        } else {
            $this->widget('LoginMenu');
        } ?>

        <?php $this->widget('GenreCloud', array(
            'title' => Controller::trans("Genres"),
        )); ?>

        <?php $this->widget('Special', array(
            'title' => Controller::trans("Special"),
        )); ?>


        <div class="portlet" id="yw5">
            <div class="portlet-decoration">
                <div class="portlet-title"><?php echo Controller::trans('Free full games!'); ?></div>
            </div>
            <div class="portlet-content" style="text-align: center; padding: 10px 0;">


            <?php
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'horizontalForm',
                'type'=>'vertical',
                'action'=>array('game/subscribe'),
            )); ?>


            <?php echo $form->textFieldRow(Subscribe::model(), 'name'); ?>
            <?php echo $form->textFieldRow(Subscribe::model(), 'email'); ?>

            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=> Controller::trans('Subscribe!'))); ?>

                <a href="<?php echo Yii::app()->createUrl('game/games', array('platform' => Game::PLATFORM_OG)); ?>" title="<?php echo Controller::trans('Play now free games'); ?>"><div class="btn btn-success"><?php echo Controller::trans('Get Free games'); ?></div></a>


            <?php $this->endWidget(); ?>

            </div>

        </div>


        <?php //$this->widget('RecentComments', array(
        //	'maxComments'=>Yii::app()->params['recentCommentCount'],
        //)); ?>

	</div>
</div>
<?php $this->endContent(); ?>