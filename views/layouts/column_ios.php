<?php $this->beginContent('/layouts/main'); ?>
<div class="container">

	<div class="wrap">
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>


    <div id="sidebar">

        <?php  $this->widget('IosTopGames', array(
            'title' => "IOS " . Controller::trans("Top 20 Ios Games"),
            'limit' => 20,
        ));  ?>

	</div>
</div>
<?php $this->endContent(); ?>