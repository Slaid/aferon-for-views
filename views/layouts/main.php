<!DOCTYPE html>
<html xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<!-- SEO -->
	
	<meta name="description" content="<?php echo $this->description; ?>"/>
	<meta name="keywords" content="<?php echo (isset($this->key) && $this->key != '') ? $this->key : "Download this game!"; ?>" />

	
	<link rel="canonical" href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" />
	<?php /* <meta property="og:locale" content="en_US" /> */ ?>
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo CHtml::encode($this->pageTitle); ?>" />
	<meta property="og:description" content="<?php echo CHtml::encode($this->pageTitle); ?>" />
	<meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" />
	<meta property="og:site_name" content="AFeroN Casual games" />
	
	
	<!-- SEO -->


    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/icons.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/rateit.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" />

    <?php Yii::app()->bootstrap->register(); ?>
    <?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.rateit.js', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/function.js', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/ajax.file.upload.js', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.countdown.js', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-1.10.3.custom.min.js', CClientScript::POS_HEAD); ?>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/images/favicon.png" rel="shortcut icon" />


    <style rel="text/css">
        .container {
            width: 1010px;
        }
    </style>
</head>

<body>

<?php

if (Yii::app()->user->hasFlash('error')) {
    echo '<div class="error">'.Yii::app()->user->getFlash('error').'</div>';
}

//IOS redirect
if(isset($_SERVER['HTTP_USER_AGENT'])) {
    Controller::$userAgent = $_SERVER['HTTP_USER_AGENT'];
    if(Yii::app()->user->getState('ios_redirect') === null &&  ( strpos(Controller::$userAgent, "iPhone") !== false ||
            strpos(Controller::$userAgent, "iPad") !== false ||
            strpos(Controller::$userAgent, "iPod") !== false)) {

        Controller::iosRedirect();
    }
}

echo Controller::alertBox(); //Окно с выводом сообщения о смене языка

if(@Yii::app()->user->name == "") {

    if(isset($_POST['name']) && $_POST['name']) {
        $username = CHtml::encode($_POST['name']);
        $model = new Profile();
        $model = $model->findByAttributes(array(
            'user_id' => Yii::app()->user->id,
        ));
        $model->nick = $username;
        if($model->save()) {
            Yii::app()->user->name = $username;
        }
    } else {
        $urlLogout = Yii::app()->createUrl('site/logout');
        echo '
<div class="your_name overflow">
    <div class="pull-left">
        <img src="/css/images/success_64.png" width="64" />
    </div>
    <div class="pull-left margin-left">
        ' . Controller::trans("Thank you, you are logged in, please introduce yourself!") . '<br />

        <form action="" method="POST">
        <div class="row success" style="margin: 20px;">
		    <label class="required">
		        Username <span class="required">*</span>
		    </label>
		    <input name="name" type="text" placeholder="Your name">
		</div>
        <input type="submit" class="btn btn-success" value="Ok"> <a class="btn btn-info" href="{$urlLogout}">Logout</a>
        </form>

    </div>
</div>
';
    }

}

?>

<div class="container" id="page">

	<div id="header">
		<div id="logo">
            <p><a href="/" title="AFeron"><?php echo CHtml::encode(Yii::app()->name); ?> <span style="color: #FF9206; margin-left: 10px;"><?php echo Controller::trans('Add to Favorite'); ?> <b>Ctrl + D</b></span>  &nbsp; <?php echo Controller::trans("Games downloads"); ?> <strong id="currentOnline">
                        <?php
                            $file = $this->rootDir() . '/download_files.txt';
                            $statistic = unserialize(@file_get_contents($file));
                            echo @$statistic['download'];
                        ?>
                    </strong></a></p>


            <div class="pull-right platform">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label'=>Controller::trans("PC Games"),
                    'type'=>'warning', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    'url' => Yii::app()->createUrl('game/games', array('platform' => Game::PLATFORM_PC))
                )); ?>
                <?php
                if(Controller::$currentLang == "en") {
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'label'=>Controller::trans('Mac Games'),
                        'type'=>'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                        'size'=>'small', // null, 'large', 'small' or 'mini'
                        'url' => Yii::app()->createUrl('game/games', array('platform' => Game::PLATFORM_MAC)),
                    ));
                }
                ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label'=>Controller::trans('Online Games'),
                    'type'=>'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    'url' => Yii::app()->createUrl('game/games', array('platform' => Game::PLATFORM_OG))
                )); ?>
            </div>
        </div>
        <div id="logo_img">

            <!--<a href="/"><img class="omsk pull-right" title="Go to Home" src="/css/images/omsk_game.jpg" /></a>-->
            <!--<img class="pp pull-left" src="/css/images/pespaleva.png" />-->
            <div class="header-right">


                <div class="share-header" style="width:450px;">
                    <div class="fb-like" data-href="http://<?php echo $this->domain; ?>" data-width="450" data-show-faces="false" data-send="true"></div>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/en_EN/all.js#xfbml=1&appId=532596850146079";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                </div>


                <div class="top-menu">
                    <?php echo CHtml::tag('a', array(
                        'class' => 'icon-gift',
                        'title' => Controller::trans('Give your loved ones a game'),
                        'href' => Yii::app()->createUrl('game/giftGame'),
                    ),'&nbsp;'); ?>

                    <?php $this->widget('bootstrap.widgets.TbMenu', array(
                        'type' => 'pills', // '', 'tabs', 'pills' (or 'list')
                        'stacked' => false, // whether this is a stacked menu
                        'htmlOptions' => array(
                            'class' => 'navigation pull-left',
                        ),
                        'items' => array(
                            array('label' => Controller::trans('Main Page'), 'icon' => 'go-home', 'url' => array('game/index')),

                            //array('label' => 'Blog', 'url' => array('post/index')),
                            array('label' => Controller::trans('Chat'), 'icon' => 'chat', 'url' => array('site/chat'), "linkOptions" => array("title" => "Online game chat.")),
                            array('label' => Controller::trans('Strategy Guide'), 'icon' => 'strategy-guide', 'url' => array('game/strategy'), "linkOptions" => array("title" => "View strategy guide for the game")),
                            //array('label' => Controller::trans('BFG Unlimited'), 'icon' => 'unlimited', 'url' => array('game/unlimited')),
                            array('label' => Controller::trans('IOS'), 'icon' => 'ios', 'url' => array('game/ios'), "linkOptions" => array("title" => "Ios games view page")),
                            //array('label' => Controller::trans('Android'), 'icon' => 'android', 'url' => array('game/android')),
                            //array('label' => Controller::trans('Fun Club'), 'icon' => 'smile', 'url' => array('club/index'), "linkOptions" => array("title" => "Fun art game club")),
                            array('label' => Controller::trans('Subscribe'), 'icon' => 'rss', 'url' => array('game/subscribe')),

                            //array('label' => "Server Lineage (MMORPG)", 'icon' => 'server', 'url' => (($this->domain != "localhost") ? "http://server.aferon.com" : "http://localhost/server/"), "linkOptions" => array("title" => "Server Lineage  Gracia Final")),
                        ),
                    )); ?>

                </div>
            </div>
            <a href="/" class="pull-left" title="<?php echo Controller::trans("Main Page"); ?> AFeroN.com"><img src="/css/images/aferon_logo.png"  alt="<?php echo Controller::trans("logotype aferon"); ?>" /></a>
            <strong style="float: right; position: relative; margin: -27px 138px 0 0;"><?php echo Controller::trans("New Games Every day!"); ?></strong>
        </div>
        <div class="banner">
            <a target="_blank" href="<?php echo Fish::affUrl('http://www.bigfishgames.com/download-games/21230/amaranthine-voyage-the-tree-of-life-ce/index.html'); ?>"><img src="/images/banner/banner.jpg" /></a>
        </div>
	</div><!-- header -->


	<?php echo $content; ?>


	<div id="footer">

        <div class="footer_items">
            <div class="span-5 languages">

                <?php

                $langs = Yii::app()->params['langs'];

                $items = array();
                $items[] = array('label' => Controller::trans("Select language"));
                foreach($langs as $domain => $name) {

                    $active = ($domain == Controller::$currentLang ? true : false);
                    $domain = ($domain == "en") ?  "" : $domain . ".";

                    $items[] = array('label' => $name, 'url' => 'http://' . $domain . 'aferon.com', 'active' => $active);
                }
                $items[] = array('label' => "Russian", 'url' => 'http://aferon.ru');

                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type' => 'list',
                    'items' => $items,
                )); ?>

            </div>

            <div class="span-5 languages">

                <?php $this->widget('bootstrap.widgets.TbMenu', array(
                    'type' => 'list',
                    'items' => array(
                        array('label' => Controller::trans("Main Menu")),
                        array('label' => Controller::trans('About'), 'url' => array('site/page', 'view' => 'about')),
                        array('label' => Controller::trans('Privacy Policy'), 'url' => array('site/page', 'view' => 'privacy_policy')),
                        array('label' => Controller::trans('Terms Of USE'), 'url' => array('site/page', 'view' => 'terms_of_use')),
                        array('label' => Controller::trans('Contact'), 'url' => array('site/contact')),
                        array('label' => Controller::trans('Members'), 'url' => array('user/index')),
                        array('label' => Controller::trans('Login'), 'url' => array('site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => Controller::trans('Logout') . ' ('.Yii::app()->user->name.')', 'url' => array('site/logout'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => Controller::trans('Registration'), 'url' => array('site/register'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => Controller::trans('Sponsorship'), 'url' => array('site/page', 'view' => 'sponsorship')),
                        array('label' => Controller::trans('For Webmasters'), 'url' => array('site/page', 'view' => 'webmasters')),
                    ),
                )); ?>

            </div>

            <div class="span-6 languages">

                <?php $this->widget('WithVideo', array(
                    'title' => "Random Games",
                ))->renderContent(); ?>

            </div>
            <div class="span-6 languages pull-right" style="width: 220px">

                <?php $this->widget('Review', array(
                    'title' => "New review",
                ))->renderContent(); ?>

				<div class="well">

					

<div class="g-plusone" data-size="small" data-annotation="inline" data-width="300"></div>

<script type="text/javascript">
  window.___gcfg = {lang: 'ru'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
					
					<div class="facebook-box" style="margin-left:-10px">
						<div id="fb-root"></div>
						<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
						<html xmlns:fb="http://ogp.me/ns/fb#">
						<div class="fb-like" data-href="https://www.facebook.com/download.fun.games" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>

					</div>
					
					<a href="https://twitter.com/DownloadGamesTT" class="twitter-follow-button" data-show-count="false">Follow @DownloadGamesTT</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<script src="//platform.linkedin.com/in.js" type="text/javascript">
                    lang: en_US
                </script>
                <script type="IN/Share"></script>


				</div>


            </div>
            <div class="clear"></div>
        </div>

        <div class="copy">Copyright &copy; 2012 - <?php echo date('Y'); ?> by AFeroN.com. <?php echo Controller::trans("All Rights Reserved"); ?>. <a href="/sitemap.xml">Sitemap.xml</a>&nbsp;
		
		
            <div class="pull-left">
				<!--<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/liveinternet.png">-->
                <!--LiveInternet counter--><script type="text/javascript"><!--
                    document.write("<a href='http://www.liveinternet.ru/click' "+
                        "target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
                        escape(document.referrer)+((typeof(screen)=="undefined")?"":
                        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                        ";"+Math.random()+
                        "' alt='' title='LiveInternet: показано число посетителей за"+
                        " сегодня' "+
                        "border='0' width='0' height='0'><\/a>")
                    //--></script><!--/LiveInternet-->

            </div>

            <div class="counter pull-right">


			</div>


		</div>

	</div><!-- footer -->

</div><!-- page -->
</body>
</html>