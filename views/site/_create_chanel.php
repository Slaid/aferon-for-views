<!-- Button to trigger modal -->
<a href="#create-chanel" role="button" class="btn btn-warning" data-toggle="modal"><?php echo Controller::trans('Create'); ?></a>

<!-- Modal -->
<div style="width: 800px; margin-left: -400px;" id="create-chanel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">X</button>
        <h3 id="myModalLabel"><?php echo Controller::trans('Create Chanel'); ?></h3>
    </div>
    <div class="modal-body" style="background: #DAECFF;">
        <div class="span-8">
            <label><?php echo Controller::trans('Chanel name'); ?></label>
            <input type="text" value="" id="chanel_name" />
            <div class="btn btn-success" id="send_chanel_name"><?php echo Controller::trans('Create Chanel'); ?></div>
            <?php if(Yii::app()->user->isGuest) { echo CHtml::tag("a", array("href" => Yii::app()->createUrl('site/login')), Controller::trans("Need Login")); } ?>
            <div id="result"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="refresh-chanel" aria-hidden="true"><?php echo Controller::trans('Close'); ?></button>
        <!--<button class="btn btn-primary">Save changes</button>-->
    </div>
</div>
