
<?php
$this->pageTitle=Yii::app()->name . ' - ' . Controller::trans('Real life Chat');

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(Controller::trans('Chat') => array('site/sitemap'), $this->trans('Real life Chat')),
));

?>

<script>

    $(function() {

        var chanel=undefined;

        var chanels = [];

        $('.thd #buttonStateful').click(function() {

            if(chanel == undefined) {
                $('#message-result-send-message').text('<?php echo Controller::trans("Please select chanel"); ?>');
                return false;
            }

            var txt = $('.thd .comment');
            if($(txt).val() != '') {
                $.ajax({
                    url: '/user/send?chanel='+chanel, //your server side script
                    data: { msg: $(txt).val() }, //our data
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        $(txt).val('');
                        refreshChat();
                        $('#message-result-send-message').html(data.message);
                    }
                });
            } else {
                $('#message-result-send-message').text('<?php echo Controller::trans("Please write message text!"); ?>');
            }
        });

        var refreshChanel = function() {
            $.ajax({
                url: '/chat_data/chanel_list.txt?r='+Math.random() * (9999999 - 999999), //your server side script
                dataType: 'JSON',
                success: function (data) {

                    var mass = '';
                    for(name in data) {
                        chanels.push(name);
                        var active = (name == chanel) ? ' active' : '';
                        mass += '<li class="chanel_id">';
                        mass += '<div class="chanel-info'+active+'"><div class="chanel-name name">' + name + '</div>';
                        mass += '</div>';
                        mass += '<div class="chanel-name user_list">';
                        for(login in data[name].user_list) {
                            mass += '<div class="user_item" value="' + data[name].user_list[login].userid + '">' + login + '</div>';
                        }
                        mass += '</div>';
                        mass += '</li>';
                    }
                    $('#chanel-item').html(mass);

                    //alert(chanels.shift());
                    //changeChanel(chanel, chanels.shift());
                }
            });
        };



        var refreshChat = function() {

            if(chanel == undefined) {
                $('#message-result-send-message').text('<?php echo Controller::trans("Please select chanel"); ?>');
                return false;
            }

            $.ajax({
                url: '/chat_data/'+chanel+'.txt?t'+Math.random() * (9999999 - 999999), //your server side script
                dataType: 'JSON',
                success: function (data) {
                    var mass = '';
                    for(id in data) {
                        mass += '<li>'+data[id].username+' : '+data[id].text+'</li>';
                    }

                    $('#chat-item').html(mass);
                }
            });
        }

        var sendIsActive = function() {
            if(chanel == undefined) {
                $('#message-result-send-message').text('<?php echo Controller::trans("Please select chanel"); ?>');
                return false;
            }
            $.ajax({
                url: '/user/updateActiveUser?chanel='+chanel+'&t'+Math.random() * (9999999 - 999999), //your server side script
                dataType: 'JSON',
                success: function (data) {
                    //qwe;
                }
            });
        }

        refreshChanel();

        $('#refresh-chanel').click(function() {
            refreshChanel();
            return false;
        });

        $('.chanel-info').live('click', function() {
            setChanel($(this).find('.name').text());
        });


        setInterval(function() {
            refreshChat();
        }, 1000);

        setInterval(function() {
            sendIsActive();
            refreshChanel();
        }, 10000);

        $('#send_chanel_name').click(function() {
            var _chanel_name = $('#chanel_name').val();
            $.ajax({
                url: '/user/createChanel', //your server side script
                data: { chanel_name: _chanel_name }, //our data
                type: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    $('#result').html(data.message);
                    refreshChanel();
                }
            });
        });


        var changeChanel = function(ch_old, ch_new) {
            if(ch_old == ch_new) {
                return false;
            }

            $.ajax({
                url: '/user/changeChanel',
                data: { chanel_name_new: ch_new, chanel_name_old: ch_old }, //our data
                type: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    refreshChanel();

                }
            });
        }

        var setChanel = function(name) {
            changeChanel(chanel, name);
            chanel = name;
            $('#message-result-send-message').text("");
            $('#current_chanel').text(chanel);
            refreshChat();
        }




    });



</script>

<h1 class="label-text"><?php echo Controller::trans('User Communication'); ?></h1>


    <div id="chat">
        <div class="pull-left body span-12">
            <ul id="chat-item">

            </ul>
        </div>
        <div class="pull-left chanel span-4">
            <h5><?php echo Controller::trans("Chanel's"); ?> <a href="#" id="refresh-chanel" class="icon-refresh"></a></h5>
            <div class="chanel-list">
                <ul id="chanel-item">
                </ul>
            </div>
            <b><?php echo Controller::trans('New Chanel'); ?></b>
            <?php $this->renderPartial('_create_chanel');  ?>
        </div>

    </div>

    <div class="thd span-17" style="padding: 5px 15px;border: 1px solid #D3D3D3; background: #FDFDF0;">
        <div style="padding: 5px 10px; display: inline-block;"><b id="current_chanel"></b> : <?php echo Yii::app()->user->name; ?></div>
        <input type="text" class="comment" name="comment" style="width:130px; margin: 0 10px 0 0;">

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'button',
            'type' => 'primary',
            'label' => Controller::trans('Send'),
            'loadingText' => 'loading...',
            'htmlOptions' => array(
                'id' => 'buttonStateful'
            ),
        )); ?>
        <b id="message-result-send-message"></b>
    </div>

