<?php
$this->pageTitle=Yii::app()->name . ' - ' . Controller::trans('Contact Us');

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(Controller::trans('Contact')=>'#', Controller::trans('Contact With AFeroN')),
));

?>

<div class="span-18">

<h1><?php echo Controller::trans('Contact Us'); ?></h1>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
    <?php echo Controller::trans('If you have any problems, You can contact us on the form. Here are accepted exclusively connected with the problems of customers. If you have problems with installing the game, or does not correct its behavior, please contact'); ?> <a href="http://bigfishgames.custhelp.com/app/home"><?php echo Controller::trans('there'); ?></a>
</p>
<p>
    <?php echo Controller::trans('If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.'); ?>
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>

	<p class="note"><?php echo Controller::trans('Fields with'); ?> <span class="required">*</span> <?php echo Controller::trans('are required'); ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'body'); ?>
		<?php echo $form->textArea($model, 'body', array('rows'=>6, "style" => "width:600px; max-width: 600px;")); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model, 'verifyCode'); ?>
		<div style="background: #FFF; padding: 10px; width: 300px; border-radius: 12px; margin-bottom: 10px;">
		<?php $this->widget('CCaptcha'); ?><br />
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint"><?php echo Controller::trans('Please enter the letters as they are shown in the image above.'); ?>
		<br/><?php echo Controller::trans('Letters are not case-sensitive.'); ?></div>
	</div>
	<?php endif; ?>

	<div class="row submit">
		<?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-info')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>

</div>