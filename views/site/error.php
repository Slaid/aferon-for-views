<?php
$this->pageTitle=Yii::app()->name . ' - ' . Controller::trans('Page not found');
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array('Error'),
));
?>
<div class="span-16">


<h2><?php echo Controller::trans('Error'); ?> <?php echo $error['code']; ?></h2>

<div class="error">
<?php echo CHtml::encode($error['message']); ?>
</div>


    <?php if(is_array($model) && count($model) > 0) : ?>
        <?php foreach($model as $data) : ?>
            <div class="news-block">
                <div class="right pull-left">
                    <div>
                        <a href="<?php echo $data->getUrl(); ?>">
                            <img src="<?php echo Fish::model()->images($data->foldername, 'feature'); ?>" alt="<?php echo Controller::trans("Feature screenshot game");?> <?php echo $data->gamename; ?>" />
                        </a>
                    </div>
                    <a href="<?php echo Yii::app()->createUrl('game/download', array('gameid' => $data->gameid)); ?>" title="<?php echo Controller::trans("Now download Pc Game"); ?> <?php echo $data->gamename; ?>">
                        <div class="btn btn-mini btn-warning font-download">download</div>
                    </a>
                </div>
                <div class="description pull-left">
                    <div class="title">
                        <a href="<?php echo $data->getUrl(); ?>" title="<?php echo Controller::trans("View game"); ?> <?php echo $data->gamename; ?>">
                            <b class="label-text"><?php echo $data->gamename; ?></b>
                        </a>
                    </div>
                    <div class="desc">
                        <?php echo $data->getMedDescription(); ?>
                    </div>
                    <div class="ff">
                        <?php echo date("j, M Y", strtotime($data->releasedate)); ?> &nbsp; / &nbsp; <i class="icon-<?php echo $data->getGenre('sname'); ?>"></i>
                        <a href="<?php echo Yii::app()->createUrl('game/games', array('platform' => Controller::currentPlatform(), 'genre' => $data->getGenre('sname'))); ?>" title="<?php echo Controller::trans("View games with genre");?> <?php echo $data->getGenre('name'); ?>"><?php echo $data->getGenre('name'); ?></a> &nbsp;
                        / &nbsp; <b><?php echo Controller::trans("Size"); ?> : </b><?php echo $data->gamesize(); ?>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>
    <?php endif; ?>

</div>