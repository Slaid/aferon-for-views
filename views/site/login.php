
<?php

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array('Login'=>array("site/login"), Controller::trans('Login in Community')),
));
?>

<div class="span-16">
<h1><?php echo Controller::trans('Login'); ?></h1>

<p><?php echo Controller::trans('Please fill out the following form with your login credentials:'); ?></p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note"><?php echo Controller::trans('Fields with'); ?> <span class="required">*</span> <?php echo Controller::trans('are required.'); ?></p>


	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton(Controller::trans('Login'), array('class' => 'btn btn-info')); ?>
        <?php echo CHtml::tag("a", array(
            "href" => Yii::app()->createUrl("site/register"),
        ), Controller::trans("Registration")); ?>
	</div>


    <h2 class="label-text" style="font-size: 14px;">Do you already have an account on one of these sites? Click the logo to log in with it here:</h2>
    <?php Yii::app()->eauth->renderWidget(); ?>



<?php $this->endWidget(); ?>
</div><!-- form -->
</div>