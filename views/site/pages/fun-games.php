
<?php
$this->pageTitle=Yii::app()->name . ' - Fun Games';

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array('About'=>array('site/about'), 'Information about AFeroN'),
));

?>
<div class="span-16">
    <img class="pull-right" src="/css/images/black_cat.png" />
    <h1>About</h1>

    <p>Founded in 2002, AFeroN is the world’s largest producer of casual games; dedicated to bringing engaging entertainment to everyone, anywhere, on any device.</p>
    <p>Through its proprietary, data-driven platform, millions of consumers seeking engaging entertainment easily discover and play PC and mobile games created by AFeroN’s network of more than 600 development partners and its in-house Big Fish Studios.</p>

    <p>The company has distributed more than 2 billion games from a growing catalog of 3,000+ unique PC games and 300+ unique mobile games, and offers cross-platform streaming games via its proprietary universal cloud gaming service, Big Fish Instant Games.</p>
    <p>Big Fish’s games are played in more than 150 countries on a wide variety of devices across 13 languages. The company is headquartered in Seattle, WA, with regional offices in Oakland, CA; Cork, Ireland; Vancouver, Canada; and Luxembourg.</p>
</div>