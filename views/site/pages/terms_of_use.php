<?php
$this->pageTitle=Yii::app()->name . ' - Terms OF Use';

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array('Contact'=>'#', 'AFeroN'),
));

?>

<div class="span-17">

<div class="col_14x pre_spacer_1x col_first">

<h1 class="ed_title">AFeroN Terms of Use
</h1>
<p>Last modified: December 13, 2012
</p>

<h2>AGREEMENT AND SERVICES
</h2>
<p>AFeroN Games, Inc. and/or its Affiliates provide access to the AFeroN Offerings subject to the conditions set forth in these Terms of Use. For purposes of these Terms of Use, the term "<strong>Affiliates</strong>" means, with respect to any party, any person or entity which controls, is controlled by, or is under common control with, such party, and the term “<strong>AFeroN Offerings</strong>” means the web sites of AFeroN, including <a href="http://www.bigfishgames.com" title="AFeroN Games">www.bigfishgames.com</a>,  <a href="http://www.selfawaregames.com" title="Self Aware Games" data-lang="en">www.selfawaregames.com</a>, any other sites on which these Terms of Use are posted, and any other AFeroN application, service or product licensed, downloaded or otherwise accessed by you through third party sites or sources, including the products and services available through any of the foregoing.
</p>
<p>IF YOU ARE NOT CONSIDERED TO BE A RESIDENT OF THE EUROPEAN UNION ON THE BASIS OF THE INFORMATION THAT YOU PROVIDE TO US, THESE TERMS ARE ENTERED INTO BETWEEN YOU AND AFeroN GAMES, INC.
</p>
<p>IF YOU ARE CONSIDERED TO BE A RESIDENT OF THE EUROPEAN UNION ON THE BASIS OF THE INFORMATION THAT YOU PROVIDE TO US, THESE TERMS ARE ENTERED INTO BETWEEN YOU AND AFeroN GAMES LUXEMBOURG S.A.R.L, (REGISTRATION NO. B 163052), WHICH HAS ITS REGISTERED OFFICE AT 46a, AVENUE J.F. KENNEDY, L-1855, LUXEMBOURG CITY, GRAND-DUCHY OF LUXEMBOURG.
</p>
<p>THE TERM "<strong>AFeroN</strong>" MEANS EITHER AFeroN GAMES, INC. OR AFeroN GAMES LUXEMBOURG S.A.R.L., DEPENDING ON YOUR COUNTRY OF RESIDENCE AS DESCRIBED ABOVE, AND IN EACH CASE ALONG WITH ITS AFFILIATES. Your use of the AFeroN Offerings constitutes your express acceptance without reservation of these Terms of Use.
</p>
<p>Use of the AFeroN Offerings is also governed by our <a href="http://www.aferon.com/company/privacy.html" title="Learn how we protect your privacy." target="_top">Privacy Policy</a> and any other terms of use applicable to services you register to use within a AFeroN Offering, including any amendments or updates thereto.
</p>
<p>Use of the AFeroN Software, as hereafter defined, is governed by the <a href="http://www.bigfishgames.com/company/gm_eula.html" target="_top">AFeroN Games, Inc. End User license</a>.
</p>
<p>Without limiting the foregoing, each of your AFeroN Offering account(s) (each a "AFeroN account"), if applicable, and participation in any AFeroN Offerings are governed by these Terms of Use. The AFeroN Offerings are always evolving, so it is important that you periodically check these Terms of Use, as well as the specific rules for any games or activities in which you choose to participate, for updates. If we revise these Terms of Use, such revision(s) will take effect immediately upon being posted within any AFeroN Offering. As with our <a href="http://www.bigfishgames.com/company/privacy.html" title="Learn how we protect your privacy." target="_top">Privacy Policy</a>, if you do not agree to any of these Terms of Use, you should discontinue using or participating in any and all AFeroN Offerings. If there is a conflict between these Terms of Use and any other rules or instructions posted within a AFeroN Offering, these Terms of Use will control.
</p>

<h2>ACCOUNT REGISTRATION
</h2>
<p>If you create a AFeroN account within any AFeroN Offering, you must provide truthful and accurate information to us in creating such account. If AFeroN has reasonable grounds to suspect that you have provided any information that is inaccurate, not current or incomplete, AFeroN may suspend or terminate your ability to use or access a AFeroN Offering, and refuse any and all current or future use of or access to any or all AFeroN Offerings (or any portion thereof). AFeroN requires all users to be over the age of thirteen (13).
</p>
<p>AFeroN reserves the right to limit the number of accounts a user can establish. This limit may change over time in our sole discretion.
</p>
<p>You are solely responsible for all activity on any and all of your AFeroN account(s) and for the security of your computer system. You should not reveal your username or password to any other person. AFeroN will not ask you to reveal your password. If you forget your password, you can request to have a new password sent to your registered e-mail address. You agree to indemnify and hold AFeroN and their respective employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and content providers harmless for any improper or illegal use of any of your AFeroN account(s). This includes illegal or improper use by someone to whom you have given permission to use your AFeroN account(s) or whom you have negligently allowed to access your AFeroN account(s). AFeroN reserves the right to terminate your AFeroN account(s) if any activity that occurs with respect to such account(s) violates these Terms of Use.
</p>

<h2>ELECTRONIC COMMUNICATIONS
</h2>
<p>When you access a AFeroN Offering, send e-mails or electronically chat with AFeroN, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by e-mail or by posting notices in the AFeroN Offerings. You agree that all agreements, notices, disclosures and other communications that AFeroN provides to you electronically satisfy any legal requirement that such communications be in writing.
</p>

<h2>VIRTUAL ITEMS
</h2>
<p>Certain AFeroN Offerings may provide you with the opportunity to license a variety of virtual items such as virtual currency, virtual goods, additional levels and content packs ("virtual items") that can be used while playing the AFeroN Offering. You may be required to pay a fee to obtain virtual items.  When you use virtual items within a AFeroN Offering, any virtual items that you have purchased will be deemed used before any virtual items that you have earned.
</p>
<p>You have no property interest in any virtual items. Any purchase of virtual items, and virtual items accumulated through any applicable AFeroN Offering membership benefits, are purchases of a limited, non-transferable, revocable license to use those virtual items within the applicable AFeroN Offering. Virtual items may not be transferred or resold for commercial gain in any manner, including, without limitation, by means of any direct sale or auction service.  Virtual Items may not be purchased or sold from any individual or other company via cash, barter or any other transaction. Virtual items have no monetary value, and cannot be used to purchase or use products or services other than within the applicable AFeroN Offering. Virtual items cannot be refunded or exchanged for cash or any other tangible value.
</p>
<p>AFeroN may manage, regulate, control, modify or eliminate your virtual items in our sole discretion, and AFeroN will have no liability to you or anyone for exercising those rights. In addition, all virtual items are unconditionally forfeited if your AFeroN Offering account is terminated or suspended for any reason, in AFeroN's sole discretion, or if AFeroN discontinues any AFeroN Offering or any portion or feature of any AFeroN Offering.
</p>
<p>AFeroN has no liability for hacking or loss of your virtual items. AFeroN has no obligation to, and will not, reimburse you for any virtual items lost due to your violation of these Terms of Use. AFeroN reserves the right, without prior notification, to limit the order quantity on any virtual items and/or to refuse to provide you with any virtual items. Price and availability of virtual items are subject to change without notice.
</p>

<h2>SOCIAL NETWORK SITES
</h2>
<p>If you access a AFeroN Offering via a third party social networking site (a “<strong>Social Game</strong>”), you should be aware that Social Games are only available to individuals who have registered with the social networking site through which s/he accesses Social Games. You agree that your social networking site account information is accurate, current and complete.
</p>
<p>If AFeroN has reasonable grounds to suspect that you have provided any information that is inaccurate, not current or incomplete, AFeroN may suspend or terminate your ability to use or access Social Games and refuse any and all current or future use of or access to Social Games (or any portion thereof).
</p>

<h2>REVIEWS, COMMUNICATIONS AND SUBMISSIONS
</h2>

<h3>Generally
</h3>
<p>Without limiting the scope of these Terms of Use, you agree to comply with our <a href="http://forums.bigfishgames.com/faqs/list.page " target="_blank" data-lang="en">Forum FAQ</a> and <a href="http://reviews.bigfishgames.com/content/8524-en_us/guidelines.htm" target="_blank" data-lang="en">Review Guidelines</a> when you submit reviews, forum posts and other content via any AFeroN Offering. Inappropriate, obscene, defamatory, offensive language, crude or explicit sexual content, discussions of any matters which are explicitly or by inference illegal in any way, discussions of illegal or any other drugs, and racially and ethnically offensive speech are examples of unsuitable content that are not permitted within the AFeroN Offerings. Content standards may vary depending on where you are within a AFeroN Offering and the expectations of the relevant game community. Some game play may involve use of stronger language than others, including mild expletives. You should always use your best and most respectful and conservative judgment in interacting as part of any game play, and submitting any content, such as a review or post to any forums or message boards, within a AFeroN Offering.
</p>
<p>We expressly reserve the right, but have no obligation, to: (a) monitor any communications within the AFeroN Offerings, including, without limitation, to ensure that appropriate standards of online conduct are being observed, and (b) immediately or at any time remove any content that we deem objectionable or unsuitable in our sole discretion. AFeroN does not endorse, approve, or prescreen any content that you or other users post or communicate on or through any AFeroN Offerings. AFeroN does not assume any responsibility or liability for any content that is generated, posted or communicated by any user on or through the AFeroN Offerings. You agree to indemnify AFeroN and each of their respective employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and content providers from any liability or damages arising out of or resulting from any content you post or communicate on or through the AFeroN Offerings.
</p>
<p>Without limiting the generality of these policies and standards, the following actions are examples of behavior that violate these Terms of Use and may result in any or all of your AFeroN account(s) being immediately suspended or terminated:
</p>


<ul class="gbullets gbullets_black">
    <li>Posting, transmitting, promoting, or distributing any content that is illegal
    </li>
    <li>Harassing or threatening any other user of a AFeroN Offering or any employee or contractor of AFeroN
    </li>
    <li>Impersonating another person, indicating that you are a AFeroN employee or a representative of AFeroN (if you are not), or attempting to mislead users by indicating that you represent AFeroN in any way
    </li>
    <li>Attempting to obtain a password, other account information, or other private information from any other user of a AFeroN Offering
    </li>
    <li>Uploading any software, files, photos, images or any other content to a AFeroN Offering that you do not own or have the legal right to freely distribute, or that contain a virus or corrupted data, or any other malicious or invasive code or program
    </li>
    <li>Posting messages for any purpose other than personal communication, including without limitation advertising, promotional materials, chain letters, pyramid schemes, political campaigning, soliciting funds, mass mailings and sending "spam", or making any commercial use of any AFeroN Offering.
    </li>
    <li>Disrupting the normal flow of dialogue, or otherwise acting in a manner that negatively affects or disrupts other users.
    </li>
    <li>Improperly using any game support functions or complaint buttons, or making false complaints or other reports to AFeroN representatives.
    </li>
    <li>Posting or communicating any player's real-world personal information within a AFeroN Offering or by or through a AFeroN Offering or any related bulletin board.
    </li>
    <li>Uploading or transmitting, or attempting to upload or transmit, any material that acts as a passive or active information collection or transmission mechanism, including, without limitation, gifs, 1x1 pixels, web bugs, and other similar devices.
    </li>
    <li>Using or launching any automated system, including, without limitation, any spider, bot, cheat utility, scraper or offline reader that accesses a AFeroN Offering, or using or launching any unauthorized script or other software.
    </li>
    <li>Using a false e-mail address or otherwise disguising the source of any content that you submit within a AFeroN Offering, or using tools which anonymize your internet protocol address.
    </li>
    <li>Interfering or circumventing any AFeroN Offering security feature or any feature that restricts or enforces limitations on use of or access to a AFeroN Offering.
    </li>
    <li>Attempting to sell any part of a AFeroN Offering, including, without limitation, any virtual items (if applicable), AFeroN accounts and access to them in exchange for real currency or items of monetary or other value.
    </li>
    <li>Engaging in cheating or any other activity that AFeroN deems to be in conflict with the spirit of a AFeroN Offering.
    </li>
</ul>

<h3>Public Nature of Communications
</h3>
<p>You acknowledge and agree that your submitted content, including your reviews and your communications with other users via online messaging, private messaging, forums or bulletin boards, and any other similar types of communications and submissions on or through any AFeroN Offering, are non-confidential, public communications, and you have no expectation of privacy concerning your use of or participation in any AFeroN Offerings (other than with respect to the information you provide to us in establishing your AFeroN account(s), if applicable). You acknowledge that personal information that you communicate publicly within any AFeroN Offering may be seen and used by others and may result in unsolicited communications. AFeroN is not liable for any information that you choose to submit or communicate to other users on or through any AFeroN Offerings, or for the actions of any other users of any AFeroN Offering.
</p>
<p>You represent and warrant that you have all necessary rights in and to any materials that you post within any AFeroN Offering, that such materials do not infringe any proprietary or other rights of third parties, that all such content is accurate and will not cause injury to any person or entity, and that you will indemnify AFeroN and their respective employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and content providers for all claims resulting from your submitted and posted content. If any such materials incorporate the name, voice, likeness and/or image of any individual, you represent and warrant that you have the right to grant AFeroN permission to use any such name, voice, likeness and/or image of such individual appearing in the materials you post throughout the world in perpetuity. Once you post or communicate any content or materials on or through a AFeroN Offering, you expressly grant AFeroN the complete, worldwide, fully sublicensable and irrevocable right to quote, re-post, use, reproduce, modify, adapt, publish, translate, create derivative works from, display, distribute, transmit, and broadcast such content or materials, including without limitation the name you submit in connection with such content or materials, in any form, with or without attribution to you, and without any notice or compensation to you of any kind. We reserve the right to immediately remove any content that may be considered, in our sole discretion, in violation of the rights of any third party.
</p>

<h3>Commercial Activity and Unsolicited E-mail
</h3>
<p>You may not use any portion of the AFeroN Offerings to collect information, including login names, about other users, and use of such information to send unsolicited e-mail or for any other purpose is strictly prohibited. You may not advertise any goods or services on any AFeroN Offerings, or otherwise exploit your participation on or through any AFeroN Offerings for any commercial purpose.
</p>

<h3>Customer Reviews
</h3>
<p>You may submit reviews of certain AFeroN Offerings. Use of the reviews feature is for your personal, non-commercial use and is at your own option and risk, and you must comply with the policies set forth in these Terms of Use and the <a href="http://reviews.bigfishgames.com/content/8524-en_us/guidelines.htm" target="_blank" data-lang="en">Review Guidelines</a>.
</p>
<p>When you post a review, we will display your rating of the AFeroN Offering, along with your user name and certain other information you may provide, such as your city and state location, skill level, favorite game and favorite genres. By submitting a review, you are consenting to the release of all information that you provide in that review to a public forum. If you do not want any such information to be shared in a public forum, do not use the review feature.
</p>

<h3>AFeroN SOFTWARE AND AFeroN INSTANT GAMES
</h3>
<p>We may require that you download certain software from AFeroN, its principals or its licensors onto your computer ("<strong>AFeroN Software</strong>"). Subject to your compliance with these Terms of Use, AFeroN grants to you a non-exclusive, non-transferable, non-sublicensable, revocable, limited license to use the AFeroN Software to participate in the AFeroN Offerings. The AFeroN Software is for your personal use, and may not be reproduced, duplicated, copied, resold, sublicensed, or otherwise used in whole or in part by you for commercial purposes. You may not modify, translate, reverse-engineer, reverse-compile or decompile, disassemble or create derivative works from any of the AFeroN Software.
</p>
<p>You will not be required to download any AFeroN Software onto your computer if you choose to access AFeroN's streaming offering ("AFeroN Instant Games"). Subject to your compliance with these Terms of Use, AFeroN grants you a non-exclusive, non-transferable, non-sublicensable, revocable, limited license to use AFeroN Instant Games to access and play games on a streaming basis.  Access to AFeroN Instant Games is for your personal, non-commercial use.  You may not reproduce, duplicate, copy, resell, modify, translate, reverse-engineer, reverse-compile or decompile, disassemble or create derivative works from AFeroN Instant Games or any games or other content contained therein.</p>
<p>NEITHER AFeroN GAMES, INC. NOR ITS LICENSORS NOR AFeroN GAMES LUXEMBOURG S.A.R.L, IS LIABLE FOR ANY DAMAGES IN CONNECTION WITH YOUR USE OF ANY AFeroN SOFTWARE OR AFeroN INSTANT GAMES (INCLUDING LIABILITY FOR ANY CONSEQUENTIAL OR INCIDENTAL DAMAGES OR DAMAGE TO YOUR COMPUTER HARDWARE OR SOFTWARE), AND THE ENTIRE RISK OF USE, INCLUDING, WITHOUT LIMITATION, ANY DAMAGE TO YOUR COMPUTER HARDWARE OR SOFTWARE, RESULTING FROM ANY USE OF THE AFeroN SOFTWARE AND AFeroN UNILIMITED, RESIDES WITH YOU.
</p>

<h2>THIRD PARTY LINKS and THIRD PARTY CONTENT AND SERVICES
</h2>
<p>Any and all software, content and services (including advertising) within a AFeroN Offering that are not owned by AFeroN are “<strong>third party content and services</strong>”.  AFeroN acts merely as an intermediary service provider of, and accepts no responsibility or liability for, third party content and services.  In addition and without limiting the generality of the foregoing, certain AFeroN Offerings may include links to sites operated by third parties, including advertisers and other content providers.  Those sites may collect data or solicit personal information from you.  AFeroN does not control such sites, and is not responsible for their content, policies, or collection, use or disclosure of any information those sites may collect.
</p>

<h2>VIOLATION OF THESE TERMS OF USE
</h2>
<p>If you violate our Terms of Use, we reserve the right, in our sole discretion, to immediately terminate your participation in any or all AFeroN Offerings, including any and all AFeroN accounts you have established. You acknowledge that we are not required to notify you prior to terminating any such account.
</p>

<h2>TERMINATION OF ANY AFeroN ACCOUNT
</h2>
<p>AFeroN and you each have the right to terminate or cancel any of your AFeroN account(s), if applicable, at any time for any reason. You understand and agree that cancellation of your AFeroN account(s) and/or ceasing use of any and all AFeroN Offerings are your sole right and remedy with respect to any dispute with AFeroN. This includes, but is not limited to, any dispute arising out of or directly or indirectly related to: (a) any provision contained in these Terms of Use or any other agreement between you and AFeroN, including, without limitation, the <a href="http://www.bigfishgames.com/company/privacy.html" title="Learn how we protect your privacy." target="_top">Privacy Policy</a>, or AFeroN's enforcement or application of these Terms of Use or any other such agreement, including, without limitation, the <a href="http://www.bigfishgames.com/company/privacy.html" title="Learn how we protect your privacy." target="_top">Privacy Policy</a>, (b) the content available on or through the AFeroN Offerings, or any change in or to such content, (c) your ability to access and/or use any AFeroN Offerings, or (d) the amount or type of any fees, surcharges, applicable taxes, billing methods, or any change to the fees, applicable taxes, surcharges or billing methods, in each case imposed or implemented by AFeroN on or through any AFeroN Offering.
</p>
<p>AFeroN reserves the right to collect fees, surcharges or costs incurred before you cancel your AFeroN account(s) or a particular subscription. In the event that your AFeroN account or a particular subscription is terminated or cancelled, no refund will be granted, no online time or other credits (e.g., points in an online game) will be credited to you or converted to cash or other form of reimbursement, and you will have no further access to your account or anything associated with it (such as points, tokens or in-game items). Any delinquent or unpaid accounts must be settled before AFeroN may allow you to create any new or additional accounts. All virtual items are unconditionally forfeited if your AFeroN account is terminated or suspended for any reason, in AFeroN's sole discretion, or if AFeroN discontinues any AFeroN Offering that includes virtual items.
</p>
<p>Without limiting the foregoing provisions, if you violate these Terms of Use, AFeroN may issue you a warning regarding the violation, or, in AFeroN's sole discretion, immediately terminate any and all AFeroN accounts that you have established with any AFeroN Offering, with or without notice.
</p>

<h2>INTELLECTUAL PROPERTY RIGHTS
</h2>
<p>The names and logos, and other graphics, logos, icons, and service names associated with the AFeroN Offerings are trademarks, registered trademarks or trade dress of AFeroN or its licensors or principals in the United States and/or other countries. AFeroN's trademarks and trade dress may not be used in connection with any product or service that is not owned or operated by or on behalf of AFeroN, or in any manner that is likely to cause confusion among consumers or that disparages or discredits AFeroN or any AFeroN Offering. The compilation of all content on the AFeroN Offerings is the exclusive property of AFeroN and is protected by United States and international copyright laws. You may not use, copy, transmit, modify, distribute, or create any derivative works from any content from the AFeroN Offerings unless we have expressly authorized you to do so in writing. All other trademarks not owned by AFeroN that appear on the AFeroN Offerings are the property of their respective owners, who may or may not be affiliated with or connected to AFeroN. If you fail to adhere to these Terms of Use, other content owners may take criminal or civil action against you. In the event legal action is taken against you for your acts and/or omissions with regard to any content on the AFeroN Offerings, you agree to indemnify and hold harmless AFeroN and its employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and content providers.
</p>

<h2>LIMITATIONS ON WARRANTIES AND LIABILITY
</h2>
<p>YOU EXPRESSLY AGREE THAT THE USE OF ANY AFeroN OFFERING, AFeroN SOFTWARE AND THE INTERNET IS AT YOUR SOLE RISK. ALL AFeroN OFFERINGS AND AFeroN SOFTWARE ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS FOR YOUR USE, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, UNLESS SUCH WARRANTIES ARE LEGALLY INCAPABLE OF EXCLUSION. WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, AFeroN DOES NOT GUARANTEE THAT YOU WILL BE ABLE TO ACCESS OR USE THE AFeroN OFFERINGS OR AFeroN SOFTWARE AT ANY PARTICULAR TIMES OR LOCATIONS, OR THAT THE AFeroN OFFERINGS, AFeroN SOFTWARE, NEWSLETTERS, E-MAILS OR OTHER COMMUNICATIONS SENT FROM AFeroN ARE FREE FROM VIRUSES OR OTHER HARMFUL COMPONENTS.
</p>
<p>YOU ACKNOWLEDGE AND AGREE THAT YOUR SOLE AND EXCLUSIVE REMEDY FOR ANY DISPUTE WITH AFeroN IS TO STOP USING THE AFeroN OFFERINGS AND AFeroN SOFTWARE, AND TO CANCEL ANY AND ALL OF YOUR AFeroN ACCOUNTS, IF APPLICABLE. YOU ACKNOWLEDGE AND AGREE THAT AFeroN IS NOT LIABLE FOR ANY ACT OR FAILURE TO ACT ON ITS OWN PART, OR FOR ANY CONDUCT OF, OR COMMUNICATION OR CONTENT POSTED WITHIN A AFeroN OFFERING BY, ANY AFeroN OFFERING USER. IN NO EVENT SHALL AFeroN’ OR ITS EMPLOYEES', CONTRACTORS', OFFICERS', DIRECTORS' OR SHAREHOLDERS' LIABILITY TO YOU EXCEED THE AMOUNT THAT YOU PAID TO AFeroN FOR YOUR PARTICIPATION IN ANY AFeroN OFFERING. IN NO CASE SHALL AFeroN OR ITS EMPLOYEES, CONTRACTORS, OFFICERS, DIRECTORS OR SHAREHOLDERS BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING FROM YOUR USE OF ANY AFeroN OFFERING OR AFeroN SOFTWARE. BECAUSE SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR THE LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH STATES OR JURISDICTIONS, SUCH LIABILITY SHALL BE LIMITED TO THE FULL EXTENT PERMITTED BY LAW.
</p>
<p>YOU FURTHER SPECIFICALLY ACKNOWLEDGE THAT AFeroN IS NOT LIABLE, AND YOU AGREE NOT TO SEEK TO HOLD AFeroN LIABLE, FOR THE CONDUCT OF THIRD PARTIES, INCLUDING OTHER USERS OF AFeroN OFFERINGS AND OPERATORS OF SOCIAL NETWORKING AND OTHER EXTERNAL SITES, AND THAT THE RISK OF USING OR ACCESSING AFeroN OFFERINGS AND AFeroN SOFTWARE, SOCIAL NETWORKING SITES AND OTHER EXTERNAL SITES, AND OF INJURY FROM THE FOREGOING, RESTS ENTIRELY WITH YOU.
</p>

<h2>INDEMNIFICATION
</h2>
<p>You agree to defend, indemnify and hold harmless AFeroN and their respective employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and content providers from and against any and all liabilities, claims and expenses, including attorneys' fees, that arise from a breach of these Terms of Use for which you are responsible or in connection with your transmission of any content to, on or through any AFeroN Offering. Without limiting your indemnification obligations described herein, AFeroN reserves the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you.
</p>

<h2>IMPORT TAXES AND FEES
</h2>
<p>When you buy physical goods (e.g. CD-ROM) through any AFeroN Offering for delivery outside the United States, you are considered an importer and, as between you and AFeroN, you will be responsible for payment of all taxes, duties, fees or other charges that may be applicable to such importation, including VAT, and you must comply with all laws and regulations of the country in which you are receiving the goods. Your privacy is important to us and we know that you care about how information about your order is used and shared. We would like our international customers and customers dispatching products internationally to be aware that cross-border deliveries are subject to opening and inspection by customs authorities.
</p>

<h2>EXPORT CONTROL LAWS
</h2>
<p>Certain AFeroN Offerings may be subject to United States and international export controls.  By accessing AFeroN Offerings, you warrant that you are not located in any country, or exporting any AFeroN Offerings, to any person or place to which the United States, European Union or any other jurisdiction has embargoed goods.  You agree to abide by all applicable export control laws and further agree not to transfer or upload, by any means electronic or otherwise, any AFeroN Offerings that may be subject to restrictions under such laws to a national destination prohibited by such laws without obtaining and complying with any required governmental authorizations.
</p>

<h2>OTHER LEGAL TERMS
</h2>
<p>You agree that these Terms of Use are not intended to confer and do not confer any rights or remedies upon any third party. If any part of these Terms of Use are held invalid or unenforceable, that portion shall be construed in a manner consistent with applicable law to reflect, as nearly as possible, the original intentions of the parties, and the remaining portions shall remain in full force and effect. If any provision of these Terms of Use is found to be illegal or unenforceable, these Terms of Use will be deemed modified to the extent necessary to make them legal and enforceable, and will remain, as modified, in full force and effect. These Terms of Use, including all terms and policies referenced herein, contain the entire understanding, and supersede all prior agreements, between you and AFeroN relating to this subject matter, and cannot be changed or terminated orally.
</p>

<h2>PRIVACY
</h2>
<p>AFeroN respects the privacy of AFeroN Offerings users. Please review our <a href="http://www.bigfishgames.com/company/privacy.html" title="Learn how we protect your privacy." target="_top">Privacy Policy</a>, which also governs your access to and use of the AFeroN Offerings, to understand our policies and practices with respect your personal information.
</p>

<h2>APPLICABLE LAW AND JURISDICTION
</h2>
<p>These Terms of Use are governed by and shall be construed in accordance with the laws of the State of Washington, USA, excluding its conflicts of law rules. You expressly agree that exclusive venue and jurisdiction for any claim or dispute you may have with AFeroN based upon or in any manner related to any access or use by you of any AFeroN Offering, shall be in the state and federal courts located in King County, Washington, USA.  <strong>YOU AND AFeroN AGREE THAT ANY CLAIM WILL BE HANDLED ONLY ON AN INDIVIDUAL BASIS AND NOT IN A CLASS, CONSOLIDATED OR REPRESENTATIVE ACTION OR CLAIM.  FURTHER, YOU AND AFeroN EACH WAIVE ANY RIGHT TO A JURY TRIAL.</strong>
</p>

<h2>STATUTE OF LIMITATIONS
</h2>
<p>You and AFeroN agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of a AFeroN Offering, these Terms of Use or the <a href="http://www.bigfishgames.com/company/privacy.html" title="Learn how we protect your privacy." target="_top">Privacy Policy</a> must be filed within ONE (1) YEAR after such claim or cause of action arose, and is thereafter forever barred.
</p>

<h2>CONTACT US
</h2>

<blockquote>
    AFeroN Games, Inc.<br> Attn: Legal Department<br> 333 Elliott Avenue West, Suite 200<br> Seattle, Washington 98119<br> USA<br>

</blockquote>
<blockquote>
    AFeroN Games Luxembourg S.A.R.L.<br> 46a, Avenue J.F. Kennedy<br> L-1855 Luxembourg City<br> Grand-Duché de Luxembourg<br>

</blockquote>

<h2>DIGITAL MILLENNIUM COPYRIGHT ACT
</h2>
<p>The Digital Millennium Copyright Act provides recourse to copyright owners who believe that their rights under the United States Copyright Act have been infringed by acts of third parties over the Internet. If you believe that your copyrighted work has been copied without your authorization and is available on or in a AFeroN Offering in a way that may constitute copyright infringement, you may provide notice of your claim to AFeroN's Designated Agent listed below. For your notice to be effective, it must include the following information:
</p>
<p>(i) A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;
</p>
<p>(ii) A description of the copyrighted work that you claim has been infringed upon;
</p>
<p>(iii) A description of where the material that you claim is infringing is located within the AFeroN Offering;
</p>
<p>(iv) Information reasonably sufficient to permit AFeroN to contact you, such as address, telephone number, and, if available, an e-mail address at which you may be contacted;
</p>
<p>(v) A statement by you that you have a good-faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and
</p>
<p>(vi) A statement that the information in the notification is accurate and, under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
</p>
<p>(vii) AFeroN's Designated Agent is:
</p>

<blockquote>
    AFeroN Games, Inc.<br> Attn: Legal Department<br> 333 Elliott Avenue West, Suite 200<br> Seattle, Washington 98119<br> USA<br>

</blockquote>

</div>

</div>