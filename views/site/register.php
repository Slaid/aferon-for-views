
<?php
$this->pageTitle = Controller::trans("Registration in Community");
$this->description = Controller::trans("Page of the registration on project AFeroN.com");

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array('Registration' => array("site/register"), Controller::trans('Registration in Community')),
));
?>
<div class="span-16">
<h1><?php echo Controller::trans('Registration at AFeroN'); ?></h1>

<p><?php echo Controller::trans('Please fill out the following form with your registration credentials:'); ?></p>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note"><?php echo Controller::trans('Fields with'); ?> <span class="required">*</span> <?php echo Controller::trans('are required.'); ?></p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'confirm_password'); ?>
        <?php echo $form->passwordField($model,'confirm_password'); ?>
        <?php echo $form->error($model,'confirm_password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

	<div class="row submit">
		<?php echo CHtml::submitButton(Controller::trans('Registration'), array('class' => 'btn btn-info')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</div>