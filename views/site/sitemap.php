
<?php
$this->pageTitle=Yii::app()->name . ' - ' . Controller::trans('Site Map');

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(Controller::trans('Site Map') => array('site/sitemap'), Controller::trans('Site Map AFeroN')),
));

?>
<div class="span-18">

<div class="well" style="overflow: hidden;">
    <h1><?php echo Controller::trans('All Games at Sitemap'); ?></h1>
    <ul>
        <?php foreach($block['platforms'] as $pl => $platform) : ?>
            <li class="span-4"><a href="<?php echo Yii::app()->createUrl('game/index', array('platform' => $pl)); ?>"><?php echo $platform; ?></a>
                <ul>
                    <?php foreach($block['genres'] as $genre) : ?>
                        <li><a class="loadGames" href="<?php echo Yii::app()->createUrl('site/sitemap', array('genre' => $genre->genreid, 'platform' => $pl)); ?>"><?php echo $genre->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php endforeach; ?>
    </ul>


</div>

<?php if(isset($block['games']) && is_array($block['games']) && count($block['games'])) { ?>
<div class="label-text"><?php echo Controller::trans('Games with'); ?> <?php echo Game::getPlatforms($block['games'][0]->platform); ?> :: <?php echo $block['games'][0]->genre->name; ?> :: <?php echo Yii::app()->params['langs'][$block['games'][0]->lang]; ?></div>
<div class="well gamesBlock">
    <ul>
    <?php foreach($block['games'] as $game) : ?>
        <li><?php
            echo CHtml::tag("a", array(
                "href" => $game->getUrl(),
                "title" => "View game : " . $game->gamename,
            ), $game->gamename);
        ?></li>
    <?php endforeach; ?>
    </ul>
</div>
<?php } ?>

    </div>