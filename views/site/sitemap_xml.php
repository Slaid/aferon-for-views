<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php if(is_array($model) && count($model) > 0) : ?>
        <?php foreach($model as $data) : ?>
            <url>
                <loc>http://<?php echo ($data->lang == 'en') ? "" : $data->lang . "."; ?>aferon.com<?php echo Yii::app()->createUrl("game/view", array("gameid" => $data->gameid)); ?></loc>
                <lastmod><?php echo date("Y-m-d", strtotime($data->releasedate)); ?></lastmod>
                <changefreq>weekly</changefreq>
                <priority>1</priority>
            </url>
        <?php endforeach; ?>
    <?php endif;?>
</urlset>