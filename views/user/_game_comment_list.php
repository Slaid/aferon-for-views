<div class="label-text"><?php echo Controller::trans('Last five comments'); ?></div>
<div class="comment-list">
    <?php if(isset($errors) && is_array($errors) && count($errors)) : ?>
        <?php foreach($errors as $error) : ?>
            <div class="alert in alert-block fade alert-error"><a class="close" data-dismiss="alert">×</a><strong><?php echo Controller::trans('Error!'); ?></strong> <?php echo $error[0]; ?></div>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if($data->comments) : ?>
    <?php foreach($data->comments as $comment) : ?>
        <div class="comment-block">

            <a title="View profile!" href="<?php  echo Yii::app()->createUrl('user/view', array("id" => $comment->user->id)); ?>"><img src="<?php echo(Profile::getAvatar($comment->user->id)); ?>" alt="<?php echo Controller::trans('Avatar'); ?> " style="width: 50px; height: 50px; float:left; margin-right: 10px;"></a>
            <div class="right">
                <div class="info"><?php echo date("d, F Y" , $comment->date); ?> | <?php echo Controller::trans('Author'); ?> : <?php echo $comment->user->profile->nick; ?><?php if($comment->user_id == Yii::app()->user->id) { ?><div class="delete" value="<?php echo $comment->id; ?>">X</div><?php } ?></div>
                <div class="body"><?php echo $comment->text ;?></div>
            </div>

        </div>
    <?php endforeach; ?>
    <?php else: ?>
        <?php echo Controller::trans('Comments not found'); ?>
    <?php endif; ?>
</div>
