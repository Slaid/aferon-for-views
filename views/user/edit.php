<?php
$this->pageTitle=Yii::app()->name . ' - ' . Controller::trans('Edit Profile');

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(Controller::trans('View Profile') => array('user/index'), Controller::trans('Edit profile') . " " . Yii::app()->user->name),
));

?>


<div class="span-16">
    <h1><?php echo Controller::trans('Edit profile'); ?></h1>

    <div class="user-profile">
        <div class="span-5">
            <div class="avatar margin-bottom">
                <?php
                echo CHtml::tag('img', array(
                    'src' => Profile::getAvatar($data->id),
                    'id' => 'avatar-url',
                )); ?>
            </div>
            <form id="upload-form" action="<?php echo Yii::app()->createUrl("user/uploadAvatar"); ?>" method="POST" enctype="multipart/form-data">
                <label for="upload-avatar" class="btn btn-success upload-avatar"><?php echo Controller::trans('Upload Avatar'); ?></label>
                <input style="display:none;" type="file" accept="image/x-png, image/gif, image/jpeg" name="avatar" id="upload-avatar"/>
                <?php echo CHtml::tag('a', array(
                    'href' => Yii::app()->createUrl('user/edit'),
                    'class' => 'btn btn-error',
                ), Controller::trans("Cancel")); ?>
            </form>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('#upload-avatar').ajaxfileupload({
                        'action': $("#upload-form").attr('action'),
                        'params': {
                            'extra': 'info'
                        },
                        'onComplete': function(response) {
                            if(response.success) {
                                $('#avatar-url').attr('src', response.patch);
                                $('#new_avatar').val(response.filename);
                                $('.upload-avatar').text(response.msg);
                                $('.upload-avatar').addClass('disabled');
                            }
                        },
                        'onStart': function() {
                            //if(weWantedTo) return false; // cancels upload
                        },
                        'onCancel': function() {
                            $('.upload-avatar').text('Fail upload');
                        }
                    });

                });
            </script>
        </div>
        <div class="span-8">
            <?php /** @var BootActiveForm $form */
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'horizontalForm',
                'type'=>'search',
            )); ?>

            <?php echo $form->textFieldRow($data->profile, 'nick', array('prepend'=>Controller::trans('Display Name'))); ?><br /><br />
            <input type="hidden" id="new_avatar" name="avatar" value="false">
            <?php echo $form->textAreaRow($data->profile, 'sigma', array('class'=>'span4', 'rows'=>5)); ?><br /><br />

            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'info', 'label'=> Controller::trans('Save'))); ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>