<?php
$this->pageTitle=Yii::app()->name . ' - ' . Controller::trans('All members');

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(Controller::trans('All users')=> array('user/index'), Controller::trans('Find profile')),
));

?>

<div class="span-18">
<h1><?php echo Controller::trans('All Users'); ?></h1>


<?php foreach($model as $data) : ?>

        <div class="user-list-block-sidebar">
            <div class="img">
                <?php echo CHtml::tag('img',array(
                    "src" => Profile::getAvatar($data->id),
                )); ?>
            </div>

            <div class="context">
                <?php echo CHtml::tag("a", array(
                    "href" => Yii::app()->createUrl("user/view", array("id" => $data->id)),
                    "title" => Controller::trans("View profile"),
                ), (isset($data->profile->nick) ? $data->profile->nick : $data->username)); ?>
                <b style="color: green;">Rating <?php echo $data->points; ?></b>
                <div>
                    <?php echo Controller::trans("Date register") . ' : <br />' . date("j, M Y", $data->date); ?>
                </div>
            </div>
        </div>


<?php endforeach; ?>


</div>
