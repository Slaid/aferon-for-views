<?php
$this->pageTitle=Yii::app()->name . ' - ' . Controller::trans('View Profile') . $data->username;

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(Controller::trans('View Profile') => array('user/view', 'id' => $data->id), Controller::trans('Profile') . ' '. $data->getName()),
));

?>
<div class="span-16">

    <div class="user-profile">
        <div class="span-5">
            <div class="avatar margin-bottom">
                <?php
                    echo CHtml::tag('img', array(
                        'src' => Profile::getAvatar($data->id),
                    )); ?>
                </div>
        </div>
        <div class="span-8">
            <h1><?php echo Controller::trans('Profile'); ?> <?php echo $data->getName(); ?></h1>
            <div class="text-label"><?php echo Controller::trans('Sigma'); ?></div>
            <div class="sigma">
                <?php echo CHtml::encode($data->profile->sigma); ?>
            </div>
        </div>
    </div>
</div>